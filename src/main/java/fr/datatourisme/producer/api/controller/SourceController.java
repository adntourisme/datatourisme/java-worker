/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.api.controller;

import fr.datatourisme.api.controller.AbstractController;
import fr.datatourisme.worker.Storage;
import fr.datatourisme.worker.Tube;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.*;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@RestController
@RequestMapping("/producer/{fluxId}/source")
public class SourceController extends AbstractController {

    @Autowired @Qualifier("producer")
    Tube tube;

    @GetMapping("/download")
    @ResponseBody
    public ResponseEntity<?> get(@PathVariable Integer fluxId) throws IOException {
        Storage storage = tube.getStorage().dir(fluxId).dir("source");
        File[] files = storage.toFile().listFiles((d, name) -> name.endsWith(".xml"));
        if (files == null || files.length == 0) {
            throw new RuntimeException("Unable to find any XML sources");
        }

        // zip files
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        ZipOutputStream zout = new ZipOutputStream(new BufferedOutputStream(outputStream));
        byte[] bytes;
        for(File file: files) {
            bytes = new byte[1024];
            FileInputStream fin = new FileInputStream(file);
            zout.putNextEntry(new ZipEntry(file.getName()));
            int length;
            while((length = fin.read(bytes, 0, 1024))>0) {
                zout.write(bytes,0,length);
            }
            zout.closeEntry();
            fin.close();
        }
        zout.close();

        // send as response
        String filename = "datatourisme-" + fluxId + "-source.zip";
        bytes = outputStream.toByteArray();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        headers.setContentDispositionFormData(filename, filename);
        headers.setCacheControl("no-cache, post-check=0, pre-check=0");
        //headers.set("Content-Encoding", "gzip");
        return new ResponseEntity<>(bytes, headers, HttpStatus.OK);
    }
}
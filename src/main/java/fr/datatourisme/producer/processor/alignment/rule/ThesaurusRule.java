/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.alignment.rule;

import fr.datatourisme.ontology.OntologyResource;
import fr.datatourisme.producer.processor.reasoner.ValidationReasoner;
import fr.datatourisme.producer.processor.xml.XmlContext;
import fr.datatourisme.producer.processor.xml.query.Exception.QueryExpressionException;
import org.apache.commons.lang3.StringUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ThesaurusRule extends AbstractRule {

    @Override
    public Stream<String> evaluate(XmlContext ctx, Resource resource) throws QueryExpressionException {
        Stream<String> values = super.evaluate(ctx, resource);
        if(type == Type.constant) {
            return checkFunctional(evaluateConstants(values), resource);
        } else {
            return checkFunctional(evaluateExtractedValues(values, resource), resource);
        }
    }

    /**
     * @param values
     * @return
     */
    protected Stream<String> evaluateConstants(Stream<String> values) {
        String rangeUri = property.getRange().getURI();
        return values
            .map(uri -> alignment.getOntology().getIndividual(uri))
            .filter(individual -> individual != null && individual.isInstanceOf(rangeUri))
            .map(OntologyResource::getURI);
    }

    /**
     * @param values
     * @return
     */
    protected Stream<String> evaluateExtractedValues(Stream<String> values, Resource resource) {
        String rangeUri = property.getRange().getURI();

        // extract values
        List<String> list = values.collect(Collectors.toList());

        Set<String> set = list.stream()
            .flatMap(value -> alignment.getThesaurusAlignment().resolve(value, rangeUri).stream())
            .map(OntologyResource::getURI)
            .collect(Collectors.toSet());

        if(set.size() == 0) {
            String rangeLabel = property.getRange().getLabel();
            list.forEach(value -> {
                addViolation(resource, ValidationReasoner.ViolationNature.warn,"La valeur \"" + StringUtils.abbreviate(value, 100) + "\" n'a pas de correspondance dans le thésaurus \"" + rangeLabel + "\"");
            });
        }

        return set.stream();
    }

    @Override
    public void doProcess(XmlContext ctx, Map<String, XmlContext> i18nCtx, Resource resource) throws QueryExpressionException {
        Model model = resource.getModel();
        evaluate(ctx, resource).forEach(uri -> {
            model.add(resource, model.createProperty(property.getURI()), model.createResource(uri));
        });
    }
}

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.alignment.transformer.literal;

import fr.datatourisme.producer.processor.alignment.transformer.exception.TransformException;

public class TimeTransformer extends AbstractTransformer<String> {

    @Override
    public String transform(Object value) throws TransformException {
        try {
            String[] split = value.toString().split(":");
            int hours = Integer.valueOf(split[0]);
            int minutes = split.length > 1 ? Integer.valueOf(split[1]) : 0;
            int seconds = split.length > 2 ? Integer.valueOf(split[2]) : 0;
            return String.format("%02d:%02d:%02d", hours, minutes, seconds);
        } catch(Exception e) {
            throw new TransformException(value, "date", e);
        }
    }
}

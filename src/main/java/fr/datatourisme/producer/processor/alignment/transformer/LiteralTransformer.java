/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.alignment.transformer;

import fr.datatourisme.ontology.OntologyProperty;
import fr.datatourisme.ontology.OntologyResource;
import fr.datatourisme.producer.processor.alignment.transformer.exception.TransformException;
import fr.datatourisme.producer.processor.alignment.transformer.literal.*;
import fr.datatourisme.vocabulary.Schema;
import org.apache.jena.datatypes.RDFDatatype;
import org.apache.jena.datatypes.TypeMapper;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.graph.impl.LiteralLabel;
import org.apache.jena.graph.impl.LiteralLabelFactory;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.ResourceFactory;
import org.apache.jena.rdf.model.impl.LiteralImpl;
import org.apache.jena.sparql.vocabulary.FOAF;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.XSD;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

public class LiteralTransformer {

    public static String defaultLang = "fr";

    private static final Map<String, Class<? extends AbstractTransformer>> getTransformerMap() {
        Map<String, Class<? extends AbstractTransformer>> map = new HashMap<>();

        // by property
        map.put(Schema.telephone.getURI(), PhoneNumberTransformer.class);
        map.put(FOAF.homepage.getURI(), URLTransformer.class);

        // by range
        map.put(XSD.date.getURI(), DateTransformer.class);
        map.put(XSD.time.getURI(), TimeTransformer.class);
        map.put(XSD.xboolean.getURI(), BooleanTransformer.class);
        map.put(XSD.xfloat.getURI(), DecimalTransformer.class);
        map.put(XSD.decimal.getURI(), DecimalTransformer.class);
        map.put(XSD.xint.getURI(), IntegerTransformer.class);
        map.put(XSD.integer.getURI(), IntegerTransformer.class);
        map.put(XSD.positiveInteger.getURI(), IntegerTransformer.class);

        return map;
    }

    /**
     * @param property
     * @return
     */
    public static Literal transform(OntologyProperty property, String originalValue, String lang) throws TransformException {
        if(originalValue == null) {
            return null;
        }

        OntologyResource range = property.getRange();
        Object value = originalValue;

        // 1. transform by property or by range
        if(getTransformerMap().containsKey(property.getURI())) {
            value = transformByUri(property.getURI(), originalValue);
        } else if(range != null && getTransformerMap().containsKey(range.getURI())) {
            value = transformByUri(range.getURI(), originalValue);
        }

        // 2. Determine language
        Boolean isLangString = range != null && range.hasURI(RDF.langString.getURI());
        if(isLangString) {
            lang = lang != null ? lang : defaultLang;
        } else {
            lang = null;
        }

        // 3. create literal

        // if range, use it to guess datatype
        if(range != null) {
            try {
                RDFDatatype dt = TypeMapper.getInstance().getSafeTypeByName(range.getURI());
                LiteralLabel ll = LiteralLabelFactory.createByValue(value, lang, dt);
                return new LiteralImpl(NodeFactory.createLiteral(ll), null);
            } catch(Exception e) {
                throw new TransformException(value, range.getShortURI(), e);
            }
        }

        // else, try builtin casting
        try {
            try{
                // try to cast to int
                // @todo : find a good library to guess type
                value = Integer.parseInt(value.toString());
            } catch (NumberFormatException ignored) {}

//            if(value instanceof String && lang != null)  {
//                return ResourceFactory.createLangLiteral(value.toString(), lang);
//            }
            return ResourceFactory.createTypedLiteral(value);
        } catch (Exception e) {
            throw new TransformException(value, null, e);
        }
    }

    /**
     * @param property
     * @return
     */
    public static Literal transform(OntologyProperty property, String originalValue) throws TransformException {
        return transform(property, originalValue, null);
    }

    /**
     * @param uri
     * @param value
     * @return
     * @throws TransformException
     */
    private static Object transformByUri(String uri, Object value) throws TransformException {
        try {
            AbstractTransformer transformer = getTransformerMap().get(uri).getConstructor().newInstance();
            return transformer.transform(value);
        } catch (InstantiationException | IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
            e.printStackTrace();
            return null;
        }
    }
}

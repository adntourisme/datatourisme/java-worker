/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.alignment.rule;

import fr.datatourisme.producer.processor.xml.XmlContext;
import fr.datatourisme.producer.processor.xml.query.Exception.QueryExpressionException;
import fr.datatourisme.vocabulary.DatatourismeAlignment;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.util.ResourceUtils;
import org.jdom2.Element;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ResourceRule extends AbstractRule {

    transient Set<? extends AbstractRule> rules = new HashSet<>();

    public Set<? extends AbstractRule> getRules() {
        return rules;
    }

    public void setRules(Set<? extends AbstractRule> rules) {
        this.rules = rules;
    }

//    @Override
//    public void process(XmlContext ctx, Map<String, XmlContext> i18nCtx, Resource resource) throws QueryExpressionException {
//        evaluateContexts(ctx, resource).forEach(ctx2 -> this.processContext(ctx2, resource));
//    }

    @Override
    public void doProcess(XmlContext ctx, Map<String, XmlContext> i18nCtx, Resource resource) throws QueryExpressionException {

        if(i18nCtx != null && i18nCtx.size() > 0) {
            // convert contexts in list
            List<XmlContext> contexts = evaluateContexts(ctx, resource).collect(Collectors.toList());

            // evaluate all i18n contexts
            Map<String, List<XmlContext>> i18nCtxList = new HashMap<>();
            for (Map.Entry<String, XmlContext> e : i18nCtx.entrySet()) {
                i18nCtxList.put(e.getKey(), evaluateContexts(e.getValue(), resource).collect(Collectors.toList()));
            }

            // foreach contexts, build the indexed base i18n context list
            for(int i = 0; i < contexts.size(); i++) {
                Map<String, XmlContext> i18nCtx2 = new HashMap<>();
                for (Map.Entry<String, List<XmlContext>> e : i18nCtxList.entrySet()) {
                    if(e.getValue().size() > i) {
                        i18nCtx2.put(e.getKey(), e.getValue().get(i));
                    }
                }
                // process sub-context
                this.processContext(contexts.get(i), i18nCtx2, resource);
            }
        } else {
            evaluateContexts(ctx, resource).forEach(ctx2 -> this.processContext(ctx2, null, resource));
        }
    }

    /**
     * Process a single context
     *
     * @param ctx
     */
    private void processContext(XmlContext ctx, Map<String, XmlContext> i18nCtx, Resource resource) {
        Model model = resource.getModel();
        Resource anon = model.createResource();
        getRules().forEach(r -> {
            try {
                r.process(ctx, i18nCtx, anon);
            } catch (QueryExpressionException e) {}
        });

        if(anon.listProperties().filterDrop(stmt -> stmt.getPredicate().hasURI(DatatourismeAlignment.hasViolation.getURI()))
                .toList().size() == 0) {
            return;
        }

        String uri = alignment.getUriGenerator().generate(anon);
        Resource named = ResourceUtils.renameResource(anon, uri );

        //model.add(res, RDF.type, model.createResource(getRDFTypeUri(res)));
        model.add(resource, model.createProperty(property.getURI()), named);
    }

    /**
     * Return a stream of sub-context based on current rule
     *
     * @param ctx
     * @return
     */
    private Stream<XmlContext> evaluateContexts(XmlContext ctx, Resource resource) throws QueryExpressionException {
        if(type == Type.single) {
            return Stream.of(ctx);
        } else if(expr != null) {
            return checkFunctional(
                ctx.evaluate(expr)
                    .filter(object -> object instanceof Element)
                    .map(object -> (Element)object)
                    .map(ctx::create),
                resource);
        }
        return Stream.empty();
    }

}

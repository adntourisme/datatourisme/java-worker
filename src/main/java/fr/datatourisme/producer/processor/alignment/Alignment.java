/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.alignment;

import fr.datatourisme.ontology.Ontology;
import fr.datatourisme.ontology.OntologyProperty;
import fr.datatourisme.producer.processor.URIGenerator;
import fr.datatourisme.producer.processor.alignment.rule.AbstractRule;
import fr.datatourisme.producer.processor.thesaurus.ThesaurusAlignment;
import org.apache.jena.rdf.model.Property;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class Alignment {
    Ontology ontology;
    ThesaurusAlignment thesaurusAlignment;
    URIGenerator uriGenerator;

    String contextExpr;

    protected Set<? extends AbstractRule> rules = new HashSet<>();

    public Alignment(Ontology ontology, ThesaurusAlignment thesaurusAlignment, URIGenerator uriGenerator) {
        this.ontology = ontology;
        this.thesaurusAlignment = thesaurusAlignment;
        this.uriGenerator = uriGenerator;
    }

    public Ontology getOntology() {
        return ontology;
    }

    public ThesaurusAlignment getThesaurusAlignment() {
        return thesaurusAlignment;
    }

    public URIGenerator getUriGenerator() {
        return uriGenerator;
    }

    public String getContextExpr() {
        return contextExpr;
    }

    public void setContextExpr(String contextExpr) {
        this.contextExpr = contextExpr;
    }

    public Set<? extends AbstractRule> getRules() {
        return rules;
    }

    public void setRules(Set<? extends AbstractRule> rules) {
        this.rules = rules;
    }

    /**
     * Return rules for a given property
     *
     * @param property
     * @return
     */
    public Set<? extends AbstractRule> getRules(OntologyProperty property) {
        return getRules().stream()
            .filter(r -> r.getProperty().getURI().equals(property.getURI()))
            .collect(Collectors.toSet());
    }

    /**
     * Return rules for a given property
     *
     *
     * @param property
     * @return
     */
    public Set<? extends AbstractRule> getRules(Property property) {
        return getRules(ontology.getProperty(property));
    }

    /**
     * Return the first rule for a given property
     *
     * @param property
     * @return
     */
    public AbstractRule getRule(OntologyProperty property) {
        return getRules(property)
            .stream().findFirst()
            .orElse(null);
    }

    /**
     * Return the first rule for a given property
     *
     *
     * @param property
     * @return
     */
    public AbstractRule getRule(Property property) {
        return getRule(ontology.getProperty(property));
    }
}

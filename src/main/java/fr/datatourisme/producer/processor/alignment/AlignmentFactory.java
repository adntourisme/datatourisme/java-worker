/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.alignment;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import fr.datatourisme.ontology.Ontology;
import fr.datatourisme.producer.processor.alignment.deserializer.AlignmentDeserializer;
import fr.datatourisme.producer.processor.thesaurus.ThesaurusAlignment;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.*;

public class AlignmentFactory {

    @Autowired
    Ontology ontologyAlignment;

    public Alignment createFromJson(ThesaurusAlignment thesaurusAlignment, Reader reader) {
        Gson gson = new GsonBuilder()
            .registerTypeAdapter(Alignment.class, new AlignmentDeserializer(ontologyAlignment, thesaurusAlignment))
            .create();
        return gson.fromJson(reader, Alignment.class);
    }

    public Alignment createFromJson(ThesaurusAlignment thesaurusAlignment, File file) throws FileNotFoundException {
        FileReader reader = new FileReader(file);
        return createFromJson(thesaurusAlignment, reader);
    }

    public Alignment createFromJson(ThesaurusAlignment thesaurusAlignment, InputStream in) {
        Reader reader = new InputStreamReader(in);
        return createFromJson(thesaurusAlignment, reader);
    }

    public Alignment createFromJson(ThesaurusAlignment thesaurusAlignment, String json) {
        Reader reader = new StringReader(json);
        return createFromJson(thesaurusAlignment, reader);
    }
}

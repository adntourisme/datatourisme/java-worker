/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.alignment.rule;

import fr.datatourisme.producer.processor.alignment.transformer.LiteralTransformer;
import fr.datatourisme.producer.processor.alignment.transformer.exception.TransformException;
import fr.datatourisme.producer.processor.xml.XmlContext;
import fr.datatourisme.producer.processor.xml.query.Exception.QueryExpressionException;
import org.apache.jena.rdf.model.Literal;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDF;

import java.util.Map;
import java.util.Objects;
import java.util.stream.Stream;

public class LiteralRule extends AbstractRule {

    protected String lang;

    @Override
    public void doProcess(XmlContext ctx, Map<String, XmlContext> i18nCtx, Resource resource) throws QueryExpressionException {
        doProcessLang(ctx, lang, resource);
        if(i18nCtx != null
            && property.getRange().hasURI(RDF.langString.getURI())
            && (lang == null || lang.equals(LiteralTransformer.defaultLang))) {
            for (Map.Entry<String, XmlContext> e : i18nCtx.entrySet()) {
                doProcessLang(e.getValue(), e.getKey(), resource);
            }
        }
    }

    /**
     *
     * @param ctx
     * @param resource
     * @throws QueryExpressionException
     */
    private void doProcessLang(XmlContext ctx, String lang, Resource resource) throws QueryExpressionException {
        Stream<Literal> stream = evaluate(ctx, resource)
            .map(value -> processValue(value, lang, resource))
            .filter(Objects::nonNull);

        Model model = resource.getModel();
        checkFunctional(stream, resource).forEach(literal -> {
            model.add(resource, model.createProperty(property.getURI()), literal);
        });
    }

    /**
     * @param value
     * @param lang
     * @param resource
     */
    private Literal processValue(String value, String lang, Resource resource) {
        try {
            return LiteralTransformer.transform(property, value, lang);
        } catch (TransformException e) {
            addWarn(resource, e.getMessage());
        }
        return null;
    }
}

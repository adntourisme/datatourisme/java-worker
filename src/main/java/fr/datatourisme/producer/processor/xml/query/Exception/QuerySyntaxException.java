/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.xml.query.Exception;

/**
 * 
 */
public class QuerySyntaxException extends QueryExpressionException {
    public QuerySyntaxException(Throwable throwable) {
        super(throwable);
    }
    public QuerySyntaxException(String s, Throwable throwable) {
        super(s, throwable);
    }
}

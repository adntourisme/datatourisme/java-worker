/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.xml.query;

import fr.datatourisme.producer.processor.xml.query.Exception.QueryEvaluationException;
import fr.datatourisme.producer.processor.xml.query.Exception.QuerySyntaxException;
import org.jdom2.Element;
import org.jdom2.Namespace;
import org.jdom2.filter.Filters;
import org.jdom2.xpath.XPathFactory;

import java.util.Collection;
import java.util.stream.Stream;

/**
 * 
 */
public class XPathExpression extends QueryExpression {
    org.jdom2.xpath.XPathExpression xPathExpression;

    XPathExpression(String expr, Collection<Namespace> namespaces) throws QuerySyntaxException {
        try {
            XPathFactory xpathFactory = XPathFactory.instance();
            xPathExpression = xpathFactory.compile(expr, Filters.fpassthrough(), null, namespaces);
        } catch (IllegalStateException e) {
            throw new QuerySyntaxException(e.getCause().getMessage(), e.getCause());
        } catch (Exception e) {
            throw new QuerySyntaxException(e.getMessage(), e.getCause());
        }
    }

    @Override
    public Stream<Object> evaluate(Element context) throws QueryEvaluationException {
        try {
            return xPathExpression.evaluate(context).stream();
        } catch (IllegalStateException e) {
            throw new QueryEvaluationException(e.getCause().getMessage(), e.getCause());
        } catch (Exception e) {
            throw new QueryEvaluationException(e.getMessage(), e.getCause());
        }
    }

    @Override
    public Object evaluateFirst(Element context) throws QueryEvaluationException {
        try {
            return xPathExpression.evaluateFirst(context);
        } catch (IllegalStateException e) {
            throw new QueryEvaluationException(e.getCause().getMessage(), e.getCause());
        } catch (Exception e) {
            throw new QueryEvaluationException(e.getMessage(), e.getCause());
        }
    }
}

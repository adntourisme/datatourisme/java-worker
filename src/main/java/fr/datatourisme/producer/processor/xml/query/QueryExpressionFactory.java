/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.xml.query;

import fr.datatourisme.producer.processor.xml.query.Exception.QuerySyntaxException;
import org.jdom2.Namespace;

import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

/**
 * 
 */
public class QueryExpressionFactory {
    List<Namespace> namespaces = new ArrayList<>();

    /**
     * An atomic reference storing all compiled expression
     */
    private final AtomicReference<Map<String, QueryExpression>> registry = new AtomicReference<>(new HashMap<>());

    public QueryExpressionFactory(Collection<Namespace> namespaces) {
        this.namespaces = new ArrayList<>(namespaces);
    }

    /**
     * Create a QueryExpression instance from this factory.
     */
    public QueryExpression compile(String expression) throws QuerySyntaxException {
        if(registry.get().containsKey(expression)) {
            return registry.get().get(expression);
        }

        QueryExpression queryExpression;
        try {
            queryExpression = new XPathExpression(expression, namespaces);
        } catch(QuerySyntaxException e) {
            try {
                queryExpression = new XQueryExpression(expression, namespaces);
            } catch (QuerySyntaxException e2) {
                throw new QuerySyntaxException("L'expression \"" + expression + "\" n'est pas une expression XPath 1.0 ou XQuery 3.1 valide.", e2);
            }
        }

        registry.get().put(expression, queryExpression);
        return queryExpression;
    }
}

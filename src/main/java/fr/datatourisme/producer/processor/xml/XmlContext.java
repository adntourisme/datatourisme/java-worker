/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.xml;

import fr.datatourisme.producer.processor.alignment.rule.AbstractRule;
import fr.datatourisme.producer.processor.xml.query.Exception.QueryExpressionException;
import fr.datatourisme.producer.processor.xml.query.QueryExpressionFactory;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

import java.io.IOException;
import java.io.StringReader;
import java.util.stream.Stream;

public class XmlContext {
    private Element context;
    private QueryExpressionFactory qef;

    /**
     * Constructor for root context
     *
     * @param context
     */
    public XmlContext(Element context) {
        this.context = context;
        qef = new QueryExpressionFactory(context.getNamespacesInScope());
    }

    /**
     * Constructor for sub-context
     *
     * @param xmlContext
     * @param context
     */
    public XmlContext(XmlContext xmlContext, Element context) {
        this.context = context;
        qef = xmlContext.qef;
    }

    public Object getContext() {
        return context;
    }

    public Stream<Object> evaluate(String expr) throws QueryExpressionException {
        return qef.compile(expr).evaluate(context);
    }

    public Object evaluateFirst(String expr) throws QueryExpressionException {
        return qef.compile(expr).evaluateFirst(context);
    }

    public String evaluateFirstValue(String expr) throws QueryExpressionException {
        return qef.compile(expr).evaluateFirstValue(context);
    }

    public Stream<String> evaluateValues(String expr) throws QueryExpressionException {
        return qef.compile(expr).evaluateValues(context);
    }

    public Stream<String> evaluateValues(AbstractRule rule) throws QueryExpressionException {
        return rule.evaluate(this);
    }

    public String evaluateFirstValue(AbstractRule rule) throws QueryExpressionException {
        return evaluateValues(rule).findFirst().orElse(null);
    }

    /**
     * Create a sub-context
     *
     * @param element
     * @return
     */
    public XmlContext create(Element element) {
        return new XmlContext(this, element);
    }

    /**
     * Create a root context from array of bytes
     *
     * @param bytes
     * @return
     * @throws JDOMException
     * @throws IOException
     */
    public static XmlContext create(byte[] bytes) throws JDOMException, IOException {
        SAXBuilder sb = new SAXBuilder();
        Document document = sb.build(new StringReader(new String(bytes)));
        return new XmlContext(document.getRootElement()/*.detach()*/);
    }
}

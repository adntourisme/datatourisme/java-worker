/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.atomic.operation.map;

import fr.datatourisme.producer.processor.atomic.operation.AbstractMapOperation;
import org.apache.commons.lang3.StringEscapeUtils;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.safety.Whitelist;

import java.util.stream.Stream;

public class Html2Text extends AbstractMapOperation {
    @Override
    public Stream<String> process(Stream<String> stream) {
        //return stream.map(html -> Jsoup.parse(html).text());
        return stream.map(this::parse);
    }

    /**
     * @param html
     * @return
     */
    private String parse(String html) {
        Document jsoupDoc = Jsoup.parse(html);

        //set pretty print to false, so \n is not removed
        jsoupDoc.outputSettings(new Document.OutputSettings().prettyPrint(false));

        //select all <br> tags and append \n after that
        jsoupDoc.select("br").after("\\n");

        //select all <p> tags and prepend \n before that
        jsoupDoc.select("p").before("\\n");

        //get the HTML from the document, and retaining original new lines
        String str = jsoupDoc.html().replaceAll("\\\\n", "\n");

        // clean
        str = Jsoup.clean(str, "", Whitelist.none(), new Document.OutputSettings().prettyPrint(false));

        // unescape html
        str = StringEscapeUtils.unescapeHtml4(str);

        return str;
    }
}

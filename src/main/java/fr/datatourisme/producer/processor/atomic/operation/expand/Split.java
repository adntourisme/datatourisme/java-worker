/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.atomic.operation.expand;

import fr.datatourisme.producer.processor.atomic.operation.AbstractExpandOperation;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.stream.Stream;

public class Split extends AbstractExpandOperation {
    public Split(Object[] args) {
        super(args);
    }

    public Stream<String> process(String string) {
        String separator = args.length > 0 ? args[0].toString() : ",";
        boolean regex = args.length > 1 ? (Boolean)args[1] : false;
        if(!regex) {
            return Arrays.stream(StringUtils.splitByWholeSeparatorPreserveAllTokens(string, separator));
        } else {
            return Arrays.stream(string.split(separator));
        }
    }
}

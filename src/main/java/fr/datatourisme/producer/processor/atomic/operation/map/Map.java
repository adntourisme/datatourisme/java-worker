/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.atomic.operation.map;

import fr.datatourisme.producer.processor.atomic.operation.AbstractMapOperation;

import java.util.stream.Stream;

/**
 * Map values to another values
 */
public class Map extends AbstractMapOperation {
    public Map(Object[] args) {
        super(args);
    }

    @Override
    public Stream<String> process(Stream<String> stream) {
        java.util.Map<String, Object> map = (java.util.Map<String, Object>) args[0];
        Boolean keep = args.length > 1 ? Boolean.valueOf(args[1].toString()) : false;
        return stream.map(s -> {
            Object val = map.getOrDefault(s, keep ? s : null);
            if(val == null) {
                return null;
            }
            return val.toString();
        });
    }
}

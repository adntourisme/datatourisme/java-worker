/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.atomic.deserializer;

import com.google.gson.*;
import fr.datatourisme.producer.processor.atomic.AtomicChain;
import fr.datatourisme.producer.processor.atomic.operation.AbstractOperation;
import fr.datatourisme.producer.processor.atomic.operation.expand.Split;
import fr.datatourisme.producer.processor.atomic.operation.map.*;
import fr.datatourisme.producer.processor.atomic.operation.reduce.*;

import java.lang.reflect.Type;
import java.util.HashMap;

public class AtomicChainDeserializer implements JsonDeserializer<AtomicChain> {

    java.util.Map<String, Class<? extends AbstractOperation>> operationsMap;

    public AtomicChainDeserializer() {
        operationsMap = new HashMap<>();
        // expand
        operationsMap.put("split", Split.class);
        // map
        operationsMap.put("concat", Concat.class);
        operationsMap.put("default", Default.class);
        operationsMap.put("distinct", Distinct.class);
        operationsMap.put("lowercase", LowerCase.class);
        operationsMap.put("map", Map.class);
        operationsMap.put("regex", Regex.class);
        operationsMap.put("replace", Replace.class);
        operationsMap.put("html2text", Html2Text.class);
        // reduce
        operationsMap.put("count", Count.class);
        operationsMap.put("first", First.class);
        operationsMap.put("index", Index.class);
        operationsMap.put("join", Join.class);
        operationsMap.put("last", Last.class);
    }

    @Override
    public AtomicChain deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
    {
        AtomicChain chain = new AtomicChain();
        JsonArray root = json.getAsJsonArray();
        root.forEach(elmt -> {
            JsonObject obj = elmt.getAsJsonObject();
            java.util.Map.Entry<String, JsonElement> es = obj.entrySet().stream().findFirst().orElse(null);
            if(es == null) {
                return;
            }

            String operation = es.getKey();
            JsonArray args = es.getValue().getAsJsonArray();
            Class<? extends AbstractOperation> opClass = operationsMap.get(operation);
            if(opClass == null) {
                return;
            }

            JsonObject opObj = new JsonObject();
            opObj.add("args", args);
            AbstractOperation op = context.deserialize(opObj, opClass);
            chain.addOperation(op);
        });
        return chain;
    }
}

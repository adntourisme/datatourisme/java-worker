/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.atomic.operation.map;

import fr.datatourisme.producer.processor.atomic.operation.AbstractMapOperation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

public class Regex extends AbstractMapOperation {
    public Regex(Object[] args) {
        super(args);
    }

    @Override
    public Stream<String> process(Stream<String> stream) {
        String pattern = args[0].toString();
//        String template = args.length > 1 ? args[1].toString() : "$1";
        Pattern p = Pattern.compile(pattern);
        return stream.map(s -> {
            Matcher m = p.matcher(s);
            if(m.find()) {
                if(m.groupCount() > 0) {
                    return m.group(1);
                } else {
                    return m.group(0);
                }
            }
            return null;
        });
    }
}

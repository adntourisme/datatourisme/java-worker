/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.thesaurus;

import fr.datatourisme.ontology.Ontology;
import fr.datatourisme.ontology.OntologyClass;
import fr.datatourisme.ontology.OntologyIndividual;
import fr.datatourisme.ontology.OntologyResource;
import fr.datatourisme.vocabulary.Datatourisme;
import org.apache.jena.ext.com.google.common.collect.HashBasedTable;
import org.apache.jena.ext.com.google.common.collect.Table;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ThesaurusAlignment {

    protected Map<String, Set<OntologyResource>> reverseMap = new HashMap<>();

    protected Ontology ontology;

    // cached container
    Table<String, String, Set<OntologyResource>> cache = HashBasedTable.create();

    public ThesaurusAlignment(Ontology ontology) {
        this.ontology = ontology;
    }

    public Ontology getOntology() {
        return ontology;
    }

    /**
     * @param map
     */
    public void setMap(Map<String, Set<String>> map) {
        this.reverseMap = this.reverseMap(this.expandMap(map));
    }

    /**
     * Resolve a definition, given a value and a class uri using the cache
     *
     * @param value
     * @param classUri
     * @return
     */
    public Set<OntologyResource> resolve(String value, String classUri) {
        // check cache
        if(cache.contains(value, classUri)) {
            return cache.get(value, classUri);
        }
        Set<OntologyResource> list = doResolve(value, classUri);
        if(list != null) {
            cache.put(value, classUri, list);
        }
        return list;
    }

    /**
     * @param value
     * @param resource
     * @return
     */
    public Set<OntologyResource> resolve(String value, Resource resource) {
        return resolve(value, resource.getURI());
    }

    /**
     * Expand the map with ontology terms
     *
     * @param map
     * @return
     */
    private Map<OntologyResource, Set<String>> expandMap(Map<String, Set<String>> map) {
        Map<OntologyResource, Set<String>> finalMap = new HashMap<>();

        // resolve resource from ontology
        map.forEach((uri, values) -> {
            OntologyResource res = ontology.getResource(uri);
            if(res != null) {
                finalMap.put(res, values);
            }
        });

        Stream<OntologyResource> stream = Stream.concat(
            ontology.listIndividuals(),
            ontology.getClass(Datatourisme.PointOfInterest).listSubClasses(false)
        );

        // add some info from individuals
        stream.forEach(i -> {
            Set<String> list = finalMap.computeIfAbsent(i, s -> new HashSet<>());

            // add label
            if(i.getLabel() != null) {
                list.add(i.getLabel());
            }

            // add alignableProperties
            if(i instanceof OntologyIndividual) {
                ((OntologyIndividual) i).listTypes(false).forEach(c -> {
                    c.listAlignableProperties().forEach(p -> {
                        ((OntologyIndividual) i).listPropertyValues(p)
                            .filter(RDFNode::isLiteral)
                            .map(RDFNode::asLiteral)
                            .forEach(l -> list.add(l.toString()));
                    });
                });
            }

            finalMap.put(i, list);
        });

        return finalMap;
    }

    /**
     * Build the reverse map
     *
     * @param map
     * @return
     */
    private Map<String, Set<OntologyResource>> reverseMap(Map<OntologyResource, Set<String>> map) {
        Map<String, Set<OntologyResource>> reverseMap = new HashMap<>();
        map.forEach((res, values) -> {
            values.forEach(value -> {
                reverseMap.computeIfAbsent(value.toLowerCase(), s -> new HashSet<>())
                    .add(res);
            });
        });
        return reverseMap;
    }

    /**
     * Resolve a definition, given a value and a class uri (only subclasses of this class)
     *
     * @param value
     * @param classUri
     * @return
     */
    private Set<OntologyResource> doResolve(String value, String classUri) {

        // define the target
        OntologyClass targetClass = ontology.getClass(classUri);
        if(targetClass == null) {
            return null;
        }

        // Try to find the value in the reverse map
        Set<OntologyResource> set = this.reverseMap.get(value.toLowerCase());
        if(set != null) {
            return set.stream()
                .filter(res -> {
                    if(res instanceof OntologyClass) {
                        return ((OntologyClass) res).hasSuperClass(targetClass);
                    } else if(res instanceof OntologyIndividual) {
                        return ((OntologyIndividual) res).isInstanceOf(targetClass);
                    }
                    return false;
                }).collect(Collectors.toSet());
        }

        return new HashSet<>();
    }

}

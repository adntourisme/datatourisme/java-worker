/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.thesaurus;

import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import fr.datatourisme.ontology.Ontology;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.*;
import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.Map;

public class ThesaurusAlignmentFactory {

    @Autowired
    Ontology ontologyAlignment;

    public ThesaurusAlignment create() {
        return new ThesaurusAlignment(ontologyAlignment);
    }

    public ThesaurusAlignment createFromJson(Reader reader) {
        ThesaurusAlignment thesaurusAlignment = new ThesaurusAlignment(ontologyAlignment);
        if(reader != null) {
            Type hashSet = new TypeToken<Map<String, HashSet<String>>>(){}.getType();
            Map map = new GsonBuilder().create().fromJson(reader, hashSet);
            thesaurusAlignment.setMap(map);
        }
        return thesaurusAlignment;
    }

    public ThesaurusAlignment createFromJson(File file) throws FileNotFoundException {
        FileReader reader = new FileReader(file);
        return createFromJson(reader);
    }

    public ThesaurusAlignment createFromJson(InputStream in) {
        Reader reader = new InputStreamReader(in);
        return createFromJson(reader);
    }
}

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.xml.xpath;

import com.google.gson.annotations.SerializedName;

import java.util.HashMap;
import java.util.Map;

public class XPathMapperItem {
    private String name;
    private String type;
    private String value;
    private String xpath;

    private Integer minOccurs = null;
    private Integer maxOccurs = null;
    private Boolean empty = true;

    @SerializedName("parent")
    private Integer parentIndex = null;

    private transient XPathMapperItem parent;
    private transient Map<XPathMapperItem, Integer> childrenCount = new HashMap<>();

    public XPathMapperItem(String name, String type, XPathMapperItem parent, Boolean relativeMode) {
        this.name = name;
        this.type = type;
        this.parent = parent;
        if(parent != null) {
            this.xpath = parent.getXpath() + "/" + this.name;
        } else {
            this.xpath = (relativeMode ? "" : "/") + this.name;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
        setEmpty(false);
    }

    public Integer getMinOccurs() {
        return minOccurs;
    }

    public void setMinOccursIfApplicable(int count) {
        if(minOccurs == null || minOccurs > count) {
            minOccurs = count;
        }
    }

    public Integer getMaxOccurs() {
        return maxOccurs;
    }

    public void setMaxOccursIfApplicable(int count) {
        if(maxOccurs == null || maxOccurs < count) {
            maxOccurs = count;
        }
    }

    public Boolean isEmpty() {
        return empty != null ? empty : false;
    }

    public void setEmpty(Boolean empty) {
        this.empty = !empty ? null : empty;
    }

    public XPathMapperItem getParent() {
        return parent;
    }

    public String getXpath() {
        return xpath;
    }

    void setParentIndex(int parentIndex) {
        this.parentIndex = parentIndex;
    }

    Map<XPathMapperItem, Integer> getChildrenCount() {
        return childrenCount;
    }

    void incrementChildrenCount(XPathMapperItem child) {
        childrenCount.put(child, childrenCount.getOrDefault(child, 0)+1);
    }

    @Override
    public String toString() {
        return "XPathMapperItem{" +
                "name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", value='" + value + '\'' +
                ", xpath='" + xpath + '\'' +
                ", minOccurs=" + minOccurs +
                ", maxOccurs=" + maxOccurs +
                ", empty=" + isEmpty() +
                ", parentIndex=" + parentIndex +
                '}';
    }
}

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.task;

import fr.datatourisme.dataset.query.SparqlQueryExecutionService;
import fr.datatourisme.ontology.Ontology;
import fr.datatourisme.ontology.OntologyClass;
import fr.datatourisme.ontology.OntologyProperty;
import fr.datatourisme.producer.processor.URIGenerator;
import fr.datatourisme.producer.processor.alignment.Alignment;
import fr.datatourisme.producer.processor.alignment.AlignmentFactory;
import fr.datatourisme.producer.processor.alignment.transformer.LiteralTransformer;
import fr.datatourisme.producer.processor.report.ResourceReport;
import fr.datatourisme.producer.processor.thesaurus.ThesaurusAlignment;
import fr.datatourisme.producer.processor.thesaurus.ThesaurusAlignmentFactory;
import fr.datatourisme.producer.processor.xml.XmlProcessor;
import fr.datatourisme.producer.xml.datasource.XmlItem;
import fr.datatourisme.producer.xml.datasource.XmlItemDao;
import fr.datatourisme.task.AbstractTask;
import fr.datatourisme.vocabulary.Datatourisme;
import fr.datatourisme.vocabulary.DatatourismeMetadata;
import fr.datatourisme.vocabulary.Schema;
import fr.datatourisme.worker.Payload;
import fr.datatourisme.worker.exception.JobException;
import fr.datatourisme.worker.logger.reporter.ReportEntry;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.filefilter.WildcardFileFilter;
import org.apache.jena.arq.querybuilder.ConstructBuilder;
import org.apache.jena.arq.querybuilder.SelectBuilder;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.graph.Triple;
import org.apache.jena.ontology.OntProperty;
import org.apache.jena.query.Query;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.sparql.core.Var;
import org.apache.jena.vocabulary.DC;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.apache.log4j.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.io.File;
import java.io.FileFilter;
import java.io.FileOutputStream;
import java.io.Reader;
import java.util.*;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class ProduceRDFTask extends AbstractTask {

    private File db;
    private File outputDir;
    private ThesaurusAlignment thesaurusAlignment;
    private Alignment alignment;
    private String checksum;

    private int fluxIdentifier;
    private int organizationIdentifier;

    private ReportEntry reportEntry = new ReportEntry();
    Map<String, String> currentSourceChecksumMap = new HashMap<>();

    // stats
    private Stats stats = new Stats();

    @Autowired
    protected ThreadPoolTaskExecutor threadPoolExecutor;

    @Autowired
    private ThesaurusAlignmentFactory thesaurusAlignmentFactory;

    @Autowired
    private AlignmentFactory alignmentFactory;

    @Autowired
    private XmlProcessor xmlProcessor;

    @Autowired
    private SparqlQueryExecutionService queryExecutionService;

    @Autowired
    private SparqlQueryExecutionService extraQueryExecutionService;

    @Autowired
    private Ontology ontologyAlignment;

    @Override
    public String getMachineName() {
        return "produce_rdf";
    }

    @Override
    protected String getLocalizedName() {
        return "Génération des données RDF";
    }

    @Override
    protected ReportEntry execute() throws Exception {
        outputDir = storage.dir("rdf").toFile();
        reportEntry.getDuration().start();

        // if db not exists, return
        if(db == null || !db.exists()) {
            logger.warn("Aucune base de donnée n'a été trouvée");
            return null;
        }

        if(alignment.getRules().size() == 0) {
            logger.warn("Aucune définition de règle d'aligmenent n'a été trouvée");
            return null;
        }

        // set the UriGenerator producer identifier
        alignment.getUriGenerator().setProducerIdentifier(String.valueOf(organizationIdentifier));

        // #1 : delete RDF
        outputDir.mkdirs();
        FileUtils.cleanDirectory(outputDir);

        // populate current hash map
//        currentSourceChecksumMap = getCurrentSourceChecksumMap();

        // process RDF
        logger.info("Démarrage de l'alignement");
        XmlItemDao dao = XmlItemDao.instanciate(db);

        // find linguistic dbs
        Map<String, XmlItemDao> i18nDaoMap = new HashMap<>();
        String extension = FilenameUtils.getExtension(db.getName());
        String baseName = FilenameUtils.removeExtension(db.getName());
        FileFilter fileFilter = new WildcardFileFilter(baseName + ".*." + extension);
        File[] files = db.getParentFile().listFiles(fileFilter);
        for(final File db: files) {
            String lang = db.getName().split("\\.")[1];
            i18nDaoMap.put(lang, XmlItemDao.instanciate(db));
        }

        // process each item
        List<Future<ReportEntry>> resultList = new ArrayList<>();
        dao.findAll().forEachRemaining(xmlItem -> {

            // collect i18n items
            Map<String, XmlItem> i18nItems = i18nDaoMap.entrySet().stream()
                .collect(HashMap::new, (m,e)->m.put(e.getKey(), e.getValue().findById(xmlItem.getIdentifier())), HashMap::putAll);
            i18nItems.entrySet().removeIf(e -> e.getValue() == null);

            // prepare & go !
            ProcessXmlItemThreadTask threadTask  = new ProcessXmlItemThreadTask(xmlItem, i18nItems, dao);
            Future<ReportEntry> result = threadPoolExecutor.submit(threadTask);
            resultList.add(result);

        });

        for(Future<ReportEntry> future : resultList) {
            ReportEntry subReport = future.get();
            if(subReport != null) {
                reportEntry.addReport(subReport);
            }
        }

        threadPoolExecutor.shutdown();

        //dao.findAll().forEachRemaining(this::processXmlItem);
        logger.info("Fin de l'alignement");

        // add stats
        reportEntry.data("stats", stats);

        reportEntry.getDuration().end();
        reportEntry.getDuration().setAvg(Math.round(
            reportEntry.getReports().stream()
                .filter(r -> r != null && r.getDuration().getTotal() != null)
                .mapToInt(r -> r.getDuration().getTotal().intValue()).average().orElse(0)
        ));
        return reportEntry;
    }

    /**
     * Main thread task form processing
     */
    class ProcessXmlItemThreadTask implements Callable<ReportEntry>
    {
        private XmlItem xmlItem;
        private XmlItemDao dao;
        private Map<String, XmlItem> i18nItems;

        public ProcessXmlItemThreadTask(XmlItem xmlItem, Map<String, XmlItem> i18nItems, XmlItemDao dao) {
            this.xmlItem = xmlItem;
            this.dao = dao;
            this.i18nItems = i18nItems;
        }

        @Override
        public ReportEntry call() throws Exception {
            ReportEntry.Duration duration = new ReportEntry.Duration();

            // calculate new checksum
            String newSourceChecksum = DigestUtils.md5Hex(xmlItem.getDigest() + checksum);

//          // get old checksum
//          String oldSourceChecksum = currentSourceChecksumMap.get(xmlItem.getIdentifier());

            stats.total.getAndIncrement();

//          if(newSourceChecksum.equals(oldSourceChecksum)) {
//              // the item didnt change, don't process him
//              logger.debug("POI ignoré : " + xmlItem.getIdentifier());
//              stats.skip++;
//              return;
//          }

            // i18n content
            Map<String, byte[]> i18nXmls = i18nItems.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> e.getValue().getBytes()));

            logger.info("Alignement du POI " + xmlItem.getIdentifier());
            try {
                ResourceReport resourceReport = xmlProcessor.process(alignment, xmlItem.getBytes(), i18nXmls);
                if(resourceReport.isValid()) {
                    stats.valid.getAndIncrement();
                    xmlItem.setStatus(true);

                    // get main objects
                    Resource res = resourceReport.getResource();
                    Model model = res.getModel();

                    // add some properties
                    String fingerprint = DigestUtils.md5Hex(URIGenerator.getResourceUUID(res));
                    res.addProperty(DatatourismeMetadata.fingerprint, fingerprint);
                    res.addProperty(DatatourismeMetadata.sourceChecksum, newSourceChecksum);
                    res.addProperty(DatatourismeMetadata.hasFluxIdentifier, ResourceFactory.createTypedLiteral(String.valueOf(fluxIdentifier), XSDDatatype.XSDint));
                    res.addProperty(DatatourismeMetadata.hasOrganizationIdentifier, ResourceFactory.createTypedLiteral(String.valueOf(organizationIdentifier), XSDDatatype.XSDint));

                    // add triples from extra dataset
                    completeModelFromExtraDataset(model);

                    // save to file
                    File outputFile = new File(outputDir, DigestUtils.md5Hex(xmlItem.getIdentifier()) + ".nt");
                    RDFDataMgr.write(new FileOutputStream(outputFile), model, RDFFormat.NTRIPLES);

                } else {
                    xmlItem.setStatus(false);
                    stats.invalid.getAndIncrement();
                }

                // save status
                dao.update(xmlItem);

                // fill anomalies stats
                stats.anomalies.total.addAndGet(resourceReport.getReports().size());
                resourceReport.getReports().forEach(r -> {
                    if(r.isError()) {
                        stats.anomalies.error.getAndIncrement();
                    } else {
                        stats.anomalies.warning.getAndIncrement();
                    }
                });

                // add subreport
                ReportEntry subReportEntry = createReportEntry(resourceReport);
                subReportEntry.setDuration(duration.end());

                return subReportEntry;

            } catch (Exception e) {
                logger.error(e.getMessage());
            }

            return null;
        }

        /**
         * Complete model from extra Dataset
         *
         * @param model
         */
        private void completeModelFromExtraDataset(Model model) {
//            /*
//            CONSTRUCT { ?s ?p ?o } WHERE { GRAPH ?g {
//  	            ?src (!<>)* ?s .
//                ?s ?p ?o .
//            } }
//            */
//            List<Resource> subjects = model.listSubjects().toList();
//            Triple triple = new Triple(Var.alloc("s"), Var.alloc("p"), Var.alloc("o"));
//            ConstructBuilder builder = new ConstructBuilder()
//                .addConstruct(triple)
//                .addGraph(Var.alloc("g"), new ConstructBuilder()
//                    .addWhere(Var.alloc("src"), "(!<>)*", Var.alloc("s"))
//                    .addWhere(triple)
//                )
//                .addValueVar("src", subjects.toArray());
//
//            extraQueryExecutionService.construct(builder.build(), model);

            // recursive version, for Blazegraph poor performances
            Model m;
            List<Resource> subjects = model.listSubjects().toList();
            do {
                Triple triple = new Triple(Var.alloc("s"), Var.alloc("p"), Var.alloc("o"));
                ConstructBuilder builder = new ConstructBuilder()
                        .addConstruct(triple)
                        .addGraph(Var.alloc("g"), triple)
                        .addValueVar("s", subjects.toArray());
                m = extraQueryExecutionService.construct(builder.build());
                model.add(m);
                subjects = m.listObjects().toList().stream()
                    .filter(RDFNode::isResource)
                    .map(RDFNode::asResource)
                    .filter(o -> !model.listSubjects().toList().contains(o))
                    .collect(Collectors.toList());
            } while (subjects.size() > 0);
        }

        /**
         * @param resourceReport
         * @return ReportEntry
         */
        private ReportEntry createReportEntry(ResourceReport resourceReport) {
            Map<String, String> reportMap = new HashMap<>();
            Resource resource = resourceReport.getResource();

            // required properties
            reportMap.put("uri", resource.getURI());
            reportMap.put("id", resource.getProperty(DC.identifier).getString());

            // get class list
            List<Statement> listStmt = resource.listProperties(RDF.type).toList();
            List<OntologyClass> listOntClass = listStmt.stream()
                .map(stmt -> stmt.getObject().asResource())
                .map(res -> ontologyAlignment.getClass(res))
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

            // find principal type (direct subclass of PointOfInterest)
            OntologyClass typeClass = listOntClass.stream()
                .filter(c -> c.hasSuperClass(Datatourisme.PointOfInterest, true))
                .findFirst().orElse(null);

            if(typeClass != null) {
                reportMap.put("type", typeClass.getLabel());

                // find secondary type (direct subclass of principal type)
                OntologyClass type2Class = listOntClass.stream()
                        .filter(c -> c.hasSuperClass(typeClass, true))
                        .findFirst().orElse(null);
                if(type2Class != null) {
                    reportMap.put("type2", type2Class.getLabel());
                }
            }

            // label
            Statement label = resource.getProperty(RDFS.label, LiteralTransformer.defaultLang);
            if(label != null) {
                reportMap.put("label", label.getString());
            }

            // city
            // @todo optimize this shit
            Optional.ofNullable(resource.getProperty(Datatourisme.isLocatedAt)).ifPresent(isLocatedAt -> {
                Optional.ofNullable(isLocatedAt.getObject().asResource().getProperty(Schema.address)).ifPresent(address -> {
                    // schema:postalCode
                    Optional.ofNullable(address.getObject().asResource().getProperty(Schema.postalCode)).ifPresent(postalCode -> {
                        reportMap.put("postalCode", postalCode.getString());
                    });
                    // city name
                    Optional.ofNullable(address.getObject().asResource().getProperty(Datatourisme.hasAddressCity)).ifPresent(hasAddressCity -> {
                        Optional.ofNullable(ontologyAlignment.getIndividual(hasAddressCity.getObject().asResource())).ifPresent(individual -> {
                            Optional.ofNullable(individual.getLabel()).ifPresent(city -> {
                                reportMap.put("city", city);
                            });
                        });
                    });
                });
            });

            // creator
            // @todo optimize this shit
            Optional.ofNullable(resource.getProperty(Datatourisme.hasBeenCreatedBy)).ifPresent(hasBeenCreatedBy -> {
                Optional.ofNullable(hasBeenCreatedBy.getObject().asResource().getProperty(Schema.legalName)).ifPresent(legalName -> {
                    reportMap.put("creator", legalName.getString());
                });
            });

            ReportEntry reportEntry = new ReportEntry().setLevel(Level.INFO).data(reportMap);
            resourceReport.getReports().forEach(r -> {
                ReportEntry anomaly = new ReportEntry(r.getMessage());
                anomaly.setLevel(r.isError() ? Level.ERROR : Level.WARN);
                if(r.getTarget() != null) {
                    // anomaly.data("property", ontologyAlignment.shortForm(r.getTarget()));
                    Map<String, String> propMap = new HashMap<>();
                    OntProperty ontProperty = ontologyAlignment.getOntModel().createOntProperty(r.getTarget());
                    if(ontProperty != null) {
                        OntologyProperty property = ontologyAlignment.createProperty(ontProperty);
                        propMap.put("uri", property.getShortURI());
                        propMap.put("label", property.getLabel());
                    } else {
                        propMap.put("uri", r.getTarget());
                    }
                    anomaly.data("property", propMap);
                }
                // update parent level
                if(anomaly.getLevel().isGreaterOrEqual(reportEntry.getLevel())) {
                    reportEntry.setLevel(anomaly.getLevel());
                }
                reportEntry.addReport(anomaly);
            });

            return reportEntry;
        }
    }

    /***
     * Build from the dataset a hash map id -> hash
     *
     * @return
     */
    private Map<String, String> getCurrentSourceChecksumMap() {
        Map<String, String> map = new HashMap<>();
        SelectBuilder sb = new SelectBuilder()
                .addVar("?id").addVar( "?md5" )
                .addWhere("?poi", "a", Datatourisme.PointOfInterest)
                .addWhere("?poi", DC.identifier, "?id")
                .addWhere("?poi", DatatourismeMetadata.sourceChecksum, "?md5")
                .addWhere("?poi", DatatourismeMetadata.hasFluxIdentifier, fluxIdentifier);
        Query q = sb.build();
        ResultSet rs = queryExecutionService.select(q);
        rs.forEachRemaining(s -> {
            map.put(s.getLiteral("id").getString(), s.getLiteral("md5").getString());
        });
        return map;
    }

    @Override
    public Class<? extends AbstractTask> getNextTask() {
        return PersistRDFTask.class;
    }

    public ProduceRDFTask setDb(File db) {
        this.db = db;
        return this;
    }

    public ProduceRDFTask setThesaurusAlignment(ThesaurusAlignment thesaurusAlignment) {
        this.thesaurusAlignment = thesaurusAlignment;
        return this;
    }

    public ProduceRDFTask setAlignment(Alignment alignment) {
        this.alignment = alignment;
        return this;
    }

    public ProduceRDFTask setChecksum(String checksum) {
        this.checksum = checksum;
        return this;
    }

    public ProduceRDFTask setFluxIdentifier(int fluxIdentifier) {
        this.fluxIdentifier = fluxIdentifier;
        return this;
    }

    public ProduceRDFTask setOrganizationIdentifier(int organizationIdentifier) {
        this.organizationIdentifier = organizationIdentifier;
        return this;
    }

    @Override
    public void initFromPayload(Payload payload) throws JobException {
        setDb(storage.file("poi.db"));
        setChecksum(payload.getRequired("checksum").toString());

        setFluxIdentifier((Integer) payload.getRequired("flux"));
        setOrganizationIdentifier((Integer) payload.getRequired("organization"));

        // get thesaurus
        Reader reader = payload.getJsonOrStream("thesaurus");
        setThesaurusAlignment(thesaurusAlignmentFactory.createFromJson(reader));

        // get alignment
        reader = payload.getRequiredJsonOrStream("alignment");
        Alignment alignment = alignmentFactory.createFromJson(thesaurusAlignment, reader);
        setAlignment(alignment);
    }

    /**
     * Stats static class
     */
    public static class Stats  {
        public static class AnomalyStats {
            public AtomicInteger total = new AtomicInteger(0);
            public AtomicInteger error = new AtomicInteger(0);
            public AtomicInteger warning = new AtomicInteger(0);
        }
        public AtomicInteger total = new AtomicInteger(0);
        public AtomicInteger skip = new AtomicInteger(0);
        public AtomicInteger invalid = new AtomicInteger(0);
        public AtomicInteger valid = new AtomicInteger(0);
        public AnomalyStats anomalies = new AnomalyStats();
    }

}

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.worker;

import com.dinstone.beanstalkc.BeanstalkClientFactory;
import fr.datatourisme.consumer.task.ExecuteQueryTask;
import fr.datatourisme.consumer.task.ProcessGraphResultTask;
import fr.datatourisme.consumer.task.ProcessTupleResultTask;
import fr.datatourisme.consumer.task.PublishDatagouvTask;
import fr.datatourisme.producer.task.*;
import fr.datatourisme.task.ReportingTask;
import fr.datatourisme.webapp.api.WebappApiClient;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
public class WorkerConfiguration {

    @Value("${beanstalkd.host}")
    private String beanstalkdHost;

    @Value("${beanstalkd.port}")
    private int beanstalkdPort;

    @Bean @Lazy
    public BeanstalkClientFactory getBeanstalkClientFactory() {
        com.dinstone.beanstalkc.Configuration configuration = new com.dinstone.beanstalkc.Configuration();
        configuration.setServiceHost(beanstalkdHost);
        configuration.setServicePort(beanstalkdPort);
        return new BeanstalkClientFactory(configuration);
    }

    @Bean @Lazy
    public Worker getWorker() {
        return new Worker();
    }

    @Bean @Lazy
    @Scope("prototype")
    public ThreadPoolTaskExecutor getThreadTaskExecutor() {
        int nbThreadPool = Runtime.getRuntime().availableProcessors();
        ThreadPoolTaskExecutor tpte = new ThreadPoolTaskExecutor();
        tpte.setCorePoolSize(nbThreadPool);
        tpte.setMaxPoolSize(nbThreadPool*2);
        //tpte.setQueueCapacity(nbThreadPool*100);
        return tpte;
    }

    /**
     * TUBES FACTORIES
     */
    @Bean(name = "producer") @Lazy
    public Tube createProducerTube(@Qualifier("producer.storage") Storage producerStorage, @Qualifier("producer.apiclient") WebappApiClient apiClient) {
        return new Tube(producerStorage, apiClient, DownloadTask.class);
    }

    @Bean(name = "consumer") @Lazy
    public Tube createConsumerTube(@Qualifier("consumer.storage") Storage consumerStorage, @Qualifier("consumer.apiclient") WebappApiClient apiClient) {
        return new Tube(consumerStorage, apiClient, ExecuteQueryTask.class);
    }

    /**
     * STORAGE FACTORIES
     */
    @Bean(name = "producer.storage") @Lazy
    public Storage createProducerStorage(Storage storage) {
        return storage.dir("producer");
    }

    @Bean(name = "consumer.storage") @Lazy
    public Storage createConsumerStorage(Storage storage) {
        return storage.dir("consumer");
    }

    /**
     * API CLIENT FACTORIES
     */
    @Bean(name = "producer.apiclient") @Lazy
    public WebappApiClient createProducerApiClient(@Value( "${webapp.producer.api.url}" ) String apiUrl) {
        return apiUrl != null ? new WebappApiClient(apiUrl) : null;
    }

    @Bean(name = "consumer.apiclient") @Lazy
    public WebappApiClient createConsumerApiClient(@Value( "${webapp.consumer.api.url}" ) String apiUrl) {
        return apiUrl != null ? new WebappApiClient(apiUrl) : null;
    }

    /**
     * SHARED TASKS
     */
    @Bean(name = "reporting") @Lazy
    @Scope("prototype")
    public ReportingTask createReportingTask() {
        return new ReportingTask();
    }

    /**
     * PRODUCER TASKS
     */
    @Bean(name = "download") @Lazy
    @Scope("prototype")
    public DownloadTask createDownloadTask() {
        return new DownloadTask();
    }

    @Bean(name = "process_xml") @Lazy
    @Scope("prototype")
    public ProcessXmlTask createProcessXmlTask() {
        return new ProcessXmlTask();
    }

    @Bean(name = "preprocess_xml") @Lazy
    @Scope("prototype")
    public PreprocessXmlTask createPreprocessXmlTask() {
        return new PreprocessXmlTask();
    }

    @Bean(name = "produce_rdf") @Lazy
    @Scope("prototype")
    public ProduceRDFTask createProduceRDFTask() {
        return new ProduceRDFTask();
    }

    @Bean(name = "persist_rdf") @Lazy
    @Scope("prototype")
    public PersistRDFTask createPersistRDFTask() {
        return new PersistRDFTask();
    }

    @Bean(name = "delete") @Lazy
    @Scope("prototype")
    public DeleteTask createDeleteTask() {
        return new DeleteTask();
    }

    /**
     * CONSUMER TASKS
     */
    @Bean(name = "execute_query") @Lazy
    @Scope("prototype")
    public ExecuteQueryTask createExecuteQueryTask() {
        return new ExecuteQueryTask();
    }

    @Bean(name = "process_construct_result") @Lazy
    @Scope("prototype")
    public ProcessGraphResultTask createProcessGraphResultTask() {
        return new ProcessGraphResultTask();
    }

    @Bean(name = "process_select_result") @Lazy
    @Scope("prototype")
    public ProcessTupleResultTask createProcessTupleResultTask() {
        return new ProcessTupleResultTask();
    }

    @Bean(name = "publish_datagouv") @Lazy
    @Scope("prototype")
    public PublishDatagouvTask createPublishDataGouvTask() {
        return new PublishDatagouvTask();
    }

}

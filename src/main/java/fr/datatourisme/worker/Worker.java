/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.worker;

import com.dinstone.beanstalkc.BeanstalkClient;
import com.dinstone.beanstalkc.BeanstalkClientFactory;
import com.dinstone.beanstalkc.ConnectionException;
import com.dinstone.beanstalkc.JobProducer;
import fr.datatourisme.task.AbstractTask;
import fr.datatourisme.webapp.api.message.JobReportMessage;
import fr.datatourisme.webapp.api.message.JobStatusMessage;
import fr.datatourisme.worker.exception.JobException;
import fr.datatourisme.worker.exception.TemporaryJobException;
import fr.datatourisme.worker.logger.reporter.ReportEntry;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

public class Worker {
    private static final Logger logger = Logger.getLogger(Worker.class);

    @Autowired
    private ApplicationContext context;

    @Autowired
    private BeanstalkClientFactory beanstalkFactory;

    @Autowired
    private ThreadPoolTaskExecutor threadPoolExecutor;

    public int run(String[] tubes) {
        com.dinstone.beanstalkc.Job bsJob;
        Job job;

        while (true) {
            logger.info("En attente de job...");
            BeanstalkClient consumer = (BeanstalkClient) beanstalkFactory.createJobConsumer(tubes);

            try {
                // try get the job from beanstalk
                bsJob = consumer.reserveJob(0);
            } catch(ConnectionException e) {
                // erreur de connexion à beanstalkd
                logger.error("Impossible de se connecter à la pile de jobs : " + e.getMessage());
                try {
                    logger.info("Nouvelle tentative dans 10s");
                    Thread.sleep(10000);
                    continue;
                } catch (InterruptedException e1) {
                    return e.hashCode();
                }
            }

            try {
                // create tube
                Map<String, String> stats = consumer.statsJob(bsJob.getId());
                String tubeName = stats.get("tube");
                Tube tube = context.getBean(tubeName, Tube.class);

                // create job from beanstalk job
                job = new Job(tube, consumer);

                try {

                    // initialize & lock job
                    job.initializeFromBsJob(bsJob);
                    if(!job.lock()) {
                        logger.info("Le job est vérouillé par un autre processus");
                        consumer.releaseJob(job.getId(), 0, 5);
                        continue;
                    }

                    // create task
                    AbstractTask task = job.createTask();

                    // contact api
                    job.callWebappApi("status/start", JobStatusMessage.class);

                    // start the job
                    logger.info("Début du job " + job.getId());
                    threadExecute(task::run, job::touch, 5000);
                    logger.info("Fin du job " + job.getId());

                    // put the next job
                    Class<? extends AbstractTask> nextTask = job.getNextTask();
                    if(nextTask != null) {
                        logger.debug("Add the next job to " + tubeName + " queue");
                        JobProducer producer = beanstalkFactory.createJobProducer(tubeName);
                        job.getPayload().set("task", context.getBeanNamesForType(nextTask)[0]);
                        long jobId = producer.putJob(nextTask.getField("defaultPriority").getInt(null), 0, 10*60, job.getPayload().toJson().getBytes());
                        job.lock(jobId);
                        producer.close();
                    } else {
                        // unlock
                        job.unlock();
                    }

                    logger.debug("Deleting job...");
                    consumer.deleteJob(job.getId());
                } catch(TemporaryJobException e) {
                    // erreur temporaire : release du job avec un délai de 1 minute
                    logger.error("Le job a rencontré une erreur temporaire : " + e.getMessage());
                    int delay = e.getDelay() > 0 ? e.getDelay() : 60;
                    if(e.getMaxTries() > 0 && Integer.parseInt(job.getStat("reserves")) > e.getMaxTries()) {
                        job.getReporter().fatal("Nombre maximal de tentatives dépassé.");
                        buryAndUnlock(job);
                    } else {
                        logger.info("Le job a été replacé dans la pile, nouvelle tentative dans " + delay + "s");
                        consumer.releaseJob(job.getId(), 1024, delay);
                    }
                } catch(JobException e) {
                    // toutes les autres exceptions sont fatales : bury du job éventuel +
                    logger.error("La tâche a rencontré une erreur fatale : " + e.getMessage(), e);
                    buryAndUnlock(job);
                } catch(Exception e) {
                    // toutes les autres exceptions sont fatales : bury du job éventuel +
                    logger.error("Le job a rencontré une erreur fatale : " + e.getMessage(), e);
                    job.getReporter().fatal(new ReportEntry(e.getMessage())/*.exception(e)*/);
                    buryAndUnlock(job);
                }
            } finally {
                consumer.close();
            }
        }
    }

    /**
     * Run a task in a thread, execute runnable each interval while tread in not complete
     */
    private <T> T threadExecute(Callable<T> task, Runnable runnable, long interval) throws Exception {
        try {
            Future<T> future = threadPoolExecutor.submit(task);
            while (!future.isDone()) {
                Thread.sleep(interval);
                runnable.run();
            }
            return future.get();
        } catch (ExecutionException e) {
            if(e.getCause() instanceof Exception) {
                throw (Exception) e.getCause();
            }
            throw e;
        }
    }

    /**
     * Bury and unlock job
     *
     * @param job
     */
    private void buryAndUnlock(Job job) {
        job.bury();
        if(job.isInitialized()) {
            try {
                job.callWebappApi("status/error", JobReportMessage.class);
            } catch (Exception e) {
                logger.error("Impossible d'envoyer le rapport d'erreurs à l'API", e);
            }
            try {
                job.unlock();
            } catch (JobException ignored) {}
        }
        logger.info("Le job a été annulé et placé en quarantaine");
    }

}


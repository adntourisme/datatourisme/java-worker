/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.worker.logger.reporter;

import com.google.gson.*;
import fr.datatourisme.worker.Job;
import fr.datatourisme.worker.logger.AbstractJobLogger;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Priority;

import java.io.IOException;

public class JobReporter extends AbstractJobLogger {

    public static JobReporter getReporter(Job job) {
        JobReporter reporter = new JobReporter();
        reporter.setFile(job.getStorage().dir("jobs").file(job.getUuid() + ".report"));
        return reporter;
    }

    /**
     * @param level
     * @param message
     */
    public void log(Priority level, Object message) {

        ReportEntry entry;
        if(message instanceof Exception) {
            entry = new ReportEntry(((Exception) message).getMessage());
            entry.exception((Exception) message);
        } else if(message instanceof ReportEntry) {
            entry = (ReportEntry) message;
        } else {
            entry = new ReportEntry(message.toString());
        }

        entry.setLevel(level);
        if(entry.stage == null) {
            entry.setStage(this.defaultStage);
        }

        JsonElement element = entry.toJsonTree();

        //logger.log(level, element.toString());

        if(file != null) {
            try {
                JsonObject object = getContent();
                object.add(entry.stage, element);
                setContent(object);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Get actual JsonObject content
     *
     * @return
     * @throws IOException
     */
    private JsonObject getContent() throws IOException {
        JsonObject object = new JsonObject();
        if(file.exists()) {
            String content = FileUtils.readFileToString(file);
            object = new JsonParser().parse(content).getAsJsonObject();
        }
        return object;
    }

    /**
     * Set JsonObject content
     * @param object
     * @throws IOException
     */
    private void setContent(JsonObject object) throws IOException {
        Gson gson = new GsonBuilder()
            .setPrettyPrinting()
            .create();
        FileUtils.writeStringToFile(file, gson.toJson(object));
    }
}

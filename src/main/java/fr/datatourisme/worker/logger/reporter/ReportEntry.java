/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.worker.logger.reporter;

import com.google.gson.*;
import fr.datatourisme.gson.serializer.CollectionSerializer;
import fr.datatourisme.gson.serializer.ExceptionSerializer;
import fr.datatourisme.gson.serializer.MapSerializer;
import fr.datatourisme.gson.serializer.PrioritySerializer;
import org.apache.log4j.Level;
import org.apache.log4j.Priority;

import java.lang.reflect.Type;
import java.util.*;

public class ReportEntry {
    String message;
    String stage;
    Priority level;
    Duration duration = new Duration();
    Exception exception;
    private Map<String, Object> data = new LinkedHashMap<String, Object>();
    private List<ReportEntry> reports = new ArrayList<>();

    public ReportEntry(String message) {
        this.message = message;
    }

    public ReportEntry() {
        this(null);
    }

    public ReportEntry setMessage(String message) {
        this.message = message;
        return this;
    }

    public ReportEntry setLevel(Priority level) {
        this.level = level;
        return this;
    }

    public Priority getLevel() {
        return level;
    }

    public ReportEntry setStage(String stage) {
        this.stage = stage;
        return this;
    }

    public Duration getDuration() {
        return duration;
    }

    public ReportEntry setDuration(Duration duration) {
        this.duration = duration;
        return this;
    }

    public ReportEntry data(Map<String, ? extends Object> map) {
        data.putAll(map);
        return this;
    }

    public ReportEntry data(String key, Object value) {
        data.put(key, value);
        return this;
    }

    public Object data(String key) {
        return data.get(key);
    }

    public List<ReportEntry> getReports() {
        return reports;
    }

    public ReportEntry addReport(ReportEntry report) {
        reports.add(report);
        return this;
    }

    public ReportEntry exception(Exception e) {
        exception = e;
        return this;
    }

    public JsonElement toJsonTree() {
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeHierarchyAdapter(Collection.class, new CollectionSerializer());
        gsonBuilder.registerTypeHierarchyAdapter(Map.class, new MapSerializer());
        gsonBuilder.registerTypeHierarchyAdapter(Level.class, new PrioritySerializer());
        gsonBuilder.registerTypeHierarchyAdapter(Exception.class, new ExceptionSerializer());
        gsonBuilder.registerTypeHierarchyAdapter(Duration.class, new DurationSerializer());
        return gsonBuilder.create().toJsonTree(this);
    }

    public String toJson(Boolean prettyPrinting) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        if(prettyPrinting) {
            gsonBuilder.setPrettyPrinting();
        }
        return gsonBuilder.create().toJson(toJsonTree());
    }

    public String toJson() {
        return toJson(false);
    }

    /**
     * Custom class to handle duration
     */
    public static class Duration {
        Long total;
        Long avg;
        Map<String, Duration> processes = new LinkedHashMap<>();
        transient long startTime;

        public Duration() {
            start();
        }
        public Duration addProcess(String key, Duration duration) {
            duration.end();
            processes.put(key, duration);
            return this;
        }

        public Duration startProcess(String key) {
            Duration duration = new Duration();
            addProcess(key, duration);
            return duration;
        }

        public Long getTotal() {
            return total;
        }

        public Long getAvg() {
            return avg;
        }

        public void setAvg(Long avg) {
            this.avg = avg;
        }

        public Map<String, Duration> getProcesses() {
            return processes;
        }

        public Duration start() {
            startTime = System.currentTimeMillis();
            return this;
        }

        public Duration end() {
            total = System.currentTimeMillis() - startTime;
            return this;
        }
    }

    /**
     * Custom serialize
     */
    public static class DurationSerializer implements JsonSerializer<Duration> {
        @Override
        public JsonElement serialize(Duration duration, Type typeOfSrc, JsonSerializationContext context) {
            if (duration == null) // exclusion is made here
                return null;
            if(duration.getProcesses().size() > 0 || duration.getAvg() != null) {
                JsonObject obj = new JsonObject();
                obj.addProperty("total", duration.getTotal());
                obj.addProperty("avg", duration.getAvg());
                if(duration.getProcesses().size() > 0) {
                    obj.add("processes", context.serialize(duration.getProcesses()));
                }
                return obj;
            } else {
                return context.serialize(duration.getTotal());
            }
        }
    }

}

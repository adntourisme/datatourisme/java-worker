/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.worker;

import com.dinstone.beanstalkc.BeanstalkClient;
import fr.datatourisme.task.AbstractTask;
import fr.datatourisme.webapp.api.WebappApiClient;
import fr.datatourisme.webapp.api.message.JobStatusMessage;
import fr.datatourisme.worker.exception.JobException;
import fr.datatourisme.worker.exception.TemporaryJobException;
import fr.datatourisme.worker.logger.JobLogger;
import fr.datatourisme.worker.logger.reporter.JobReporter;
import org.apache.log4j.Level;
import org.apache.log4j.Priority;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.net.SocketTimeoutException;
import java.util.Map;
import java.util.UUID;

public class Job {

    private Tube tube;
    private Storage storage;
    private BeanstalkClient beanstalkClient;

    com.dinstone.beanstalkc.Job bsJob;
    String uuid;
    Payload payload = new Payload();
    Map<String, String> stats;

    boolean initialized = false;
    boolean first = false;
    AbstractTask task;

    protected JobLogger logger = new JobLogger();
    private JobReporter reporter = new JobReporter();

    public Job(Tube tube, BeanstalkClient beanstalkClient) {
        this.tube = tube;
        this.beanstalkClient = beanstalkClient;
    }

    /**
     * Initialize the job from JobManager
     *
     * @throws JobException
     */
    void initializeFromBsJob(com.dinstone.beanstalkc.Job bsJob) throws JobException {
        this.bsJob = bsJob;
        Payload payload = Payload.fromJson(bsJob.getData());
        initialize(payload);
    }

    /**
     * set payload
     *
     * @param payload
     * @throws JobException
     */
    public void initialize(Payload payload) throws JobException {
        this.payload = payload;

        storage = tube.getStorage().dir(payload.getRequired("flux").toString());
        setUuid(payload.getString("uuid").orElse(UUID.randomUUID().toString()));
        payload.set("uuid", uuid);

        // create job logger
        logger = JobLogger.getLogger(this);

        // reporter
        reporter = JobReporter.getReporter(this);

        initialized = true;
    }

    public Payload getPayload() {
        return payload;
    }

    public long getId() {
        return bsJob.getId();
    }

    public String getUuid() {
        return uuid;
    }

    private void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public Storage getStorage() {
        return storage;
    }

    public Map<String, String> getStats() {
        if(stats == null && bsJob != null) {
            stats = beanstalkClient.statsJob(bsJob.getId());
        }
        return stats;
    }

    String getStat(String key) {
        return getStats().get(key);
    }

    public JobLogger getLogger() {
        return logger;
    }

    public JobReporter getReporter() {
        return reporter;
    }

    public boolean isFirst() {
        return first;
    }

    public boolean isInitialized() {
        return initialized;
    }

    private void log(Priority priority, Object message) {
        logger.log(priority, message, "Traitement");
    }

    Class<? extends AbstractTask> getNextTask() {
        return task.getNextTask();
    }

    /**
     * Return the lock file
     *
     * @return
     */
    private File getLockFile() throws JobException {
        return storage.file("job.lock");
    }

    /**
     * Try to lock the flux
     *
     * @return
     * @throws IOException
     */
    boolean lock() throws JobException {
        File lock = getLockFile();
        if(lock.exists()) {
            try {
                BufferedReader br = new BufferedReader(new FileReader(lock));
                long id = Long.parseLong(br.readLine());
                // ok if same job
                if(id == getId()) {
                    br.close();
                    return true;
                }
                // check if the job still alive
                Map<String, String> stats = beanstalkClient.statsJob(id);
                if(stats == null) {
                    // dead : lock with current job
                    first = true;
                    log(Level.INFO, "Démarrage du traitement");
                    lock(getId());
                    return true;
                }
                br.close();
                return false;
            } catch(NumberFormatException | IOException e) {
                throw new JobException("Impossible de lire le fichier de lock", e);
            }
        } else {
            first = true;
            log(Level.INFO, "Démarrage du traitement");
            lock(getId());
            return true;
        }
    }

    /**
     * @param newId
     * @throws IOException
     */
    void lock(long newId) throws JobException {
        File lock = getLockFile();
        BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter(lock));
            bw.write(newId+ "\n");
            bw.close();
        } catch (IOException e) {
            throw new JobException("Impossible d'écrire dans le fichier de lock", e);
        }
    }

    /**
     * @return
     */
    boolean unlock() throws JobException {
        log(Level.INFO, "Fin du traitement");
        return getLockFile().delete();
    }

    /**
     * @return
     */
    public void touch() {
        beanstalkClient.touchJob(getId());
    }

    /**
     * @return
     */
    public void bury() {
        beanstalkClient.buryJob(getId(), 0);
    }

    /**
     * Run the associate task
     * @return
     */
    AbstractTask createTask() throws JobException, IllegalAccessException, InstantiationException {
        String taskName = payload.getString("task").orElse(null);
        task = tube.getTask(taskName);
        payload.set("task", tube.getTaskName(task.getClass()));
        task.initFromJob(this);
        return task;
    }

    /**
     * @param route
     * @param msgClass
     */
    public void callWebappApi(String route, Class<? extends JobStatusMessage> msgClass) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException, JobException {
        WebappApiClient apiClient = tube.getApiClient();
        if(apiClient != null) {
            try {
                Constructor constructor = msgClass.getConstructor(Job.class);
                JobStatusMessage msg = (JobStatusMessage) constructor.newInstance(this);
                apiClient.post(route, msg);
            } catch (SocketTimeoutException e) {
                throw new TemporaryJobException("Impossible de contacter l'API", e);
            } catch (Exception e) {
                throw new JobException("Impossible de contacter l'API", e);
            }
        }
    }
}
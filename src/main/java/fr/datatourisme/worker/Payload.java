/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.worker;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import fr.datatourisme.gson.deserializer.FixedDoubleMapDeserializer;
import fr.datatourisme.worker.exception.InvalidPayloadException;
import fr.datatourisme.worker.exception.JobException;
import fr.datatourisme.worker.exception.MissingRequiredPayloadException;
import fr.datatourisme.worker.exception.TemporaryJobException;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.lang.reflect.Type;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class Payload {
    Map<String, Object> data = new HashMap<>();

    /**
     * @return
     */
    public Map<String, Object> getData() {
        return data;
    }

    /**
     * @param data
     */
    public void setData(Map<String, Object> data) {
        this.data = data;
    }

    /**
     * @param json
     * @return
     * @throws InvalidPayloadException
     */
    @JsonCreator
    public static Payload fromJson(String json) throws InvalidPayloadException {
        Payload payload = new Payload();

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeHierarchyAdapter(Map.class, new FixedDoubleMapDeserializer());

        try {
            Type stringObjectMap = new TypeToken<Map<String, Object>>(){}.getType();
            payload.setData(gsonBuilder.create().fromJson(json, stringObjectMap));
        } catch (Exception e) {
            throw new InvalidPayloadException("Impossible de décoder le payload", e);
        }
        return payload;
    }

    public static Payload fromJson(byte[] data) throws InvalidPayloadException {
        String json = new String(data, StandardCharsets.UTF_8);
        return Payload.fromJson(json);
    }

    /**
     * @return
     */
    public String toJson() {
        GsonBuilder gsonBuilder = new GsonBuilder();
//        gsonBuilder.registerTypeAdapter(Double.class, (JsonSerializer<Double>) (src, typeOfSrc, context) -> {
//            Integer value = (int)Math.round(src);
//            return new JsonPrimitive(value);
//        });
        return gsonBuilder.create().toJson(this.data);
    }

    /**
     * @param key
     * @return
     */
    public Optional<Object> get(String key) {
        return Optional.ofNullable(this.data.get(key));
    }

    /**
     * @param key
     * @return
     */
    public Object set(String key, Object value) {
        return this.data.put(key, value);
    }

    /**
     * @param key
     * @return
     */
    public Boolean has(String key) {
        return this.data.containsKey(key);
    }

    /**
     * @param key
     * @return
     */
    public Optional<String> getString(String key) {
        return get(key).flatMap(o -> Optional.ofNullable(o.toString()));
    }

    /**
     * @param key
     * @return
     * @throws MissingRequiredPayloadException
     */
    public Object getRequired(String key) throws MissingRequiredPayloadException {
        if(!this.data.containsKey(key)) {
            throw new MissingRequiredPayloadException(key);
        }
        return this.data.get(key);
    }

    /**
     * @param key
     * @return
     */
    public InputStream getStream(String key) throws JobException {
        String url = getString(key).orElse(null);
        if(url == null) {
            return null;
        }
        try {
            URL u = new URL(url);
            URLConnection con = u.openConnection();
            con.setConnectTimeout(2000);
            con.setReadTimeout(2000);
            return con.getInputStream();
        } catch (SocketTimeoutException e) {
            throw new TemporaryJobException("Impossible de contacter l'url " + url, e)
                    .setMaxTries(10);
        } catch (Exception e) {
            throw new JobException("Impossible de lire le contenu à partir de l'url " + url, e);
        }
    }

    /**
     * @param key
     * @return
     * @throws JobException
     */
    public Reader getJsonOrStream(String key) throws JobException {
        Object obj = get(key).orElse(null);
        if(obj == null) {
            return null;
        }
        if(obj instanceof Map) {
            GsonBuilder gsonBuilder = new GsonBuilder();
            String json = gsonBuilder.create().toJson(obj);
            return new StringReader(json);
        } else if(obj instanceof String) {
            InputStream in = getStream(key);
            return new InputStreamReader(in);
        } else {
            throw new JobException("Impossible de charger le stream JSON \"" + key + "\"");
        }
    }

    /**
     * @param key
     * @return
     * @throws JobException
     */
    public Reader getRequiredJsonOrStream(String key) throws JobException {
        if(!this.data.containsKey(key)) {
            throw new MissingRequiredPayloadException(key);
        }
        return getJsonOrStream(key);
    }
}

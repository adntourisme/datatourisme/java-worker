/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.worker;

import fr.datatourisme.task.AbstractTask;
import fr.datatourisme.webapp.api.WebappApiClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

/**
 * fr.datatourisme.worker.tube
 */
public class Tube {
    private Storage storage;
    private WebappApiClient apiClient;
    private Class<? extends AbstractTask> firstTaskClass;

    @Autowired
    private ApplicationContext context;

    public Tube(Storage storage, WebappApiClient webappApiClient, Class<? extends AbstractTask> firstTaskClass) {
        this.storage = storage;
        this.apiClient = webappApiClient;
        this.firstTaskClass = firstTaskClass;
    }

    public Storage getStorage() {
        return storage;
    }

    public WebappApiClient getApiClient() {
        return apiClient;
    }

    public Class<? extends AbstractTask> getFirstTaskClass() {
        return firstTaskClass;
    }

    /**
     * @param taskName
     * @return
     * @throws IllegalAccessException
     * @throws InstantiationException
     */
    public AbstractTask getTask(String taskName) throws IllegalAccessException, InstantiationException {
        if(taskName == null) {
            AbstractTask task = this.getFirstTaskClass().newInstance();
            context.getAutowireCapableBeanFactory().autowireBean(task);
            return task;
        } else {
            return context.getBean(taskName, AbstractTask.class);
        }
    }

    /**
     * @param taskClass
     * @return
     */
    public String getTaskName(Class<? extends AbstractTask> taskClass) {
        return context.getBeanNamesForType(taskClass)[0];
    }
}

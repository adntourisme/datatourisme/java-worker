/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.bundle.serializer;

import com.conjecto.graphstore.Triplet;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import fr.datatourisme.ontology.Ontology;
import fr.datatourisme.ontology.OntologyProperty;
import fr.datatourisme.vocabulary.Datatourisme;
import org.apache.jena.ext.com.google.common.cache.CacheBuilder;
import org.apache.jena.ext.com.google.common.cache.CacheLoader;
import org.apache.jena.ext.com.google.common.cache.LoadingCache;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntProperty;
import org.apache.jena.shared.PrefixMapping;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDF;
import org.semanticweb.yars.nx.Resource;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.function.Function;

public class Context {
    Ontology ontology;
    String defaultNamespace;
    PrefixMapping prefixMapping;
    List<Function<String, Boolean>> excludeCallbacks;
    LoadingCache<String, Boolean> hasLangStringRangeCache = CacheBuilder.newBuilder().build(CacheLoader.from(this::hasLangStringRangeSupplier));

    /**
     * Constructor
     *
     * @param ontology
     * @param defaultNamespace
     * @param excludeCallbacks
     */
    public Context(Ontology ontology, String defaultNamespace, PrefixMapping prefixMapping, List<Function<String, Boolean>> excludeCallbacks) {
        this.ontology = ontology;
        this.defaultNamespace = defaultNamespace;
        this.excludeCallbacks = excludeCallbacks;
        this.prefixMapping = prefixMapping;
    }

    /**
     * Return default namespace
     *
     * @return
     */
    public String getDefaultNamespace() {
        return defaultNamespace;
    }

    /**
     * Return default language
     *
     * @return
     */
    public String getDefaultLanguage() {
        return "fr";
    }

    /**
     * Return the prefix mapping
     *
     * @return
     */
    public PrefixMapping getPrefixMapping() {
        return prefixMapping;
    }

    /**
     * Return true if the IRI must be excluded from serialization
     *
     * @param uri
     * @return
     */
    public Boolean isExclude(String uri) {
        return excludeCallbacks.stream().anyMatch(f -> f.apply(uri));
    }

    /**
     * Return true if the triple must be excluded from serialization
     *
     * @param triplet
     * @return
     */
    public boolean isExclude(Triplet triplet) {
        return isExclude(triplet.getPredicate().getLabel())
            || (triplet.getObject() instanceof Resource && isExclude(triplet.getObject().getLabel()));
    }

    /**
     * Memoized supplier for isExcluded method
     *
     * @param uri
     * @return
     */
    private Boolean hasLangStringRangeSupplier(String uri) {
        OntProperty property = ontology.getOntModel().getOntProperty(uri);
        if (property != null) {
            return property.hasRange(RDF.langString);
        }
        return false;
    }

    /**
     * Return true if tjhe given property is functional
     *
     * @param uri
     * @return
     */
    public Boolean isFunctional(String uri) {
        OntologyProperty property = ontology.getProperty(uri);
        if (property != null) {
            return property.isFunctional();
        }
        return false;
    }

    /**
     * Return true if the triple must be excluded from serialization
     *
     * @return
     */
    public boolean hasLangStringRange(String uri) {
        try {
            return hasLangStringRangeCache.get(uri);
        } catch (ExecutionException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * @return
     */
    public String shortIRI(String uri) {
        if (uri.startsWith(defaultNamespace)) {
            return uri.substring(defaultNamespace.length());
        } else {
            return prefixMapping.shortForm(uri);
        }
    }

    /**
     * Return true if the object resource must be embedded (iteratively serialized) .
     * Otherwise, it will just be referenced by uri
     *
     * @param triplet
     * @return
     */
    public boolean isEmbeddable(Triplet triplet) {
        // if it's attached to the special keyword @type
        if (RDF.type.getURI().equals(triplet.getPredicate().getLabel())) {
            return false;
        }
        // if the subject is in the main ontology, don't expand it
        if (triplet.getObject().getLabel().startsWith(Datatourisme.NS)) {
            return false;
        }
        // owl:sameAs is a non embeddable reference
        if (OWL.sameAs.getURI().equals(triplet.getPredicate().getLabel())) {
            return false;
        }
        // otherwise, expand
        return true;
    }

    /**
     * Dump to JsonSLD
     *
     * @return
     */
    public String toJson() {
        JsonObject context = new JsonObject();
        OntModel model = ontology.getOntModel();

        // add @vocab
        //Ontology ont = model.listOntologies().next();
        if (defaultNamespace != null) {
            context.addProperty("@vocab", defaultNamespace);
        }

        // add prefixes
        model.getNsPrefixMap().forEach((prefix, uri) -> {
            if (!uri.equals(defaultNamespace)) {
                context.addProperty(prefix, uri);
            }
        });

        model.listAllOntProperties().forEachRemaining(property -> {
            JsonObject definition = new JsonObject();
            // @type
            org.apache.jena.rdf.model.Resource range = property.getRange();
            if ( range != null && !range.isAnon()) {
                definition.addProperty("@type", shortIRI(range.getURI()));
            }

            // language
            if (property.hasRange(RDF.langString)) {
                definition.addProperty("@container", "@language" );
            }

            if (definition.size() > 0) {
                context.add(shortIRI(property.getURI()), definition);
            }
        });

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.setPrettyPrinting();
        return gsonBuilder.create().toJson(context);
    }
}

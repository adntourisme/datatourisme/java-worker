/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.bundle.builder;

import fr.datatourisme.bundle.ResourceBundle;
import fr.datatourisme.bundle.ResourceStore;
import org.apache.commons.io.IOUtils;

import java.io.*;
import java.util.HashSet;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * ResourceBundleBuilder
 */
abstract public class ResourceBundleBuilder {

    ResourceStore resourceStore;
    File bundleFile;
    File indexFile;

    ZipOutputStream bundleOutputStream;
    OutputStream indexOutputStream;
    Set<String> resources = new HashSet<>();

    public ResourceBundleBuilder(ResourceStore resourceStore) throws IOException {
        this.resourceStore = resourceStore;
        this.bundleFile = File.createTempFile("resource-bundle", ".zip");
        this.indexFile = File.createTempFile("resource-bundle-index", ".tmp");

        // bundle output stream
        FileOutputStream fos = new FileOutputStream(this.bundleFile);
        bundleOutputStream = new ZipOutputStream(fos);

        // index output stream
        indexOutputStream = new FileOutputStream(this.indexFile);

        writeIndexHeader();
    }

    /**
     * @return
     */
    abstract public ResourceStore.Format getFormat();

    /**
     * @return
     */
    abstract protected void writeIndexHeader() throws IOException;

    /**
     * @return
     */
    abstract protected void writeIndexFooter() throws IOException;


    /**
     * Add a resource
     *
     * @param uri
     */
    public ResourceBundleBuilder add(String uri) throws IOException {
        File source = resourceStore.getResource(uri, this.getFormat());
        if (source.exists()) {

            // add zip entry
            String name = "objects/" + ResourceStore.getResourcePath(uri, this.getFormat());
            writeZipEntry(name, source);

            // add index entry
            source = resourceStore.getResourceIndexEntry(uri, this.getFormat());
            if (source.exists()) {
                try(FileInputStream fos = new FileInputStream(source)) {
                    writeIndexEntry(fos);
                }
                indexOutputStream.flush();
            }

            resources.add(uri);
        }
        return this;
    }

    /**
     * writeIndexEntry
     */
    protected void writeIndexEntry(FileInputStream input) throws IOException {
        indexOutputStream.write("\n".getBytes());
        IOUtils.copy(input, indexOutputStream);
    }

    /**
     * writeToOutputStream
     *
     * @param name
     * @param is
     * @throws IOException
     */
    protected void writeZipEntry(String name, InputStream is) throws IOException {
        ZipEntry ze = new ZipEntry(name);
        bundleOutputStream.putNextEntry(ze);
        byte[] buffer = new byte[1024];
        int length;
        while ((length = is.read(buffer)) > 0) {
            bundleOutputStream.write(buffer, 0, length);
        }
        bundleOutputStream.closeEntry();
    }

    /**
     * writeToOutputStream
     *
     * @param name
     * @param file
     * @throws IOException
     */
    protected void writeZipEntry(String name, File file) throws IOException {
        try(InputStream is = new FileInputStream(file)) {
            writeZipEntry(name, is);
        }
    }

    /**
     * @return
     */
    public ResourceBundle build() throws IOException {
        // finalize index file
        writeIndexFooter();
        indexOutputStream.close();

        // add index entry & close bundle
        writeZipEntry("index." + getFormat(), indexFile);
        bundleOutputStream.close();

        // remove index temp file
        indexFile.delete();

        // prepare & return bundle
        ResourceBundle bundle = new ResourceBundle();
        bundle.setFile(this.bundleFile);
        bundle.setResources(this.resources);
        return bundle;
    }
}

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.bundle;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Set;
import java.util.zip.GZIPOutputStream;

public class ResourceBundle {
    private File file;
    private Set<String> resources;

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public int getSize() {
        return resources.size();
    }

    public Set<String> getResources() {
        return resources;
    }

    public void setResources(Set<String> resources) {
        this.resources = resources;
    }

    public void delete() {
        file.delete();
    }

    /**
     * move
     */
    public void move(Path target) throws IOException {
        Files.move(file.toPath(), target, StandardCopyOption.REPLACE_EXISTING);
    }

    /**
     * move
     */
    public void moveToGzip(Path target) throws IOException {
        if (target.toFile().exists()) {
            target.toFile().delete();
        }
        FileInputStream fis = new FileInputStream(file);
        FileOutputStream fos = new FileOutputStream(target.toFile());
        GZIPOutputStream gzipOS = new GZIPOutputStream(fos);
        byte[] buffer = new byte[1024];
        int len;
        while((len=fis.read(buffer)) != -1){
            gzipOS.write(buffer, 0, len);
        }
        gzipOS.close();
        fos.close();
        file.delete();
    }
}

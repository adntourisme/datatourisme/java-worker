/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.ontology;

import fr.datatourisme.vocabulary.DatatourismeAlignment;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.jena.ext.com.google.common.base.Function;
import org.apache.jena.ext.com.google.common.base.Supplier;
import org.apache.jena.ext.com.google.common.base.Suppliers;
import org.apache.jena.ext.com.google.common.cache.CacheBuilder;
import org.apache.jena.ext.com.google.common.cache.CacheLoader;
import org.apache.jena.ontology.OntClass;
import org.apache.jena.ontology.OntProperty;
import org.apache.jena.ontology.OntResource;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.RDFList;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.shared.Lock;
import org.apache.jena.vocabulary.OWL;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class OntologyClass extends OntologyResource {
    private static final Log logger = LogFactory.getLog(OntologyClass.class);

    // cached suppliers/functions
    private Supplier<List<OntologyProperty>> listDirectProperties = Suppliers.memoize(this::getDirectProperties);
    private Supplier<List<OntologyProperty>> listCandidateProperties = Suppliers.memoize(this::getCandidateProperties);
    private Function<Boolean, List<OntologyClass>> listSuperClasses = CacheBuilder.newBuilder().build(CacheLoader.from(this::getSuperClasses));
    private Function<Boolean, List<OntologyClass>> listSubClasses = CacheBuilder.newBuilder().build(CacheLoader.from(this::getSubClasses));
    private Supplier<List<OntologyProperty>> listAlignableProperties = Suppliers.memoize(this::getAlignableProperties);

    /**
     * @param ontology
     * @param ontResource
     */
    public OntologyClass(Ontology ontology, OntClass ontResource) {
        super(ontology, ontResource);
    }

    /**
     * List super classes
     *
     * @return Stream
     */
    public Stream<OntologyClass> listSuperClasses(Boolean direct) {
        return listSuperClasses.apply(direct).stream();
    }

    /**
     * @return
     */
    private List<OntologyClass> getSuperClasses(Boolean direct) {
        Stream<OntologyClass> stream;
        if(direct) {
            ontResource.getOntModel().enterCriticalSection(Lock.READ);
            try {
                stream = ((OntClass)ontResource).listSuperClasses(true).toList().stream()
                    .filter(c -> !c.isAnon())
                    .map(c -> ontology.createClass(c));
            } finally {
                ontResource.getOntModel().leaveCriticalSection() ;
            }
        } else {
            stream = Stream.concat(
                listSuperClasses(true),
                listSuperClasses(true).flatMap(c -> c.listSuperClasses(false))
            );
        }
        return stream.collect(Collectors.toList());
    }

    /**
     * List direct super classes
     *
     * @return Stream
     */
    public Stream<OntologyClass> listSuperClasses() {
        return listSuperClasses(false);
    }

    /**
     * @param uri
     * @return Boolean
     */
    public Boolean hasSuperClass(String uri, Boolean direct) {
        return listSuperClasses(direct)
            .anyMatch(c -> c.hasURI(uri));
    }

    public Boolean hasSuperClass(Resource res, Boolean direct) {
        return hasSuperClass(res.getURI(), direct);
    }

    public Boolean hasSuperClass(OntologyResource res, Boolean direct) {
        return hasSuperClass(res.getURI(), direct);
    }

    public Boolean hasSuperClass(String uri) {
        return hasSuperClass(uri, false);
    }

    public Boolean hasSuperClass(Resource res) {
        return hasSuperClass(res, false);
    }

    public Boolean hasSuperClass(OntologyClass res) {
        return hasSuperClass(res, false);
    }

    /**
     * List sub classes
     *
     * @return Stream
     */
    public Stream<OntologyClass> listSubClasses(Boolean direct) {
        return listSubClasses.apply(direct).stream();
    }

    /**
     * @return
     */
    private List<OntologyClass> getSubClasses(Boolean direct) {
        Stream<OntologyClass> stream;
        if(direct) {
            ontResource.getOntModel().enterCriticalSection(Lock.READ);
            try {
                stream = ((OntClass)ontResource).listSubClasses(true).toList().stream()
                    .filter(c -> !c.isAnon())
                    .map(c -> ontology.createClass(c));
            } finally {
                ontResource.getOntModel().leaveCriticalSection() ;
            }
        } else {
            stream = Stream.concat(
                listSubClasses(true),
                listSubClasses(true).flatMap(c -> c.listSubClasses(false))
            );
        }
        return stream.collect(Collectors.toList());
    }

//    private List<OntologyClass> getSubClasses(Boolean direct) {
//        return ((OntClass)ontResource).listSubClasses(direct).toList().stream()
//                .filter(c -> !c.isAnon())
//                .map(c -> ontology.createClass(c))
//                .collect(Collectors.toList());
//    }

    /**
     * List direct sub classes
     *
     * @return Stream
     */
    public Stream<OntologyClass> listSubClasses() {
        return listSubClasses(false);
    }

    /**
     * @param uri
     * @return
     */
    public Boolean hasSubClass(String uri, Boolean direct) {
        return listSubClasses(direct)
            .anyMatch(c -> c.hasURI(uri));
    }

    public Boolean hasSubClass(Resource res, Boolean direct) {
        return hasSubClass(res.getURI(), direct);
    }

    public Boolean hasSubClass(OntologyResource res, Boolean direct) {
        return hasSubClass(res.getURI(), direct);
    }

    public Boolean hasSubClass(String uri) {
        return hasSubClass(uri, false);
    }

    public Boolean hasSubClass(Resource res) {
        return hasSubClass(res, false);
    }

    public Boolean hasSubClass(OntologyClass res) {
        return hasSubClass(res, false);
    }

    /**
     * List "direct" properties, aka ...
     *
     * @return Stream
     */
    public Stream<OntologyProperty> listDirectProperties() {
        return listDirectProperties.get().stream();
    }

    /**
     * @return
     */
    private List<OntologyProperty> getDirectProperties() {
        ontResource.getOntModel().enterCriticalSection(Lock.READ);
        try {
            return ontResource.getOntModel().listAllOntProperties().toList().stream()
                .filter(p -> {
                    return p.listDomain().toList().stream()
                        //.filter(OntResource::isClass) // remove protection to detect anomalies
                        .map(OntResource::asClass)
                        .flatMap(c -> {
                            // if the domain is an union class, flat map operands
                            if(c.isUnionClass()) {
                                return c.asUnionClass().listOperands().toList().stream();
                            }
                            return Stream.of(c);
                        })
                        .filter(c -> !c.isAnon())
                        .anyMatch(c -> {
                            return c.equals(ontResource)
                                    || c.hasEquivalentClass(ontResource)
                                    //|| ontClass.hasEquivalentClass(c)
                                    || c.equals(OWL.Thing);
                        });
                })
                .map(p -> ontology.createProperty(p))
                .collect(Collectors.toList());
        } finally {
            ontResource.getOntModel().leaveCriticalSection() ;
        }
    }

    /**
     * List "candidate" properties, aka ...
     *
     * @return Stream
     */
    public Stream<OntologyProperty> listCandidateProperties() {
        return listCandidateProperties.get().stream();
    }

    /**
     * @return
     */
    private List<OntologyProperty> getCandidateProperties() {
        return Stream.concat(
            listDirectProperties(),
            Stream.concat(
                listSuperClasses(false).flatMap(OntologyClass::listDirectProperties),
                listSubClasses(false).flatMap(_c -> Stream.concat(
                    _c.listDirectProperties(),
                    _c.listSuperClasses(false).flatMap(OntologyClass::listDirectProperties)
                ))
            )).distinct()
            .collect(Collectors.toList());
    }

    /**
     * List alignable properties, aka ...
     *
     * @return Stream
     */
    public Stream<OntologyProperty> listAlignableProperties() {
        return listAlignableProperties.get().stream();
    }

    /**
     * @return
     */
    private List<OntologyProperty> getAlignableProperties() {
        ontResource.getOntModel().enterCriticalSection(Lock.READ);
        try {
            RDFNode alignablePropertiesNode = ontResource.getPropertyValue(DatatourismeAlignment.hasAlignableProperties);
            if(alignablePropertiesNode != null && alignablePropertiesNode.canAs(RDFList.class)) {
                return alignablePropertiesNode.as(RDFList.class).asJavaList().stream()
                    .map(n -> n.as(Property.class))
                    .map(p -> ontology.createProperty(p.as(OntProperty.class)))
                    .collect(Collectors.toList());
            }
            return new ArrayList<>();
        } finally {
            ontResource.getOntModel().leaveCriticalSection();
        }
    }
}
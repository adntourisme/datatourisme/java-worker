/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.ontology;

import org.apache.jena.ontology.OntResource;
import org.apache.jena.shared.Lock;

public class OntologyResource {
    protected Ontology ontology;
    protected OntResource ontResource;

    public OntologyResource(Ontology ontology, OntResource ontResource) {
        this.ontology = ontology;
        this.ontResource = ontResource;
    }

    public Ontology getOntology() {
        return ontology;
    }

    public OntResource getOntResource() {
        return ontResource;
    }

    public String getURI() {
        return ontResource.getURI();
    }

    public String getShortURI() {
        return ontology.shortForm(ontResource.getURI());
    }

    public Boolean hasURI(String uri) {
        return ontResource.hasURI(uri);
    }

    public String getLabel(String lang) {
        ontResource.getOntModel().enterCriticalSection(Lock.READ);
        try {
            String label = ontResource.getLabel(lang);
            if(label == null) {
                label = ontResource.getLabel(Ontology.defaultLanguage);
            }
            return label;
        } finally {
            ontResource.getOntModel().leaveCriticalSection();
        }
    }

    public String getLabel() {
        return getLabel(Ontology.defaultLanguage);
    }

    public String getComment(String lang) {
        ontResource.getOntModel().enterCriticalSection(Lock.READ);
        try {
            String comment = ontResource.getComment(lang);
            if(comment == null) {
                comment = ontResource.getComment(Ontology.defaultLanguage);
            }
            return comment;
        } finally {
            ontResource.getOntModel().leaveCriticalSection();
        }
    }

    public String getComment() {
        return getComment(Ontology.defaultLanguage);
    }

    @Override
    public String toString() {
        return this.getURI();
    }

    @Override public boolean equals(Object other) {
        boolean result = false;
        if (other instanceof OntologyResource) {
            OntologyResource that = (OntologyResource) other;
            result = (this.hasURI(that.getURI()));
        }
        return result;
    }

    @Override public int hashCode() {
        return (41 * /*(41 + getX())*/ + getURI().hashCode());
    }
}

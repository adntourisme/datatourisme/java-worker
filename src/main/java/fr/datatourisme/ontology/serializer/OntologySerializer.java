/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.ontology.serializer;

import com.google.gson.*;
import fr.datatourisme.gson.serializer.PrefixMappingSerializer;
import fr.datatourisme.ontology.*;
import fr.datatourisme.vocabulary.Datatourisme;
import fr.datatourisme.vocabulary.DatatourismeAlignment;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.shared.PrefixMapping;
import org.apache.jena.vocabulary.OWL2;

import java.lang.reflect.Type;

public class OntologySerializer implements JsonSerializer<Ontology> {
    private static final Log logger = LogFactory.getLog(OntologySerializer.class);

    private String language = Ontology.defaultLanguage;

    public OntologySerializer() { }

    public OntologySerializer(String language) {
        this.language = language;
    }

    /**
     * @param ontology
     * @param prettyPrinting
     * @return
     */
    public static String serialize(Ontology ontology, String language, Boolean prettyPrinting) {
        GsonBuilder gsonBuilder = new GsonBuilder()
            .registerTypeHierarchyAdapter(PrefixMapping.class, new PrefixMappingSerializer())
            .registerTypeAdapter(Ontology.class, new OntologySerializer(language));
        if(prettyPrinting) {
            gsonBuilder.setPrettyPrinting();
        }
        return gsonBuilder.create().toJson(ontology, ontology.getClass());
    }

    /**
     * @param ontology
     * @return
     */
    public static String serialize(Ontology ontology, Boolean prettyPrinting) {
        return OntologySerializer.serialize(ontology, Ontology.defaultLanguage, prettyPrinting);
    }

    /**
     * @param ontology
     * @return
     */
    public static String serialize(Ontology ontology, String language) {
        return OntologySerializer.serialize(ontology, language, false);
    }

    /**
     * @param ontology
     * @return
     */
    public static String serialize(Ontology ontology) {
        return OntologySerializer.serialize(ontology, Ontology.defaultLanguage, false);
    }

    @Override
    public JsonElement serialize(Ontology ontology, Type typeOfSrc, JsonSerializationContext context) {
        if(ontology == null) {
            return null;
        }

        // context
        JsonObject obj = new JsonObject();
        obj.add("@context", context.serialize(ontology.getPrefixMapping()));

        // classes
        JsonObject classes = new JsonObject();
        ontology.listClasses().forEach(c -> {
            JsonObject jsonObj = serializeClass(c);
            classes.add(c.getShortURI(), jsonObj);
        });
        obj.add("classes", classes);

        // properties
        JsonObject properties = new JsonObject();
        ontology.listProperties().forEach(p -> {
            JsonObject jsonObj = serializeProperty(p);
            properties.add(p.getShortURI(), jsonObj);
        });
        obj.add("properties", properties);

        // individuals
        logger.debug("Serialize individuals");
        JsonObject individuals = new JsonObject();
        ontology.listIndividuals().forEach(i -> {
            JsonObject jsonObj = serializeIndividual(i);
            individuals.add(i.getShortURI(), jsonObj);
        });
        obj.add("individuals", individuals);

        return obj;
    }

    private JsonObject serializeClass(OntologyClass ontologyClass) {
        JsonObject jsonObj = serializeResource(ontologyClass);

        // superClasses
        logger.debug(" -> Serialize superClasses");
        JsonArray superClasses = new JsonArray();
        ontologyClass.listSuperClasses(true).forEach(c -> superClasses.add(c.getShortURI()));
        if(superClasses.size() > 0) {
            jsonObj.add("superClasses", superClasses);
        }

        // subClasses
        logger.debug(" -> Serialize subClasses");
        JsonArray subClasses = new JsonArray();
        ontologyClass.listSubClasses(true).forEach(c -> subClasses.add(c.getShortURI()));
        if(subClasses.size() > 0) {
            jsonObj.add("subClasses", subClasses);
        }

        // directProperties
        logger.debug(" -> Serialize directProperties");
        JsonArray directProperties = new JsonArray();
        ontologyClass.listDirectProperties().forEach(p -> directProperties.add(p.getShortURI()));
        if(directProperties.size() > 0) {
            jsonObj.add("directProperties", directProperties);
        }

        // candidateProperties
        logger.debug(" -> Serialize candidateProperties");
        JsonArray candidateProperties = new JsonArray();
        ontologyClass.listCandidateProperties().forEach(p -> candidateProperties.add(p.getShortURI()));
        if(candidateProperties.size() > 0) {
            jsonObj.add("transversalProperties", candidateProperties);
        }

        // thesaurus
        if(ontologyClass.hasSuperClass(OWL2.NamedIndividual)) {
            jsonObj.add("thesaurus", new JsonPrimitive(true));
        }

        return jsonObj;
    }

    /**
     * @param ontologyProperty
     * @return
     */
    private JsonObject serializeProperty(OntologyProperty ontologyProperty) {
        JsonObject jsonObj = serializeResource(ontologyProperty);

        // type
        jsonObj.addProperty("type", ontologyProperty.isDatatype() ? "data" : "object");

        // functional
        if(ontologyProperty.isFunctional()) {
            jsonObj.addProperty("functional", true);
        }

        // priority
        if(ontologyProperty.getOntResource().hasProperty(Datatourisme.hasPriority)) {
            float priority = ontologyProperty.getOntResource().getPropertyValue(Datatourisme.hasPriority).asLiteral().getFloat();
            jsonObj.addProperty("priority", priority);
        }

        // alignable
        if(ontologyProperty.getOntResource().hasProperty(DatatourismeAlignment.isAlignable)) {
            boolean alignable = ontologyProperty.getOntResource().getPropertyValue(DatatourismeAlignment.isAlignable).asLiteral().getBoolean();
            jsonObj.addProperty("alignable", alignable);
        }

        // ranges
        JsonArray ranges = new JsonArray();
        ontologyProperty.listRanges().forEach(c -> ranges.add(c.getShortURI()));
        if(ranges.size() > 0) {
            jsonObj.add("ranges", ranges);
        }

        // directDomains
        JsonArray directDomains = new JsonArray();
        ontologyProperty.getOntology().listClasses().filter(c -> {
            return c.listDirectProperties().anyMatch(p -> p.hasURI(ontologyProperty.getURI()));
        }).forEach(c -> directDomains.add(c.getShortURI()));
        if(directDomains.size() > 0) {
            jsonObj.add("directDomains", directDomains);
        }

        return jsonObj;
    }

    /**
     * @param ontologyIndividual
     * @return
     */
    private JsonObject serializeIndividual(OntologyIndividual ontologyIndividual) {
        JsonObject jsonObj = serializeResource(ontologyIndividual);

        // properties
        JsonObject properties = new JsonObject();
        ontologyIndividual.listPropertiesValues().forEach(es -> {
            JsonArray values = new JsonArray();
            es.getValue().forEach(o -> {
                if(o instanceof Resource) {
                    values.add(ontologyIndividual.getOntology().shortForm(((Resource) o).getURI()));
                } else {
                    values.add(o.toString());
                }
            });
            properties.add(es.getKey().getShortURI(), values);
        });
        if(properties.size() > 0) {
            jsonObj.add("properties", properties);
        }

        // types
        JsonArray types = new JsonArray();
        ontologyIndividual.listTypes(true).forEach(c -> types.add(c.getShortURI()));
        if(types.size() > 0) {
            jsonObj.add("types", types);
        }

        return jsonObj;
    }

    /**
     * @param ontologyResource
     * @return
     */
    private JsonObject serializeResource(OntologyResource ontologyResource) {
        JsonObject jsonObj = new JsonObject();
        jsonObj.addProperty("uri", ontologyResource.getShortURI());

        String label = ontologyResource.getLabel(language);
        if(label != null) {
            jsonObj.addProperty("label", label);
        }

        String comment = ontologyResource.getComment(language);
        if(comment != null) {
            jsonObj.addProperty("comment", comment);
        }

        return jsonObj;
    }
}

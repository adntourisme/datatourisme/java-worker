/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.messaging.listener;

import fr.datatourisme.dataset.DatatourismeRepository;
import org.apache.jena.ext.com.google.common.collect.ImmutableSet;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.log4j.Logger;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.StringWriter;
import java.util.List;

/**
 * This message listener is used to populate a empty message on the pipeline source
 */
public class PipelineSourceMessageListener implements MessageListener {
    private static final Logger logger = Logger.getLogger(PipelineSourceMessageListener.class);

    @Autowired
    private DatatourismeRepository datatourismeRepository;

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    Exchange pipelineSourceExchange;

    @Override
    public void onMessage(Message message) {
        // ignore populated message
        if (message.getBody().length > 0) {
            return;
        }

        // search for a URI in the headers
        String uri = message.getMessageProperties().getHeader("uri");
        if (uri != null) {
            Resource res = datatourismeRepository.findPOI(uri);
            if (res != null) {
                Model model = datatourismeRepository.constructPOIHierarchy(ImmutableSet.of(res));

                // send model back to the pipeline
                logger.info(String.format("Send model to the pipeline : %s statements", model.size()));

                StringWriter writer = new StringWriter();
                RDFDataMgr.write(writer, model, Lang.NTRIPLES) ;
                MessageProperties messageProperties = new MessageProperties();
                Message message2 = new Message(writer.toString().getBytes(), messageProperties);
                rabbitTemplate.send(pipelineSourceExchange.getName(), "", message2);

            } else {
                logger.warn(String.format("POI not found : %s", uri));
            }
        }
    }

    @Override
    public void containerAckMode(AcknowledgeMode mode) {
        // todo ?
    }

    @Override
    public void onMessageBatch(List<Message> messages) {
        messages.forEach(this::onMessage);
    }
}

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.utils;

import com.ximpleware.*;
import org.jdom2.JDOMException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

public class VTDUtils {

    /**
     * Extract namespaces from VTDNav
     *
     * @param vn
     * @return
     * @throws NavException
     */
    public static Map<String, String> extractNamespaces(VTDNav vn) throws NavException {
        int tokenCount = vn.getTokenCount();
        String token;
        String nsPrefix;
        String nsUri;
        Map<String, String> namespaces = new LinkedHashMap<>();
        for ( int i = 0; i < tokenCount; i++ ) {
            if ( vn.startsWith( i, "xmlns:" ) ) {
                token = vn.toNormalizedString( i );
                nsPrefix = token.substring( token.indexOf( ":" ) + 1 );
                nsUri = vn.toNormalizedString( i + 1 );
                if(namespaces.containsKey(nsPrefix) && !namespaces.get(nsPrefix).equals(nsUri)) {
                    throw new NavException("Multiple namespaces for the same prefix is not supported");
                }
                namespaces.put(nsPrefix, nsUri);
            }// if
        }
        return namespaces;
    }

    /**
     * Return a Stream of elements content
     *
     * @param file
     * @param xpathExpr
     * @return
     * @throws NavException
     * @throws XPathParseException
     * @throws XPathEvalException
     * @throws JDOMException
     * @throws IOException
     */
    public static Stream<byte[]> streamElements(File file, String xpathExpr) throws NavException, XPathParseException, XPathEvalException, JDOMException, IOException, ParseException {

//        VTDGen vg = new VTDGen();
//        vg.parseFile(file.getAbsolutePath(), true);

        // read the file
        FileInputStream fis = new FileInputStream(file);
        byte[] b = new byte[(int) file.length()];
        fis.read(b);
        // create vtd gen
        VTDGen vg = new VTDGen();
        vg.setDoc(b);
        vg.parse(true); // set namespace awareness to true

        VTDNav vn = vg.getNav();
        AutoPilot ap = new AutoPilot(vn);

        Map<String, String> namespaces = VTDUtils.extractNamespaces(vn);
        namespaces.forEach(ap::declareXPathNameSpace);
        ap.selectXPath(xpathExpr);

        // prepare namespaces addons
        List<String> namespacesAddons = new ArrayList<>();
        namespaces.forEach((prefix, uri) -> {
            namespacesAddons.add(" xmlns:"+prefix+"=\""+uri+"\"");
        });

        Stream.Builder<byte[]> streamB = Stream.builder();
        while (ap.evalXPath() != -1) {

            // ---
            // extract content
            // ---
            Fragment elementFragment = new Fragment(vn.getElementFragment());
            Fragment contentFragment = new Fragment(vn.getContentFragment());

            // store bytes
            //byte[] bytes = vn.getXML().getBytes((int)elementFragment[0], (int)elementFragment[1]);

            // prepare outputStream && get head size
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );
            int headSize;
            if(contentFragment.offset > -1) {
                headSize = contentFragment.offset - elementFragment.offset - 1;
            } else {
                // empty element
                headSize = elementFragment.length - 2;
            }

            // remove local namespaces from head
            String head = new String(vn.getXML().getBytes(elementFragment.offset, headSize));
            head = head.replaceAll("\\s+xmlns(:[^=]+)?=\"[^\"]*\"", "");

            // add global namespaces to root element
            outputStream.write( head.getBytes() );
            for(String addon: namespacesAddons) {
                outputStream.write( addon.getBytes() );
            }

            outputStream.write(vn.getXML().getBytes(elementFragment.offset + headSize, elementFragment.length - headSize));
            byte[] content = outputStream.toByteArray( );

            // if not UTF8
            if(vn.getEncoding() != 2) {
                content = new String(content, VTDUtils.getCharset(vn.getEncoding())).getBytes(Charset.forName("UTF-8"));
            }

            // add to stream
            streamB.accept(content);
        }
        return streamB.build();
    }


    /**
     * @param encoding
     *
     * @return Charset
     */
    private static Charset getCharset(int encoding) {
        switch(encoding) {
            case VTDNav.FORMAT_UTF8:
                return Charset.forName("UTF-8");
            case VTDNav.FORMAT_ASCII:
                return Charset.forName("ASCII");
            case VTDNav.FORMAT_ISO_8859_1:
                return Charset.forName("ISO-8859-1");
            case VTDNav.FORMAT_ISO_8859_2:
                return Charset.forName("ISO-8859-2");
            case VTDNav.FORMAT_ISO_8859_3:
                return Charset.forName("ISO-8859-3");
            case VTDNav.FORMAT_ISO_8859_4:
                return Charset.forName("ISO-8859-4");
            case VTDNav.FORMAT_ISO_8859_5:
                return Charset.forName("ISO-8859-5");
            case VTDNav.FORMAT_ISO_8859_6:
                return Charset.forName("ISO-8859-6");
            case VTDNav.FORMAT_ISO_8859_7:
                return Charset.forName("ISO-8859-7");
            case VTDNav.FORMAT_ISO_8859_8:
                return Charset.forName("ISO-8859-8");
            case VTDNav.FORMAT_ISO_8859_9:
                return Charset.forName("ISO-8859-9");
            case VTDNav.FORMAT_ISO_8859_11:
                return Charset.forName("ISO-8859-11");
            case VTDNav.FORMAT_ISO_8859_13:
                return Charset.forName("ISO-8859-13");
            case VTDNav.FORMAT_ISO_8859_15:
                return Charset.forName("ISO-8859-15");
            case VTDNav.FORMAT_WIN_1250:
                return Charset.forName("WINDOWS-1250");
            case VTDNav.FORMAT_WIN_1251:
                return Charset.forName("WINDOWS-1251");
            case VTDNav.FORMAT_WIN_1252:
                return Charset.forName("WINDOWS-1252");
            case VTDNav.FORMAT_WIN_1253:
                return Charset.forName("WINDOWS-1253");
            case VTDNav.FORMAT_WIN_1254:
                return Charset.forName("WINDOWS-1254");
            case VTDNav.FORMAT_WIN_1255:
                return Charset.forName("WINDOWS-1255");
            case VTDNav.FORMAT_WIN_1256:
                return Charset.forName("WINDOWS-1256");
            case VTDNav.FORMAT_WIN_1257:
                return Charset.forName("WINDOWS-1257");
            case VTDNav.FORMAT_WIN_1258:
                return Charset.forName("WINDOWS-1258");
            case VTDNav.FORMAT_UTF_16LE:
                return Charset.forName("UTF-16LE");
            case VTDNav.FORMAT_UTF_16BE:
                return Charset.forName("UTF-16BE");
            default:
                throw new RuntimeException("Unsupported encoding (" + encoding + ")");
        }
    }

    /**
     * VTD fragment
     */
    private static class Fragment  {
        public int offset;
        public int length;
        public Fragment(long fragment) {
            this.offset = (int)fragment;
            this.length = (int)(fragment>>32);
        }
    }
}

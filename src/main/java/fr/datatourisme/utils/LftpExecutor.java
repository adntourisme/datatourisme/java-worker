/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.utils;

import org.apache.commons.exec.CommandLine;
import org.apache.commons.exec.DefaultExecutor;
import org.apache.commons.exec.ExecuteException;
import org.apache.commons.exec.PumpStreamHandler;
import org.apache.http.client.HttpResponseException;
import org.apache.http.conn.ConnectTimeoutException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LftpExecutor {

    CommandLine cmdLine = new CommandLine("lftp");
    DefaultExecutor executor = new DefaultExecutor();

    CollectingLogOutputStream outputStream = new CollectingLogOutputStream();
    ByteArrayOutputStream errorStream = new ByteArrayOutputStream();

    private static final Pattern httpCodePattern = Pattern.compile("\\b4\\d\\d\\b");

    public LftpExecutor() {
        PumpStreamHandler psh = new PumpStreamHandler(outputStream, errorStream);
        executor.setStreamHandler(psh);
    }

    public CollectingLogOutputStream getOutputStream() {
        return outputStream;
    }

    public ByteArrayOutputStream getErrorStream() {
        return errorStream;
    }

    public LftpExecutor addArgument(String argument) {
        cmdLine.addArgument(argument);
        return this;
    }

    public LftpExecutor addArgument(String argument, boolean handleQuoting) {
        cmdLine.addArgument(argument, handleQuoting);
        return this;
    }

    public LftpExecutor setWorkingDirectory(File dir) {
        executor.setWorkingDirectory(dir);
        return this;
    }

    public static LftpExecutor timeoutCommand(String command, Integer timeout, Integer maxRetries) {
        return new LftpExecutor()
            .addArgument("-c")
            .addArgument("set net:timeout " + timeout + "; set net:max-retries " + (maxRetries+1) + "; " + command, false);
    }

    public static LftpExecutor timeoutCommand(String command) {
        return timeoutCommand(command, 30, 2);
    }

    public int execute() throws IOException {
        try {
            return executor.execute(cmdLine);
        } catch(ExecuteException e) {
            String errorMsg = errorStream.toString();
            Matcher m = httpCodePattern.matcher(errorMsg);
            if(m.find()) {
                throw new HttpResponseException(Integer.valueOf(m.group(0)), errorMsg);
            }
            if(errorMsg.contains("max-retries")) {
                // timeout
                throw new ConnectTimeoutException();
            }
            throw new IOException(errorMsg);
        }
    }

}

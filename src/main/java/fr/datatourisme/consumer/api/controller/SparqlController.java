/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.consumer.api.controller;

import fr.datatourisme.api.controller.AbstractController;
import fr.datatourisme.dataset.BlazegraphRestClient;
import org.apache.commons.io.IOUtils;
import org.apache.jena.query.Query;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.QueryParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.InputStream;

/**
 * fr.datatourisme.consumer.api.controller
 */

@RestController
@RequestMapping("/consumer/sparql")
public class SparqlController extends AbstractController {

    @Autowired
    BlazegraphRestClient datasetRestClient;

    @PostMapping("/preview")
    @ResponseBody
    public ResponseEntity<?> post(@RequestBody String queryStr) {
        try {
            // parse de la requête
            Query query = QueryFactory.create(queryStr);
            query.setLimit(10);
            InputStream inputStream = datasetRestClient.query(query, "application/json", 2*60*1000); //2 min timeout
            return responseJson(IOUtils.toString(inputStream));

        } catch (QueryParseException e) {
            return error(e, HttpStatus.BAD_REQUEST);

        } catch (Exception e) {
            return error(e);

        }
    }
}
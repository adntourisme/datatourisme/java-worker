/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.consumer.processor.graph;

import com.conjecto.graphstore.GraphStore;
import com.conjecto.graphstore.Triplet;
import org.semanticweb.yars.nx.Node;
import org.semanticweb.yars.nx.Resource;

import java.util.*;

/**
 * fr.datatourisme.consumer
 */
public class GraphStoreHierarchyCompleter {
    private GraphStore store;
    private GraphStore sourceStore;

    private Queue<Node> queue;
    private Set<Node> visited;

    private Set<Triplet> pool;
    int poolSize = 1000;

    /**
     * @param store
     * @param sourceStore
     */
    public GraphStoreHierarchyCompleter(GraphStore store, GraphStore sourceStore) {
        this.store = store;
        this.sourceStore = sourceStore;
    }

    /**
     * @param iter
     */
    public void process(Iterator<Triplet> iter) {
        queue = new LinkedList<>();
        visited = new HashSet<>();
        pool = new HashSet<>();

        while(iter.hasNext()) {
            queue.add(iter.next().getSubject());
        }

        while (!queue.isEmpty()) {
            Node node = queue.remove();
            if(!visited.contains(node)) {
                processNode(node);
                visited.add(node);
            }
            if(pool.size() >= poolSize) {
                store.add(pool);
                pool.clear();
            }
        }

        store.add(pool);
    }

    /**
     * @param node
     */
    private void processNode(Node node) {
        Iterator<Triplet> iter = sourceStore.querySPO(node.toString());
        while(iter.hasNext()) {
            Triplet triplet = iter.next();
            pool.add(triplet);
            if(triplet.getObject() instanceof Resource) {
                queue.add(triplet.getObject());
            }
        }
    }
}

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.consumer.processor.tuple;

import org.openrdf.model.Literal;
import org.openrdf.model.Value;
import org.openrdf.query.Binding;
import org.openrdf.query.BindingSet;
import org.openrdf.query.QueryResultHandlerException;
import org.openrdf.query.TupleQueryResultHandlerException;
import org.openrdf.query.resultio.QueryResultFormat;
import org.openrdf.query.resultio.TupleQueryResultFormat;
import org.openrdf.query.resultio.TupleQueryResultWriter;
import org.openrdf.rio.RioSetting;
import org.openrdf.rio.WriterConfig;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Surround TupleQueryResult instance to perform custom process for each solution. As language filter.
 */
public class TupleResultWriterWrapper implements TupleQueryResultWriter {
    TupleQueryResultWriter writer;
    Set<String> languages;

    public TupleResultWriterWrapper(TupleQueryResultWriter writer) {
        this.writer = writer;
    }

    public void setLanguages(Set<String> languages) {
        this.languages = languages;
    }

    @Override
    public TupleQueryResultFormat getTupleQueryResultFormat() {
        return writer.getTupleQueryResultFormat();
    }

    @Override
    public QueryResultFormat getQueryResultFormat() {
        return writer.getQueryResultFormat();
    }

    @Override
    public void handleNamespace(String s, String s1) throws QueryResultHandlerException {
        writer.handleNamespace(s, s1);
    }

    @Override
    public void startDocument() throws QueryResultHandlerException {
        writer.startDocument();
    }

    @Override
    public void handleStylesheet(String s) throws QueryResultHandlerException {
        writer.handleStylesheet(s);
    }

    @Override
    public void startHeader() throws QueryResultHandlerException {
        writer.startHeader();
    }

    @Override
    public void endHeader() throws QueryResultHandlerException {
        writer.endHeader();
    }

    @Override
    public void setWriterConfig(WriterConfig writerConfig) {
        writer.setWriterConfig(writerConfig);
    }

    @Override
    public WriterConfig getWriterConfig() {
        return writer.getWriterConfig();
    }

    @Override
    public Collection<RioSetting<?>> getSupportedSettings() {
        return writer.getSupportedSettings();
    }

    @Override
    public void handleBoolean(boolean value) throws QueryResultHandlerException {
        writer.handleBoolean(value);
    }

    @Override
    public void handleLinks(List<String> linkUrls) throws QueryResultHandlerException {
        writer.handleLinks(linkUrls);
    }

    @Override
    public void startQueryResult(List<String> bindingNames) throws TupleQueryResultHandlerException {
        writer.startQueryResult(bindingNames);
    }

    @Override
    public void endQueryResult() throws TupleQueryResultHandlerException {
        writer.endQueryResult();
    }

    @Override
    public void handleSolution(BindingSet bindingSet) throws TupleQueryResultHandlerException {
        if(languages != null && languages.size() > 0) {
            Iterator<Binding> iter = bindingSet.iterator();
            while(iter.hasNext()) {
                Value value = iter.next().getValue();
                if(value instanceof Literal && ((Literal) value).getLanguage() != null && !languages.contains(((Literal) value).getLanguage())) {
                    return;
                }
            }
        }
        writer.handleSolution(bindingSet);
    }
}

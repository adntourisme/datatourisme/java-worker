/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.command;

import fr.datatourisme.producer.xml.xpath.XPathMapper;
import org.nanocom.console.input.InputArgument;
import org.nanocom.console.input.InputParameterInterface;
import org.springframework.context.annotation.Import;

import java.io.File;
import java.nio.file.Files;
import java.util.List;

@Import(fr.datatourisme.Configuration.class)
public class XpathSchema extends AbstractCommand {

    @Override
    protected void configure() {
        setName("xpath-schema");
        setDescription("Process XML file(s) to generate a Xpath JSON representation");
        setDefinition(new InputParameterInterface[] {
            // arguments
            new InputArgument("files", InputArgument.REQUIRED | InputArgument.IS_ARRAY, "Ontology TTL file to process")
        });
    }

    @Override
    public int run() {
        List<String> files = (List<String>) input.getArgument("files");
        XPathMapper mapper = new XPathMapper();
        files.forEach(file -> {
            try {
                mapper.parse(Files.readAllBytes(new File(file).toPath()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });
        System.out.println(mapper.toJson());
        return 0;
    }

}

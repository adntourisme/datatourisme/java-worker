/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.command;

import fr.datatourisme.assembler.Assembler;
import fr.datatourisme.ontology.Ontology;
import fr.datatourisme.ontology.serializer.OntologySerializer;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.nanocom.console.input.InputArgument;
import org.nanocom.console.input.InputParameterInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;

import java.util.List;

@Import(fr.datatourisme.Configuration.class)
public class SerializeOntology extends AbstractCommand {

    @Autowired
    private Assembler assembler;

    @Override
    protected void configure() {
        setName("serialize-ontology");
        setDescription("Process OWL file(s) to generate a JSON representation");
        setDefinition(new InputParameterInterface[] {
            // arguments
            new InputArgument("files", InputArgument.REQUIRED | InputArgument.IS_ARRAY, "Ontology TTL file to process")
        });
    }

    @Override
    public int run() {
        List<String> files = (List<String>) input.getArgument("files");

        Model model = assembler.getModel();
        Resource resource = model.createResource( model.expandPrefix( ":ontologyModelSpec" ) );
        OntModelSpec spec = (OntModelSpec) org.apache.jena.assembler.Assembler.general.open(resource);
        OntModel ontModel = ModelFactory.createOntologyModel( spec );
        files.forEach(ontModel::read);

        Ontology ontology = new Ontology(ontModel);
        String json = OntologySerializer.serialize(ontology, true);

        System.out.println(json);
        return 0;
    }

}

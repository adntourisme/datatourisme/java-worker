/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.command;

import com.google.gson.GsonBuilder;
import fr.datatourisme.gson.serializer.CollectionSerializer;
import fr.datatourisme.producer.processor.alignment.Alignment;
import fr.datatourisme.producer.processor.alignment.AlignmentFactory;
import fr.datatourisme.producer.processor.thesaurus.ThesaurusAlignment;
import fr.datatourisme.producer.processor.thesaurus.ThesaurusAlignmentFactory;
import fr.datatourisme.producer.task.ProduceRDFTask;
import fr.datatourisme.worker.Storage;
import fr.datatourisme.worker.logger.reporter.ReportEntry;
import org.apache.commons.codec.digest.DigestUtils;
import org.nanocom.console.input.InputArgument;
import org.nanocom.console.input.InputOption;
import org.nanocom.console.input.InputParameterInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Import;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;
import java.util.Collection;

@Import(fr.datatourisme.Configuration.class)
public class BenchProcess extends AbstractCommand {

    @Autowired
    ApplicationContext context;

    @Autowired
    ThesaurusAlignmentFactory thesaurusAlignmentFactory;

    @Autowired
    AlignmentFactory alignmentFactory;

    @Override
    protected void configure() {
        setName("bench-process");
        setDescription("Bench a complete process");
        setDefinition(new InputParameterInterface[] {
            // arguments
            new InputArgument("datasource", InputArgument.REQUIRED, "Datasource to use"),
            new InputArgument("alignment", InputArgument.REQUIRED, "Alignment to use"),
            new InputArgument("thesaurus", InputArgument.REQUIRED, "Thesaurus to use"),
            new InputOption("dir", "d", InputOption.VALUE_OPTIONAL, "Storage dir to use"),
        });
    }

    @Override
    public int run() {
        try {
            File dir;
            if(input.getOption("dir") != null) {
                dir = new File(input.getOption("dir").toString());
            } else {
                dir = Files.createTempDirectory("datatourisme_bench").toFile();
                dir.deleteOnExit();
            }

            Storage storage = new Storage(dir);
            ReportEntry reportEntry;
            ReportEntry.Duration duration = new ReportEntry.Duration();

            ThesaurusAlignment thesaurusAlignment = thesaurusAlignmentFactory.createFromJson(new File(input.getArgument("thesaurus").toString()));
            Alignment alignment = alignmentFactory.createFromJson(thesaurusAlignment, new FileInputStream(input.getArgument("alignment").toString()));
            File db = storage.file("poi.db");
//
//            // DOWNLOAD
//            DownloadTask downloadTask = context.getAutowireCapableBeanFactory().createBean(DownloadTask.class);
//            downloadTask.setStorage(storage);
//            downloadTask.setUrl(new File(input.getArgument("datasource").toString()).toURI().toString());
//            reportEntry = downloadTask.run();
//            duration.addProcess(downloadTask.getMachineName(), reportEntry.getDuration());
//
//            // PREPROCESS
//            PreprocessXmlTask preprocessXmlTask = context.getAutowireCapableBeanFactory().createBean(PreprocessXmlTask.class);
//            preprocessXmlTask.setStorage(storage);
//            reportEntry = preprocessXmlTask.run();
//            duration.addProcess(preprocessXmlTask.getMachineName(), reportEntry.getDuration());
//
//            // PROCESS
//            ProcessXmlTask processXmlTask = context.getAutowireCapableBeanFactory().createBean(ProcessXmlTask.class);
//            processXmlTask.setStorage(storage);
//            processXmlTask.setDb(db);
//            processXmlTask.setReset(true);
//            processXmlTask.setAlignment(alignment);
//            processXmlTask.setThesaurusAlignment(thesaurusAlignment);
//            reportEntry = processXmlTask.run();
//            duration.addProcess(processXmlTask.getMachineName(), reportEntry.getDuration());

            // PRODUCE
            ProduceRDFTask produceRDFTask = context.getAutowireCapableBeanFactory().createBean(ProduceRDFTask.class);
            produceRDFTask.setStorage(storage);
            produceRDFTask.setDb(db);
            produceRDFTask.setAlignment(alignment);
            produceRDFTask.setThesaurusAlignment(thesaurusAlignment);
            produceRDFTask.setChecksum(DigestUtils.md5Hex(String.valueOf(Math.random())));
            produceRDFTask.setFluxIdentifier(1);
            produceRDFTask.setOrganizationIdentifier(1);
            reportEntry = produceRDFTask.run();
            duration.addProcess(produceRDFTask.getMachineName(), reportEntry.getDuration());

//            // PERSIST
//            PersistRDFTask persistRDFTask = context.getAutowireCapableBeanFactory().createBean(PersistRDFTask.class);
//            persistRDFTask.setStorage(storage);
//            persistRDFTask.setFluxIdentifier(1);
//            persistRDFTask.setOrganizationIdentifier(1);
//            persistRDFTask.setDb(db);
//            reportEntry = persistRDFTask.run();
//            duration.addProcess(persistRDFTask.getMachineName(), reportEntry.getDuration());

            // FINAL REPORT
            duration.end();
            GsonBuilder gsonBuilder = new GsonBuilder().setPrettyPrinting();
            gsonBuilder.registerTypeHierarchyAdapter(Collection.class, new CollectionSerializer());
            gsonBuilder.registerTypeHierarchyAdapter(ReportEntry.Duration.class, new ReportEntry.DurationSerializer());
            System.out.println(gsonBuilder.create().toJson(duration));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

}

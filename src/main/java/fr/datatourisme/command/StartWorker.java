/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.command;

import fr.datatourisme.worker.Tube;
import fr.datatourisme.worker.Worker;
import org.apache.log4j.PropertyConfigurator;
import org.nanocom.console.input.InputArgument;
import org.nanocom.console.input.InputParameterInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Import;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

@Import(fr.datatourisme.Configuration.class)
public class StartWorker extends AbstractCommand {

    @Autowired
    ApplicationContext context;

    @Autowired
    Worker worker;

    @Override
    public String getProfile() {
        return "worker";
    }

    @Override
    protected void configure() {
        setName("start-worker");
        setDescription("Start a beanstalkd worker");
        setDefinition(new InputParameterInterface[] {
            // arguments
            new InputArgument("tube", InputArgument.OPTIONAL | InputArgument.IS_ARRAY, "List of tubes")
        });
    }

    @Override
    public int run() {
        updateLog4jConfiguration();
        List<String> tubesArg = (List<String>) input.getArgument("tube");

        // retrieve tube list
        String[] tubes;
        if(tubesArg.size() > 0) {
            tubes = tubesArg.toArray(new String[tubesArg.size()]);
        } else {
            tubes = context.getBeanNamesForType(Tube.class);
        }

        // run worker
        return worker.run(tubes);
    }

    /**
     * Update log4j configuration to force Job logging (important !)
     */
    private void updateLog4jConfiguration() {
        Properties props = new Properties();
        try {
            InputStream configStream = getClass().getResourceAsStream( "/log4j.properties");
            props.load(configStream);
            configStream.close();
        } catch (IOException e) {
            System.out.println("Error: Cannot laod configuration file ");
        }
        props.setProperty("log4j.logger.fr.datatourisme.worker.JobManager.Job", "INFO");
        PropertyConfigurator.configure(props);
    }
}

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.webapp.api;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Request;
import org.apache.http.entity.ContentType;
import org.apache.log4j.Logger;

import java.io.IOException;

public class WebappApiClient {
    Logger logger = Logger.getLogger(WebappApiClient.class);

    String baseUrl;

    Gson gson;

    public WebappApiClient(String baseUrl) {
        this.baseUrl = baseUrl;
        GsonBuilder gsonBuilder = new GsonBuilder();
        this.gson = gsonBuilder.create();
    }

    public Content post(String path, Object body) throws IOException {
        String encoded = gson.toJson(body);
        logger.info("POST to " + baseUrl + "/" + path + " : " + encoded);
        return Request.Post(baseUrl + "/" + path)
            .connectTimeout(1000)
            .socketTimeout(60000 * 60 * 2)
            .bodyString(encoded, ContentType.APPLICATION_JSON)
            .execute().returnContent();
    }
}

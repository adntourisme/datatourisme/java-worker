/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.dataset;

import fr.datatourisme.dataset.query.SparqlQueryExecutionService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
public class DatasetConfiguration {

    @Value("${blazegraph.host}")
    private String blazegraphHost;

    @Value("${blazegraph.port}")
    private int blazegraphPort;

    @Value("${blazegraph.query.timeout}")
    private int blazegraphQueryTimeout;

    @Value("${blazegraph.namespace.default}")
    private String blazegraphDefaultNamespace;

    @Value("${blazegraph.namespace.archive}")
    private String blazegraphArchiveNamespace;

    @Value("${blazegraph.namespace.extra}")
    private String blazegraphExtraNamespace;

    @Value("${datatourisme.graphstore.path}")
    private String graphStorePath;

    @Bean @Lazy
    public SparqlQueryExecutionService queryExecutionService() {
        return new SparqlQueryExecutionService(getBlazegraphEndpointUrl() + "/namespace/"+ blazegraphDefaultNamespace +"/sparql");
    }

    @Bean @Lazy
    public SparqlQueryExecutionService extraQueryExecutionService() {
        return new SparqlQueryExecutionService(getBlazegraphEndpointUrl() + "/namespace/"+ blazegraphExtraNamespace +"/sparql");
    }

    @Bean @Lazy
    public DatatourismeRepository datatourismeRepository(SparqlQueryExecutionService queryExecutionService) {
        return new DatatourismeRepository(queryExecutionService);
    }

    @Bean(name="datasetRestClient") @Lazy
    public BlazegraphRestClient getDatasetRestClient() {
        BlazegraphRestClient blazegraphRestClient = new BlazegraphRestClient(getBlazegraphEndpointUrl() + "/namespace/"+ blazegraphDefaultNamespace +"/sparql");
        blazegraphRestClient.setQueryTimeout(blazegraphQueryTimeout);
        return blazegraphRestClient;
    }

    @Bean(name="archiveDatasetRestClient") @Lazy
    public BlazegraphRestClient getArchiveDatasetRestClient() {
        return new BlazegraphRestClient(getBlazegraphEndpointUrl() + "/namespace/"+ blazegraphArchiveNamespace +"/sparql");
    }

    @Bean(name="extraDatasetRestClient") @Lazy
    public BlazegraphRestClient getExtraDatasetRestClient() {
        return new BlazegraphRestClient(getBlazegraphEndpointUrl() + "/namespace/"+ blazegraphExtraNamespace +"/sparql");
    }


    @Bean @Lazy
    public GraphStoreManager getGraphStoreManager() {
        return new GraphStoreManager(graphStorePath);
    }

    /**
     * @return
     */
    private String getBlazegraphEndpointUrl() {
        return "http://" + blazegraphHost + ":" + blazegraphPort + "/blazegraph";
    }
}

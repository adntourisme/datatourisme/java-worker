/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.dataset;

import com.conjecto.graphstore.GraphStore;
import com.conjecto.graphstore.GraphStoreOptions;
import com.conjecto.graphstore.exception.GraphStoreException;
import com.conjecto.graphstore.exception.GraphStoreLockException;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;

/**
 * fr.datatourisme.dataset
 */
public class GraphStoreManager {

    /**
     * File
     */
    File storeFile;

    /**
     * @param path
     */
    public GraphStoreManager(String path) {
        this.storeFile = new File(path);
    }

    /**
     * Try to open the GraphStore with a timeout to acquire the lock
     *
     * @param options
     * @param timeout
     *
     * @return
     * @throws GraphStoreException
     */
    public GraphStore open(GraphStoreOptions options, boolean readOnly, long timeout) throws GraphStoreException {
        long start = System.currentTimeMillis();
        while(true) {
            try {
                if(readOnly) {
                    return GraphStore.openReadOnly(storeFile.getAbsolutePath(), options);
                } else {
                    return GraphStore.open(storeFile.getAbsolutePath(), options);
                }
            } catch (GraphStoreLockException e) {
                if(System.currentTimeMillis() - start > timeout) {
                    throw e;
                }
                try {
                    Thread.sleep(100);
                } catch (InterruptedException ex) {
                    Thread.currentThread().interrupt();
                    throw new RuntimeException(ex);
                }
            }
        }
    }

    public GraphStore open(GraphStoreOptions options, long timeout) throws GraphStoreException {
        return open(options, false, timeout);
    }

    public GraphStore open(long timeout) throws GraphStoreException {
        GraphStoreOptions options = new GraphStoreOptions();
        return open(options, timeout);
    }

    public GraphStore openReadOnly(GraphStoreOptions options, long timeout) throws GraphStoreException {
        return open(options, true, timeout);
    }

    public GraphStore openReadOnly(long timeout) throws GraphStoreException {
        GraphStoreOptions options = new GraphStoreOptions();
        return openReadOnly(options, timeout);
    }

    public static GraphStore open(String path, GraphStoreOptions options, long timeout) throws GraphStoreException {
        GraphStoreManager mgr = new GraphStoreManager(path);
        return mgr.open(options, false, timeout);
    }

    public static GraphStore open(String path, long timeout) throws GraphStoreException {
        GraphStoreManager mgr = new GraphStoreManager(path);
        return mgr.open(timeout);
    }

    /**
     * @throws IOException
     */
    public void delete() throws IOException {
        FileUtils.deleteDirectory(storeFile);
    }

    /**
     * @return
     * @throws IOException
     */
    public boolean exists() throws IOException {
        return storeFile.exists();
    }
}

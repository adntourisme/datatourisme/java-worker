/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.dataset.query;

import org.apache.jena.query.Query;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.update.UpdateExecutionFactory;
import org.apache.jena.update.UpdateProcessor;
import org.apache.jena.update.UpdateRequest;

/**
 * fr.datatourisme.dataset.query
 */
public class SparqlQueryExecutionService extends AbstractQueryExecutionService {
    private String service;

    public SparqlQueryExecutionService(String service) {
        this.service = service;
    }

    @Override
    public QueryExecution queryExecution(Query query) {
        return QueryExecutionFactory.sparqlService(service, query);
    }

    @Override
    public UpdateProcessor updateExecution(UpdateRequest request) {
        return UpdateExecutionFactory.createRemote(request, service);
    }
}

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.gson.serializer;

import com.google.gson.JsonElement;
import com.google.gson.JsonPrimitive;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import org.apache.log4j.Priority;

import java.lang.reflect.Type;

public class PrioritySerializer implements JsonSerializer<Priority> {
    @Override
    public JsonElement serialize(Priority level, Type typeOfSrc, JsonSerializationContext context) {
        if (level == null) // exclusion is made here
            return null;
        return new JsonPrimitive(level.toString());
    }
}

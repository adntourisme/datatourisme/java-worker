/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.gson.serializer;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import org.apache.commons.lang3.exception.ExceptionUtils;

import java.lang.reflect.Type;

public class ExceptionSerializer implements JsonSerializer<Exception> {
    @Override
    public JsonElement serialize(Exception exception, Type typeOfSrc, JsonSerializationContext context) {
        if (exception == null) // exclusion is made here
            return null;
        JsonObject obj = new JsonObject();
        obj.addProperty("message", exception.getMessage());
        obj.addProperty("class", exception.getClass().getSimpleName());
        //obj.add("stacktrace", context.serialize(exception.getStackTrace()));
        obj.add("stacktrace", context.serialize(ExceptionUtils.getStackTrace(exception)));
        return obj;
    }
}

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.assembler.builtin;

import org.apache.jena.datatypes.BaseDatatype;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.reasoner.rulesys.RuleContext;
import org.apache.jena.reasoner.rulesys.builtins.BaseBuiltin;

public class GeoLatLon extends BaseBuiltin {
    @Override
    public String getName() {
        return "geolatlon";
    }

    @Override
    public int getArgLength() {
        return 0;
    }

    @Override
    public boolean bodyCall(org.apache.jena.graph.Node[] args, int length, RuleContext context) {
        Node lat = getArg(0, args, context);
        Node lon = getArg(1, args, context);
        Node time = null;
        if(length > 3) {
            time = getArg(2, args, context);
        }
        String concat = lat.getLiteralLexicalForm() + "#" + lon.getLiteralLexicalForm();
        String dt = "http://www.bigdata.com/rdf/geospatial/literals/v1#lat-lon";
        if(time != null) {
            concat += "#" + time.getLiteralLexicalForm();
            dt = "http://www.bigdata.com/rdf/geospatial/literals/v1#lat-lon-time";
        }
        Node result = NodeFactory.createLiteral(concat, new BaseDatatype(dt));
        return context.getEnv().bind(args[length-1], result);
    }
}

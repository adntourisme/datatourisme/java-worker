/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.assembler.builtin;

import org.apache.jena.graph.Node;
import org.apache.jena.graph.NodeFactory;
import org.apache.jena.graph.Triple;
import org.apache.jena.reasoner.rulesys.RuleContext;
import org.apache.jena.reasoner.rulesys.builtins.BaseBuiltin;
import org.apache.jena.vocabulary.RDF;


/**
 * Create a blank node list with given arguments
 */
public class MakeList extends BaseBuiltin {

    /**
     * Return a name for this builtin, normally this will be the name of the
     * functor that will be used to invoke it.
     */
    @Override
    public String getName() {
        return "makeList";
    }

    /**
     * This method is invoked when the builtin is called in a rule body.
     * @param args the array of argument values for the builtin, this is an array
     * of Nodes, some of which may be Node_RuleVariables.
     * @param length the length of the argument list, may be less than the length of the args array
     * for some rule engines
     * @param context an execution context giving access to other relevant data
     * @return return true if the buildin predicate is deemed to have succeeded in
     * the current environment
     */
    @Override
    public boolean bodyCall(Node[] args, int length, RuleContext context) {
        Node list = NodeFactory.createBlankNode();
        Node first = NodeFactory.createURI(RDF.first.getURI());
        Node rest = NodeFactory.createURI(RDF.rest.getURI());

        Node curList = list;
        for (int i = 1; i < length; i++) {
            Node value = getArg(i, args, context);
            context.add( new Triple( curList, first, value ) );
            if(i < length - 1) {
                Node nextList = NodeFactory.createBlankNode();
                context.add( new Triple( curList, rest, nextList ) );
                curList = nextList;
            } else {
                context.add( new Triple( curList, rest, RDF.nil.asNode() ) );
            }
        }

        return context.getEnv().bind(args[0], list);
    }
}

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.assembler;

import fr.datatourisme.assembler.builtin.*;
import fr.datatourisme.ontology.Ontology;
import fr.datatourisme.vocabulary.*;
import org.apache.jena.ontology.OntModel;
import org.apache.jena.ontology.OntModelSpec;
import org.apache.jena.ontology.impl.OntModelImpl;
import org.apache.jena.rdf.model.InfModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.reasoner.Reasoner;
import org.apache.jena.reasoner.ReasonerFactory;
import org.apache.jena.reasoner.rulesys.BuiltinRegistry;
import org.apache.jena.riot.system.stream.JenaIOEnvironment;
import org.apache.jena.riot.system.stream.LocationMapper;
import org.apache.jena.util.FileManager;

import java.io.File;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Assembler {

    Model model;
    Map<String, Object> cache = new ConcurrentHashMap<>();

    public Assembler(Map<String, String> locationMap) {
        // load model
        model = FileManager.get().loadModel( "assembler/assembler.ttl" );

        // add resources to location mapper
        LocationMapper mapper = JenaIOEnvironment.getLocationMapper();
        mapper.addAltPrefix("file://resources/", "");

        // if location map provided, add the prefixes
        if(locationMap != null) {
            locationMap.forEach((key, path) -> {
                mapper.addAltPrefix("file://"+key+"/", new File(path).toURI().toString());
            });
        }

        setEnvOptions();
    }

    public Model getModel() {
        return model;
    }

    /**
     * Set some global options
     */
    private void setEnvOptions() {

        Ontology ontology = createOntology(":ontology");

        // register rules bultins
        BuiltinRegistry.theRegistry.register(new RegexNeg());
        BuiltinRegistry.theRegistry.register(new GeoLatLon());
        BuiltinRegistry.theRegistry.register(new MakeDataResource());
        BuiltinRegistry.theRegistry.register(new MakeList());
        BuiltinRegistry.theRegistry.register(new IndividualLabel(ontology));
        BuiltinRegistry.theRegistry.register(new IndividualProp(ontology));
        BuiltinRegistry.theRegistry.register(new Substr());
        BuiltinRegistry.theRegistry.register(new ListDrop());
        BuiltinRegistry.theRegistry.register(new NotInseeZip());

        // config Jena
        org.apache.jena.shared.impl.JenaParameters.enableEagerLiteralValidation = true;
//        org.apache.jena.shared.impl.JenaParameters.enableSilentAcceptanceOfUnknownDatatypes = false;
    }

    /**
     * @param uri
     * @return Mode
     */
    public Model openModel(String uri) {
        return (Model) cache.computeIfAbsent(uri, _uri -> {
            Resource resource = model.createResource( model.expandPrefix( uri ) );
            Model model = org.apache.jena.assembler.Assembler.general.openModel(resource);
            // normalize prefixes
            model.setNsPrefix(Datatourisme.PREFIX, Datatourisme.getURI());
            model.setNsPrefix(DatatourismeData.PREFIX, DatatourismeData.getURI());
            model.setNsPrefix(DatatourismeAlignment.PREFIX, DatatourismeAlignment.getURI());
            model.setNsPrefix(DatatourismeThesaurus.PREFIX, DatatourismeThesaurus.getURI());
            model.setNsPrefix(DatatourismeMetadata.PREFIX, DatatourismeMetadata.getURI());
            return model;
        });
    }

    /**
     * @param uri
     * @return OntModel
     */
    public OntModel openOntModel(String uri) {
        // create a non inf model to prevent concurrency exception during operations
        OntModel ontModel = (OntModel) openModel(uri);
        Model model = ModelFactory.createDefaultModel().add(ontModel);
        ontModel = new OntModelImpl(OntModelSpec.OWL_MEM, model);
        //ontModel.setStrictMode(false); // NO WAY !!!
        return ontModel;
    }

    /**
     * Make a copy of inferred model to avoid concurency error during alignment
     *
     * @param uri
     * @return
     */
    public Ontology createOntology(String uri) {
        return new Ontology(openOntModel(uri));
    }

    /**
     * @param uri
     * @return Mode
     */
    public Reasoner openReasoner(String uri) {
        return (Reasoner) cache.computeIfAbsent(uri, _uri -> {
            Resource resource = model.createResource( model.expandPrefix( uri ) );
            ReasonerFactory factory = (ReasonerFactory) org.apache.jena.assembler.Assembler.general.open(resource);
            Reasoner reasoner = factory.create(null);
            return reasoner;
        });
    }

    /**
     * @param uri
     * @return
     */
    public InfModel createReasonerInfModel(String uri, Model data) {
        Reasoner reasoner = openReasoner( uri );
        return ModelFactory.createInfModel(reasoner, data);
    }

    /**
     * @param uri
     * @return
     */
    public InfModel createReasonerInfModel(String uri) {
        return createReasonerInfModel(uri, ModelFactory.createDefaultModel());
    }
}

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.task;

import fr.datatourisme.worker.Job;
import fr.datatourisme.worker.Payload;
import fr.datatourisme.worker.Storage;
import fr.datatourisme.worker.exception.JobException;
import fr.datatourisme.worker.exception.TemporaryJobException;
import fr.datatourisme.worker.logger.JobLogger;
import fr.datatourisme.worker.logger.reporter.JobReporter;
import fr.datatourisme.worker.logger.reporter.ReportEntry;

abstract public class AbstractTask {

    protected Job job;
    protected Storage storage;

    protected JobLogger logger = (JobLogger) new JobLogger().setDefaultStage(this.getLocalizedName());
    protected JobReporter reporter = (JobReporter) new JobReporter().setDefaultStage(this.getMachineName());

    public static int defaultPriority = 512;

    public abstract String getMachineName();

    protected abstract String getLocalizedName();

    protected abstract ReportEntry execute() throws Exception;

    public Class<? extends AbstractTask> getNextTask() {
        return null;
    }

    abstract public void initFromPayload(Payload payload) throws JobException;

    public void initFromJob(Job job) throws JobException {
        this.job = job;
        this.storage = job.getStorage();

        // add log appender
        logger = (JobLogger) job.getLogger().setDefaultStage(this.getLocalizedName());
        reporter = (JobReporter) job.getReporter().setDefaultStage(this.getMachineName());

        // init from payload
        initFromPayload(job.getPayload());
    }

    public void setStorage(Storage storage) {
        this.storage = storage;
    }

    /**
     * @throws JobException
     */
    public ReportEntry run() throws JobException {
        try {
            ReportEntry reportEntry = execute();
            if(reportEntry != null) {
                reportEntry.getDuration().end();
                reporter.info(reportEntry);
            }
            return reportEntry;
        } catch(TemporaryJobException e) {
            reporter.warn(new ReportEntry(e.getMessage()).exception(e));
            logger.warn(e);
            throw e;
        } catch (Exception e) {
            reporter.fatal(new ReportEntry(e.getMessage()).exception(e));
            logger.fatal(e);
            throw new JobException(e.getMessage(), e);
        }
    }
}

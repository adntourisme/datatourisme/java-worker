/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.api.controller;

import fr.datatourisme.worker.Storage;
import fr.datatourisme.worker.Tube;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

@RestController
@RequestMapping("/{tubeName}/{fluxId}/job")
public class JobController extends AbstractController {

    @Autowired
    ApplicationContext context;

    @ModelAttribute("storage")
    protected Storage getStorage(@PathVariable String tubeName) {
        return context.getBean(tubeName, Tube.class).getStorage();
    }

    @GetMapping("/{uuid}/log")
    @ResponseBody
    public ResponseEntity<?> getLog(@ModelAttribute("storage") Storage storage, @PathVariable Integer fluxId, @PathVariable String uuid) throws IOException {
        File logFile = storage.dir(fluxId).dir("jobs").file(uuid + ".log");
        if(!logFile.exists()) {
            return error("Impossible de trouver le log du job " + uuid, HttpStatus.NOT_FOUND);
        }
        List<String> lines = Files.readAllLines(Paths.get(logFile.getAbsolutePath()));
        return response(lines);
    }

    @GetMapping("/{uuid}/report")
    @ResponseBody
    public ResponseEntity<?> getReport(@ModelAttribute("storage") Storage storage, @PathVariable Integer fluxId, @PathVariable String uuid) throws IOException {
        File reportFile = storage.dir(fluxId).dir("jobs").file(uuid + ".report");
        if(!reportFile.exists()) {
            return error("Impossible de trouver le report du job " + uuid, HttpStatus.NOT_FOUND);
        }

        List<String> lines = Files.readAllLines(Paths.get(reportFile.getAbsolutePath()));
        String[] linesArr = new String[lines.size()];
        lines.toArray(linesArr);

        return responseJson("["+ String.join(",", linesArr) +"]");
    }

    @GetMapping("/{uuid}/status")
    @ResponseBody
    public ResponseEntity<?> getStatus(@ModelAttribute("storage") Storage storage, @PathVariable Integer fluxId, @PathVariable String uuid) throws IOException {
        return response("todo");
    }

    @PostMapping
    @ResponseBody
    public ResponseEntity<?> post(@ModelAttribute("storage") Storage storage, @PathVariable Integer fluxId) throws IOException {
        return response("todo");
    }

}
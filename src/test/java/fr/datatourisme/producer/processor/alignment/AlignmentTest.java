/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.alignment;

import fr.datatourisme.ontology.Ontology;
import fr.datatourisme.ontology.OntologyProperty;
import fr.datatourisme.producer.AbstractProducerTest;
import fr.datatourisme.producer.processor.alignment.rule.AbstractRule;
import fr.datatourisme.producer.processor.xml.XmlContext;
import fr.datatourisme.vocabulary.Datatourisme;
import org.apache.commons.io.IOUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.jdom2.Element;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.FileInputStream;
import java.util.Set;

import static org.junit.Assert.assertEquals;

public class AlignmentTest extends AbstractProducerTest {
    @Autowired
    Ontology ontologyAlignment;

    @Test
    public void testFromJson() {
        Alignment alignment = getAlignement();
        Set<? extends AbstractRule> rules = alignment.getRules();
        Assert.assertEquals(11, rules.size());
    }

    @Test
    public void bugMultipleFunctionProperty() throws Exception {
        setFixturesDir("lot");
        Alignment alignment = getAlignement();
        XmlContext ctx = XmlContext.create(IOUtils.toByteArray(new FileInputStream(getXmlSource())));

        Element element= (Element) ctx.evaluate("//entry").findFirst().orElse(null);
        XmlContext entryCtx = ctx.create(element);

        Model model = ModelFactory.createDefaultModel();
        Resource res = model.createResource();
        OntologyProperty property = ontologyAlignment.getProperty(Datatourisme.property("offers"));
        alignment.getRule(property).process(entryCtx, res);

        Property hasEligiblePolicy = Datatourisme.property("hasEligiblePolicy");
        model.listResourcesWithProperty(hasEligiblePolicy).forEachRemaining(resource -> {
            assertEquals(1, resource.listProperties(hasEligiblePolicy).toList().size());
        });
    }
}

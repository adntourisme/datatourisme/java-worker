/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.alignment.transformer.literal;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * fr.datatourisme.producer.processor.alignment.transformer.literal
 */
public class PhoneNumberTransformerTest {
    @Test
    public void transform() throws Exception {
        PhoneNumberTransformer transformer = new PhoneNumberTransformer();
        assertEquals("+33 2 99 11 44 55", transformer.transform("0299114455"));
        assertEquals("+33 2 99 11 44 55", transformer.transform("02 99 11 44 55"));
        assertEquals("+33 2 99 11 44 55", transformer.transform("02.99.11.44.55"));
        assertEquals("+33 2 99 11 44 55", transformer.transform("2 99 11 44 55"));
        assertEquals("+44 20 7946 0018", transformer.transform("+44 2079460018"));
    }

}
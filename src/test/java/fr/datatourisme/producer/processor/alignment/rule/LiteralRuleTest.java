/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.alignment.rule;

import fr.datatourisme.ontology.Ontology;
import fr.datatourisme.producer.AbstractProducerTest;
import fr.datatourisme.producer.processor.xml.XmlContext;
import org.apache.commons.io.IOUtils;
import org.apache.jena.vocabulary.RDFS;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.FileInputStream;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;

/**
 * fr.datatourisme.producer.processor.alignment.rule
 */
public class LiteralRuleTest extends AbstractProducerTest {

    @Autowired
    Ontology ontologyAlignment;

    @Test
    public void evaluate() throws Exception {
        LiteralRule rule = new LiteralRule();
        rule.setAlignment(getAlignement());
        rule.setProperty(ontologyAlignment.getProperty(RDFS.label));
        rule.setType(AbstractRule.Type.xpath);
        rule.setExpr("//nom/libelleFr");

        XmlContext ctx = XmlContext.create(IOUtils.toByteArray(new FileInputStream(getXmlSource())));
        Set<String> set = rule.evaluate(ctx).collect(Collectors.toSet());

        assertEquals(66, set.size());
    }
}
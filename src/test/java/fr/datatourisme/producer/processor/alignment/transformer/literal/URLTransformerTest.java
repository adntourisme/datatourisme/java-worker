/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.alignment.transformer.literal;

import fr.datatourisme.producer.processor.alignment.transformer.exception.TransformException;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * fr.datatourisme.producer.processor.alignment.transformer.literal
 */
public class URLTransformerTest {
    @Test
    public void transform() throws Exception {
        URLTransformer transformer = new URLTransformer();
        assertEquals("http://www.example.com/", transformer.transform("www.example.com/"));
        assertEquals("http://www.example.com/qsdqsd", transformer.transform("http://www.example.com/qsdqsd"));
        assertEquals("http://www.erwin-trum.com/Accueil/Accueil.php", transformer.transform("http://www.erwin-trum.com/Accueil/Accueil.php"));
        assertEquals("https://www.decathlonvillage.com/yutz-accueil", transformer.transform("https://www.decathlonvillage.com/yutz-accueil"));
    }

    @Test(expected=TransformException.class)
    public void transformFailed() throws Exception {
        URLTransformer transformer = new URLTransformer();
        transformer.transform("https//www.lamaisondelavachequirit.com");
    }
}
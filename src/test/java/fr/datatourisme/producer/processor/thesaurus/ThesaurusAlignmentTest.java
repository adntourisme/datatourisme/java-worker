/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.thesaurus;

import fr.datatourisme.producer.AbstractProducerTest;
import fr.datatourisme.vocabulary.Datatourisme;
import fr.datatourisme.vocabulary.DatatourismeThesaurus;
import org.junit.Assert;
import org.junit.Test;

public class ThesaurusAlignmentTest extends AbstractProducerTest {

    @Test
    public void testFromJson() {
        ThesaurusAlignment thesaurusAlignment = getThesaurusAlignment();

        Assert.assertEquals(thesaurusAlignment.resolve("ADOS", Datatourisme.resource("Audience")).stream().findFirst().orElse(null).getURI(),
            DatatourismeThesaurus.resource("Teenagers").getURI()
        );

        Assert.assertEquals(thesaurusAlignment.resolve("ADOS", Datatourisme.resource("PeopleAudience")).stream().findFirst().orElse(null).getURI(),
            DatatourismeThesaurus.resource("Teenagers").getURI()
        );

        Assert.assertEquals(thesaurusAlignment.resolve("ACTIVITE", Datatourisme.PointOfInterest).stream().findFirst().orElse(null).getURI(),
            Datatourisme.resource("PlaceOfInterest").getURI()
        );

        Assert.assertEquals(thesaurusAlignment.resolve("PATRIMOINE_CULTUREL", Datatourisme.PointOfInterest).stream().findFirst().orElse(null).getURI(),
            Datatourisme.resource("CulturalSite").getURI()
        );

        Assert.assertEquals(3, thesaurusAlignment.resolve("3301", Datatourisme.PointOfInterest).size());
        Assert.assertEquals(0, thesaurusAlignment.resolve("ADOS", Datatourisme.PointOfInterest).size());
        Assert.assertEquals(0, thesaurusAlignment.resolve("NULL", Datatourisme.PointOfInterest).size());
    }

    @Test
    public void testAlignCity() {
        ThesaurusAlignment thesaurusAlignment = getThesaurusAlignment();

        // test by insee
        Assert.assertEquals(thesaurusAlignment.resolve("35238", Datatourisme.resource("City")).stream().findFirst().orElse(null).getURI(),
                DatatourismeThesaurus.resource("35238").getURI()
        );

        // test by label
        Assert.assertEquals(thesaurusAlignment.resolve("Rennes", Datatourisme.resource("City")).stream().findFirst().orElse(null).getURI(),
                DatatourismeThesaurus.resource("35238").getURI()
        );
    }


    @Test
    public void testAlignPOIClass() {
        ThesaurusAlignment thesaurusAlignment = getThesaurusAlignment();

        // test by label
        Assert.assertEquals(thesaurusAlignment.resolve("Site religieux", Datatourisme.PointOfInterest).stream().findFirst().orElse(null).getURI(),
            Datatourisme.resource("ReligiousSite").getURI()
        );
    }
}

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.reasoner;

import fr.datatourisme.ontology.Ontology;
import fr.datatourisme.producer.AbstractProducerTest;
import fr.datatourisme.vocabulary.*;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;

public class InferenceReasonerTest extends AbstractProducerTest {

    @Autowired
    InferenceReasoner inferenceReasoner;

    @Autowired
    Ontology ontology;

    @Test
    public void testInterference() throws Exception {
        Model model = ModelFactory.createDefaultModel();
        model.createResource("urn:poi")
                .addProperty(RDF.type, Datatourisme.EntertainmentAndEvent);
        inferenceReasoner.process(model);
//        model.listStatements().forEachRemaining(System.out::println);
        Assert.assertEquals(3, model.size());

        Model model2 = ModelFactory.createDefaultModel();
        model2.createResource("urn:poi")
                .addProperty(RDF.type, Datatourisme.PlaceOfInterest);
        inferenceReasoner.process(model2);
//        model2.listStatements().forEachRemaining(System.out::println);
        Assert.assertEquals(2, model2.size());
    }

    @Test
    public void testGeoLiteral() throws Exception {
        Model model = ModelFactory.createDefaultModel();

        Resource geo = model.createResource("urn:geo")
            .addProperty(Schema.latitude, model.createTypedLiteral(new BigDecimal(48.1119800)))
            .addProperty(Schema.longitude, model.createTypedLiteral(new BigDecimal(-1.6742900)));

        Resource main = model.createResource("urn:test")
            .addProperty(RDF.type, Datatourisme.PlaceOfInterest)
            .addProperty(Schema.geo, geo);

        inferenceReasoner.process(model);
        //model.listStatements().forEachRemaining(System.out::println);

        Assert.assertEquals(Schema.GeoCoordinates, model.getProperty(geo, RDF.type).getObject());
        Assert.assertNotNull(model.getProperty(geo, Datatourisme.latlon));
        Assert.assertEquals(9, model.size());
    }

    @Test
    @Ignore
    @Deprecated
    public void testPlaceLocatedItself() throws Exception {
        Model model = ModelFactory.createDefaultModel();

        Resource place = model.createResource("urn:place")
            .addProperty(Schema.latitude, model.createTypedLiteral(new BigDecimal(48.1119800)))
            .addProperty(Schema.longitude, model.createTypedLiteral(new BigDecimal(-1.6742900)));

        Resource main = model.createResource("urn:test")
            .addProperty(RDF.type, Datatourisme.PlaceOfInterest)
            .addProperty(Datatourisme.isLocatedAt, place);

        inferenceReasoner.process(model);
        //model.listStatements().forEachRemaining(System.out::println);

        // now place is located on itself & get all properties
        Assert.assertEquals(main, model.getProperty(main, Datatourisme.isLocatedAt).getObject());
        Assert.assertNotNull(model.getProperty(main, Schema.latitude));
        Assert.assertNotNull(model.getProperty(main, Schema.longitude));

        // all properties of urn:place have gone
        Assert.assertEquals(0, place.listProperties().toList().size());
        Assert.assertEquals(8, model.size());
    }

    @Test
    public void testContact() throws Exception {
        Model model = ModelFactory.createDefaultModel();

        Resource address = model.createResource("urn:address")
            //.addProperty(RDF.type, Datatourisme.resource("PostalAddress"))
            .addProperty(Datatourisme.property("hasAddressCity"), model.createResource(ontology.getIndividual(DatatourismeThesaurus.resource("35238")).getURI()));

        inferenceReasoner.process(model);
        model.listStatements().forEachRemaining(System.out::println);

        Assert.assertEquals("Rennes", address.getProperty(Schema.property("addressLocality")).getObject().asLiteral().getString());
        Assert.assertEquals(4, model.size());
    }

    @Test
    public void testMultimedia() throws Exception {
        Model model = ModelFactory.createDefaultModel();

        Resource poi = model.createResource("urn:poi")
            .addProperty(RDF.type, Datatourisme.PlaceOfInterest)
            .addProperty(Datatourisme.property("hasRepresentation"),model.createResource("urn:representation")
                .addProperty(RDF.type, Ebucore.resource("Image"))
                .addProperty(Ebucore.property("hasRealisation"), model.createResource("urn:realisation")
                    .addProperty(Ebucore.property("locator"), model.createTypedLiteral("http://photo.jpg", XSDDatatype.XSDanyURI))
                )
            );

        inferenceReasoner.process(model);
        //model.listStatements().forEachRemaining(System.out::println);

        Assert.assertEquals("http://photo.jpg",
            poi.getProperty(Schema.property("image"))
                .getObject().asResource()
                .getProperty(Schema.property("contentUrl")).getString());
    }

    @Test
    @Ignore
    @Deprecated
    public void testLocalisation() throws Exception {
        Model model = ModelFactory.createDefaultModel();

        Resource offer = model.createResource("urn:offer")
            .addProperty(RDFS.label, "My offer");

        Resource place = model.createResource("urn:place");

        model.createResource("urn:poi")
            .addProperty(RDF.type, Datatourisme.PointOfInterest)
            .addProperty(Schema.offers, offer)
            .addProperty(Datatourisme.isLocatedAt, place);

        inferenceReasoner.process(model);
        //model.listStatements().forEachRemaining(System.out::println);

        Assert.assertEquals(place, offer.getProperty(Schema.areaServed).getObject());
        Assert.assertEquals(7, model.size());
    }

    @Test
    public void testEquipement() throws Exception {
        Model model = ModelFactory.createDefaultModel();

        Resource feature = model.createResource("urn:feature")
            .addProperty(RDFS.label, "My feature");

        Resource poi = model.createResource("urn:poi")
            .addProperty(RDF.type, Datatourisme.PointOfInterest)
            .addProperty(Datatourisme.property("hasFeature"), model.createResource("urn:features")
                .addProperty(Datatourisme.property("features"), feature)
            );

        inferenceReasoner.process(model);
        model.listStatements().forEachRemaining(System.out::println);

        Assert.assertEquals(feature, poi.getProperty(Datatourisme.property("isEquippedWith")).getObject());
        Assert.assertEquals(9, model.size());
    }

    @Test
    public void testPeriode() throws Exception {
        Model model = ModelFactory.createDefaultModel();

        Resource poi = model.createResource("urn:poi")
            .addProperty(RDF.type, Datatourisme.EntertainmentAndEvent)
            .addProperty(Datatourisme.property("takesPlaceAt"), model.createResource("urn:period")
                .addProperty(Datatourisme.property("startDate"), model.createTypedLiteral("2000-01-01", XSDDatatype.XSDdate))
                .addProperty(Datatourisme.property("endDate"), model.createTypedLiteral("2000-12-31", XSDDatatype.XSDdate))
            );

        inferenceReasoner.process(model);
        //model.listStatements().forEachRemaining(System.out::println);

        Assert.assertEquals("2000-01-01", poi.getProperty(Schema.startDate).getObject().asLiteral().getString());
        Assert.assertEquals("2000-12-31", poi.getProperty(Schema.endDate).getObject().asLiteral().getString());
    }

    @Test
    public void testClassement() throws Exception {
        Model model = ModelFactory.createDefaultModel();

        Resource review = model.createResource("urn:review")
            .addProperty(RDF.type, Datatourisme.resource("ScaleReview"));

        Resource poi = model.createResource("urn:poi")
            .addProperty(Datatourisme.property("hasReview"), review);

        inferenceReasoner.process(model);
        //model.listStatements().forEachRemaining(System.out::println);

        Assert.assertEquals(review, poi.getProperty(Schema.review).getObject());
        Assert.assertEquals(6, model.size());
    }

    @Test
    public void testDrop() throws Exception {
        Model model = ModelFactory.createDefaultModel();
        model.createResource("urn:poi")
            .addProperty(RDF.type, Datatourisme.resource("Product"))
            .addProperty(RDFS.label, model.createTypedLiteral("label"))
            .addProperty(DatatourismeMetadata.isObsolete, model.createTypedLiteral(false));
        inferenceReasoner.process(model);
        Assert.assertEquals(4, model.size());
    }

    @Test
    public void testDropCity() throws Exception {
        Model model = ModelFactory.createDefaultModel();

        Resource poi = model.createResource("urn:poi")
                .addProperty(Schema.address, model.createResource()
                        .addProperty(Datatourisme.hasAddressCity, ontology.getIndividual(DatatourismeThesaurus.resource("41113")).getOntResource())
                        .addProperty(Datatourisme.hasAddressCity, ontology.getIndividual(DatatourismeThesaurus.resource("72157")).getOntResource())
                        .addProperty(Schema.property("postalCode"), model.createTypedLiteral("72240"))
                );
        inferenceReasoner.process(model);
        //model.listStatements().forEachRemaining(System.out::println);

        Assert.assertEquals(1, poi.getPropertyResourceValue(Schema.address).listProperties(Datatourisme.hasAddressCity).toList().size());
        Assert.assertEquals(DatatourismeThesaurus.resource("72157").getURI(), poi.getPropertyResourceValue(Schema.address).getPropertyResourceValue(Datatourisme.hasAddressCity).getURI());
    }

    @Test
    public void testDropCityException() throws Exception {
        Model model = ModelFactory.createDefaultModel();

        Resource poi = model.createResource("urn:poi")
                .addProperty(Schema.address, model.createResource()
                        .addProperty(Datatourisme.hasAddressCity, ontology.getIndividual(DatatourismeThesaurus.resource("2B033")).getOntResource())
                        .addProperty(Schema.property("postalCode"), model.createTypedLiteral("20200"))
                );


        Resource poi2 = model.createResource("urn:poi2")
                .addProperty(Schema.address, model.createResource()
                        .addProperty(Datatourisme.hasAddressCity, ontology.getIndividual(DatatourismeThesaurus.resource("39274")).getOntResource())
                        .addProperty(Schema.property("postalCode"), model.createTypedLiteral("01410"))
                );

        inferenceReasoner.process(model);
        //model.listStatements().forEachRemaining(System.out::println);

        Assert.assertEquals(1, poi.getPropertyResourceValue(Schema.address).listProperties(Datatourisme.hasAddressCity).toList().size());
        Assert.assertEquals(DatatourismeThesaurus.resource("2B033").getURI(), poi.getPropertyResourceValue(Schema.address).getPropertyResourceValue(Datatourisme.hasAddressCity).getURI());

        Assert.assertEquals(1, poi2.getPropertyResourceValue(Schema.address).listProperties(Datatourisme.hasAddressCity).toList().size());
        Assert.assertEquals(DatatourismeThesaurus.resource("39274").getURI(), poi2.getPropertyResourceValue(Schema.address).getPropertyResourceValue(Datatourisme.hasAddressCity).getURI());

    }
}

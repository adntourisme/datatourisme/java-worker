/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor;

import fr.datatourisme.vocabulary.Datatourisme;
import org.apache.jena.datatypes.xsd.XSDDatatype;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.DC;
import org.apache.jena.vocabulary.RDF;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * fr.datatourisme.producer.processor
 */
public class URIGeneratorTest {
    @Test
    public void bug1() throws Exception {
        URIGenerator generator = new URIGenerator();
        Model model = ModelFactory.createDefaultModel();

        Resource res1 = model.createResource();
        res1.addProperty(RDF.type, Datatourisme.resource("Period"));
        res1.addProperty(Datatourisme.property("startDate"), "2001-01-10", XSDDatatype.XSDdate);
        res1.addProperty(Datatourisme.property("endDate"), "2001-01-12", XSDDatatype.XSDdate);

        Resource res2 = model.createResource();
        res2.addProperty(RDF.type, Datatourisme.resource("Period"));
        res2.addProperty(Datatourisme.property("endDate"), "2001-01-12", XSDDatatype.XSDdate);
        res2.addProperty(Datatourisme.property("startDate"), "2001-01-10", XSDDatatype.XSDdate);

        assertEquals(generator.generate(res1), generator.generate(res2));
    }

    @Test
    public void bug2() throws Exception {
        URIGenerator generator = new URIGenerator();
        Model model = ModelFactory.createDefaultModel();

        String description = "Marché d'art et de métiers anciens, ateliers médiévaux, visite du château, musique, théâtre, jonglerie au rendez-vous dans le village médiéval. Spectacle de combats, campement, archerie.\n" +
                "Repas médiéval animé le soir et spectacle de feu en final.";
        Resource res1 = model.createResource();
        res1.addProperty(Datatourisme.property("shortDescription"), description, XSDDatatype.XSDstring);
        res1.addProperty(DC.description, description, XSDDatatype.XSDstring);

        description = "Reconstitution d'une table médiévale suivie d'une collation composée de produits locaux.\n" +
                "Parcours découverte sur les habitudes alimentaires du Moyen-Age et évolution de certains mots et expressions encore employés.";
        Resource res2 = model.createResource();
        res2.addProperty(Datatourisme.property("shortDescription"), description, XSDDatatype.XSDstring);
        res2.addProperty(DC.description, description, XSDDatatype.XSDstring);

        assertNotEquals(generator.generate(res1), generator.generate(res2));
    }
}
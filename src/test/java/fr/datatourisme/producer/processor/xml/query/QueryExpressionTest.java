/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.xml.query;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.junit.Test;

import java.io.StringReader;

/**
 * 
 */
public class QueryExpressionTest/* extends AbstractTest*/ {
    @Test
    public void name() throws Exception {
//        File file = getXmlSource();
//        SAXBuilder sb = new SAXBuilder();
//        Document document = sb.build(new FileReader(file));

        String xml = "<h:test xmlns:h=\"http://www.w3.org/TR/html4/\" xmlns:s=\"http://www.w3.org/TR/html5/\"><h:test1 type=\"test\"><s:toto>xwcxwc</s:toto></h:test1><h:test1 type=\"test\"><s:toto>xwcxwc</s:toto></h:test1></h:test>";
        SAXBuilder sb = new SAXBuilder();
        Document document = sb.build(new StringReader(xml));
        Element element = document.getRootElement();
        QueryExpressionFactory qef = new QueryExpressionFactory(element.getNamespacesInScope());

        qef.compile("for $i in //s:toto return $i").evaluate(element).forEach(o -> {
            System.out.println(o);
        });


       /* System.out.println("test1");
        QueryExpression expression = new XQueryExpression("test1");
        expression.setContext(element).evaluate().forEach(o -> {
            System.out.println(o);

        });*/

    }
}
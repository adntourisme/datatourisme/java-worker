/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.processor.atomic.operation.map;

import fr.datatourisme.producer.processor.atomic.AtomicChain;
import fr.datatourisme.producer.processor.atomic.operation.AbstractOperation;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * fr.datatourisme.producer.processor.atomic.operation.map
 */
public class Html2TextTest {

    @Test
    public void process() throws Exception {
        List<String> test = new ArrayList<>();
        test.add("<p>Vive<br />la <strong>france</strong></p>");
        test.add("<p>Ceci&nbsp;est un test</p>");

        List<String> results = AtomicChain.create(new AbstractOperation[] {
            new Html2Text()
        }).process(test);

        Assert.assertEquals("Vive\nla france", results.get(0));
        Assert.assertEquals("Ceci est un test", results.get(1));
    }
}
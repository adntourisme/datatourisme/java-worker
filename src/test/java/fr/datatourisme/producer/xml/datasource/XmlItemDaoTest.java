/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.xml.datasource;

import com.alexkasko.springjdbc.iterable.CloseableIterator;
import fr.datatourisme.producer.AbstractProducerTest;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * Unit test for XmlItemDaoTest
 */
public class XmlItemDaoTest extends AbstractProducerTest
{
    private XmlItemDao dao;

    @Before
    public void setUp() {
        dao = XmlItemDao.instanciate(getTempFile("datasource.index"));
    }

    /**
     * testInsertOrUpdate
     */
    @Test
    public void testInsertOrReplace() throws IOException {
        XmlItem item = new XmlItem("id000");
        item.setBytes("<root></root>".getBytes());
        item.setStatus(false);

        dao.insertOrReplace(item);

        XmlItem back = dao.findById("id000");
        Assert.assertEquals(item.getDigest(), back.getDigest());
        Assert.assertEquals(item.getStatus(), back.getStatus());
    }

    /**
     * testInsertOrUpdate
     */
    @Test
    public void testUpdate() throws IOException {
        XmlItem item = new XmlItem("id000");
        item.setBytes("<root></root>".getBytes());
        item.setLabel("test");
        item.setStatus(true);
        dao.insertOrReplace(item);

        item.setLabel("test2");
        item.setStatus(false);
        dao.update(item);

        XmlItem back = dao.findById("id000");
        Assert.assertEquals("test2", back.getLabel());
        Assert.assertEquals(false, back.getStatus());
    }

    /**
     * testDelete
     */
    @Test
    public void testDelete() throws IOException {
        XmlItem item = new XmlItem("id000");
        item.setBytes("<root></root>".getBytes());
        dao.insertOrReplace(item);

        dao.delete(item);
        XmlItem back = dao.findById("id000");
        Assert.assertNull(back);
    }

    /**
     * testFindAll
     */
    @Test
    public void testFindAll() throws IOException {
        CloseableIterator<XmlItem> iter = dao.findAll();
        Assert.assertFalse(iter.hasNext());

        XmlItem item = new XmlItem("id000");
        item.setBytes("<root></root>".getBytes());
        dao.insertOrReplace(item);

        try {
            iter = dao.findAll();
            XmlItem back = iter.next();
            Assert.assertEquals(item.getIdentifier(), back.getIdentifier());
        } finally {
            IOUtils.closeQuietly(iter);
        }
    }
    /**
     * testFindAllByAttr
     */
    @Test
    public void testFindAllByAttr() throws IOException {
        CloseableIterator<XmlItem> iter = null;

        XmlItem item = new XmlItem("id000");
        item.setBytes("<root></root>".getBytes());
        dao.insertOrReplace(item);

        dao.insertAttr(item, "attr", "test");
        try {
            iter = dao.findAllByAttr("attr", "test");
            XmlItem back = iter.next();
            Assert.assertEquals(item.getIdentifier(), back.getIdentifier());
        } finally {
            IOUtils.closeQuietly(iter);
        }

        dao.resetAttr("attr");
        try {
            iter = dao.findAllByAttr("attr", "test");
            Assert.assertFalse(iter.hasNext());
        } finally {
            IOUtils.closeQuietly(iter);
        }
    }
}

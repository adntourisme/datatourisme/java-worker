/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.producer.xml.xpath;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ximpleware.NavException;
import com.ximpleware.ParseException;
import fr.datatourisme.producer.AbstractProducerTest;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;

public class XPathMapperTest extends AbstractProducerTest {

    @Test
    public void testParse() throws IOException, ParseException, NavException {
        URL sourceFile = this.getClass().getClassLoader().getResource("fixtures/xml/xpath/simple.xml");
        URL source2File = this.getClass().getClassLoader().getResource("fixtures/xml/xpath/simple2.xml");

        XPathMapper mapper = new XPathMapper(true);

        byte[] bytes = IOUtils.toByteArray(new FileInputStream(sourceFile.getFile()));
        mapper.parse(bytes);

        bytes = IOUtils.toByteArray(new FileInputStream(source2File.getFile()));
        mapper.parse(bytes);

        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(mapper.getItemsAsList()/*.get("objetsTouristiques/objetTouristique/descriptionTarif/conditions")*/);
        Assert.assertNotNull(json);
        //System.out.println(json);
    }

    @Test
    public void testCumulativeBug() throws IOException, ParseException, NavException {
        XPathMapper mapper = new XPathMapper(true);
        mapper.parse("<poi><test>salut</test></poi>".getBytes());
        mapper.parse("<poi><test>salut</test></poi>".getBytes());
        mapper.parse("<poi><test>salut</test></poi>".getBytes());
        mapper.parse("<poi><test>salut</test></poi>".getBytes());
        mapper.parse("<poi><test>salut</test><test>salut</test></poi>".getBytes());

        Assert.assertSame(2, mapper.getItemsAsList().get(0).getMaxOccurs());
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        String json = gson.toJson(mapper.getItemsAsList()/*.get("objetsTouristiques/objetTouristique/descriptionTarif/conditions")*/);
    }
}

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.consumer.task;

import fr.datatourisme.vocabulary.Datatourisme;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Statement;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.junit.Test;

import java.io.FileInputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.zip.GZIPInputStream;

import static org.junit.Assert.assertTrue;

/**
 * fr.datatourisme.consumer.task
 */
public class ProcessGraphResultTaskTest extends AbstractTaskTest {

    @Test
    public void testExecute() throws Exception {
        getConstructExecuteQueryTask().run();
        getProcessGraphResultTask().run();
        assertTrue(storage.dir("output").file("out.complete.nt").exists());
        assertTrue(storage.dir("output").file("out.complete.nt").length() > 0);
    }

    @Test
    public void testLanguageExecute() throws Exception {

        getConstructExecuteQueryTask().run();

        Set<String> languages = new HashSet<>(Arrays.asList("en"));
        ProcessGraphResultTask task2 = getProcessGraphResultTask();
        task2.setLanguages(languages);
        task2.run();

        // load model
        FileInputStream fis = new FileInputStream(storage.dir("output").file("out.complete.nt"));
        GZIPInputStream gzin = new GZIPInputStream(fis);
        Model model = ModelFactory.createDefaultModel() ;
        RDFDataMgr.read(model, gzin, Lang.TTL);

        //
        List<Statement> list = model.listStatements(null, Datatourisme.property("shortDescription"), (RDFNode)null).filterKeep(stmt -> {
            String language = stmt.getObject().asLiteral().getLanguage();
            return !language.equals("") && !languages.contains(language);
        }).toList();

        Set<String> set = model.listStatements(null, Datatourisme.property("shortDescription"), (RDFNode)null).filterKeep(stmt -> {
            String language = stmt.getObject().asLiteral().getLanguage();
            return !language.equals("");
        }).mapWith(stmt -> stmt.getObject().asLiteral().getLanguage()).toSet();

        assertTrue(list.isEmpty());
        assertTrue(set.size() == 1 && set.iterator().next().equals("en"));
    }

    @Test
    public void testBundleExecute() throws Exception {
        getConstructExecuteQueryTask().run();
        getProcessGraphResultTask("bundle/json").run();
        assertTrue(storage.dir("output").file("out.complete.zip").exists());
//        getProcessGraphResultTask("bundle/xml").run();
//        assertTrue(storage.dir("output").file("out.complete.zip").exists());
    }
}
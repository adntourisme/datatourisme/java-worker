/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.consumer.task;

import fr.datatourisme.worker.logger.reporter.ReportEntry;
import org.junit.Test;

/**
 * fr.datatourisme.consumer.task
 */
public class PublishDataGouvTaskTest extends AbstractTaskTest {

    @Test
    public void testExecute() throws Exception {
        //getProcessSPARQLTask().run();
        ReportEntry reportEntry = getPublishDataGouvTask().run();
        System.out.println(reportEntry.toJson(true));
    }
}
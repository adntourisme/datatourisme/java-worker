/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.consumer.processor.tuple;

import org.junit.Test;
import org.openrdf.query.resultio.binary.BinaryQueryResultParser;

import java.net.URL;

/**
 * fr.datatourisme.consumer.processor.tuple
 */
public class ExtractPOIUrisHandlerTest {
    @Test
    public void test() throws Exception {
        URL file = getClass().getClassLoader().getResource("fixtures/consumer/tuples.bin");
        BinaryQueryResultParser parser = new BinaryQueryResultParser();
        ExtractPOIUrisHandler extractHandler = new ExtractPOIUrisHandler();
        parser.setQueryResultHandler(extractHandler);
        parser.parse(file.openStream());

        extractHandler.getList().forEach(s -> {
            System.out.println(s);
        });
    }
}
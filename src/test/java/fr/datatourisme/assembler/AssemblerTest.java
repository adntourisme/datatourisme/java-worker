/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.assembler;

import fr.datatourisme.AbstractTest;
import fr.datatourisme.ontology.Ontology;
import fr.datatourisme.ontology.OntologyClass;
import fr.datatourisme.vocabulary.Datatourisme;
import fr.datatourisme.vocabulary.DatatourismeAlignment;
import fr.datatourisme.vocabulary.DatatourismeThesaurus;
import junitx.framework.Assert;
import org.apache.jena.sparql.vocabulary.FOAF;
import org.apache.jena.vocabulary.RDFS;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class AssemblerTest extends AbstractTest {

    @Autowired
    Assembler assembler;

    @Test
    public void testOntology() {
        Ontology ontology = assembler.createOntology( ":ontology" );
        Assert.assertTrue(ontology.getIndividual(DatatourismeThesaurus.resource("PrivateGarden")).isInstanceOf(Datatourisme.resource("Theme")));
        Assert.assertNotNull(ontology.getProperty(Datatourisme.property("hasCreated")));
    }

    @Test
    public void testOntologyLite() {
        Ontology ontology = assembler.createOntology( ":ontologyLite" );
        Assert.assertTrue(ontology.getClass(Datatourisme.resource("FastFoodRestaurant")).hasSuperClass(Datatourisme.PointOfInterest));
        Assert.assertNotNull(ontology.getProperty(Datatourisme.property("hasCreated")));
    }

    @Test
    public void testOntologyAlignment() {
        Ontology ontology = assembler.createOntology( ":ontologyAlignment" );

        // test hasAlignableProperties
        Assert.assertEquals(1, ontology.getClass(Datatourisme.resource("City")).getOntResource().listPropertyValues(DatatourismeAlignment.hasAlignableProperties).toList().size());

        // test hasCreated has been removed
        Assert.assertNull(ontology.getProperty(Datatourisme.property("hasCreated")));

        // test rdfs:comment has been removed for PointOfInterest, but not from foaf:Agent
        OntologyClass c = ontology.getClass(Datatourisme.PointOfInterest);
        Assert.assertFalse(c.listDirectProperties().anyMatch(p -> p.hasURI(RDFS.comment.getURI())));
        c = ontology.getClass(FOAF.Agent);
        Assert.assertTrue(c.listDirectProperties().anyMatch(p -> p.hasURI(RDFS.comment.getURI())));
    }
}

/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.webapp.api;

import fr.datatourisme.AbstractTest;
import org.apache.http.client.fluent.Content;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import java.io.IOException;

public class WebappApiClientTest extends AbstractTest {

    @Autowired
    @Qualifier("producer.apiclient")
    private WebappApiClient client;

    @Test
    @Ignore
    public void testPost() throws IOException {
        Content test = client.post("posts", null);
        System.out.println(test.getType());
    }
}

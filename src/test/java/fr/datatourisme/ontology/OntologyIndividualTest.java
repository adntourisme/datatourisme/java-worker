/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme.ontology;

import fr.datatourisme.AbstractTest;
import fr.datatourisme.vocabulary.Datatourisme;
import fr.datatourisme.vocabulary.DatatourismeThesaurus;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.stream.Collectors;

/**
 * fr.datatourisme.ontology
 */
public class OntologyIndividualTest extends AbstractTest {

    @Autowired
    Ontology ontology;

    @Test
    public void testHeritage() throws Exception {
        OntologyIndividual o = ontology.getIndividual(DatatourismeThesaurus.resource("VideoTerminal"));
        Assert.assertTrue(o.listTypes(false).map(OntologyResource::getURI).collect(Collectors.toList()).contains(Datatourisme.resource("Amenity").getURI()));
        Assert.assertTrue(o.isInstanceOf(Datatourisme.resource("Amenity").getURI()));
    }

}
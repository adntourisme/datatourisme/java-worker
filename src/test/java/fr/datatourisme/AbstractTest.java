/*
 * This file is part of the DATAtourisme project.
 * 2022
 * @author Conjecto <contact@conjecto.com>
 * SPDX-License-Identifier:  GPL-3.0-or-later
 * For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
 */

package fr.datatourisme;

import fr.datatourisme.task.AbstractTask;
import fr.datatourisme.worker.Storage;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.Closeable;
import java.io.File;
import java.util.Iterator;


@RunWith(SpringRunner.class)
@ContextConfiguration(classes = {Configuration.class})
@TestPropertySource("classpath:application.properties")
abstract public class AbstractTest {

    @Autowired
    protected ApplicationContext context;

    @Rule
    public TemporaryFolder temp = new TemporaryFolder();

    protected File getTempFile(String name) {
        return new File(temp.getRoot(), name);
    }

    protected Storage storage;

    @Before
    public void setUp() throws Exception {
        storage = new Storage(temp.getRoot());
    }

    /**
     * @param className
     * @param <T>
     * @return
     */
    protected <T extends AbstractTask> T createTask(Class<T> className) {
        try {
            T task = className.newInstance();
            context.getAutowireCapableBeanFactory().autowireBean(task);
            task.setStorage(storage);
            return task;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * @param expected
     * @param iter
     */
    public static void assertIterCount(int expected, Iterator<?> iter) {
        int count = 0;
        while(iter.hasNext()) {
            count++;
            iter.next();
        }
        if(iter instanceof Closeable) {
            IOUtils.closeQuietly((Closeable) iter);
        }
        Assert.assertEquals(expected, count);
    }
}

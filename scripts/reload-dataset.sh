#!/bin/bash

#
# This file is part of the DATAtourisme project.
# 2022
# @author Conjecto <contact@conjecto.com>
# SPDX-License-Identifier:  GPL-3.0-or-later
# For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
#

: ${DATATOURISME_DATA_PATH?"Need to set DATATOURISME_DATA_PATH"}
: ${DATATOURISME_WEBAPP_PRODUCER_PATH?"Need to set DATATOURISME_WEBAPP_PRODUCER_PATH"}
: ${DATATOURISME_WEBAPP_CONSUMER_URL?"Need to set DATATOURISME_WEBAPP_CONSUMER_URL"}


allFlux=`php ${DATATOURISME_WEBAPP_PRODUCER_PATH}/bin/console doctrine:query:sql \
    "SELECT CONCAT(id, ':', organization_id, ':', process_strategy_id) FROM flux f WHERE f.status = 'production'" | grep "string(" | cut -d '"' -f2`

for flux in ${allFlux}; do
    id=`cut -d':' -f1 <<<"$flux"`
    org=`cut -d':' -f2 <<<"$flux"`
    mode=`cut -d':' -f3 <<<"$flux"`
    directory=${DATATOURISME_DATA_PATH}/producer/${id}/source
    [[ ${mode} = "1" ]] && reset="true" || reset="false"
    if [ -d "$directory" ]; then
        alignment=`printf '%s/api/%d/rules' ${DATATOURISME_WEBAPP_CONSUMER_URL} ${id}`
        thesaurus=`printf '%s/internal/api/%d/thesaurus' ${DATATOURISME_WEBAPP_CONSUMER_URL} ${org}`

        payload=`printf '{
            "__source":"file://%s",
            "task":"process_xml",
            "alignment":"%s",
            "thesaurus":"%s",
            "flux": %d,
            "organization": %d,
            "reset": %s,
            "checksum": 1
        }' ${directory} ${alignment} ${thesaurus} ${id} ${org}, ${reset}`

        beanstool put -t producer -b "${payload}"
    fi
done
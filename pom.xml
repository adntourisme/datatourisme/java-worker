<!--
  ~ This file is part of the DATAtourisme project.
  ~ 2022
  ~ @author Conjecto <contact@conjecto.com>
  ~ SPDX-License-Identifier:  GPL-3.0-or-later
  ~ For the full copyright and license information, please view the LICENSE file that was distributed with this source code.
  -->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
  xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
  <modelVersion>4.0.0</modelVersion>

  <groupId>fr.datatourisme</groupId>
  <artifactId>java-worker</artifactId>
  <version>1.9.1</version>

  <name>java-worker</name>
  <description>Component part of the DATAtourisme project</description>
  <url>https://www.datatourisme.gouv.fr</url>

  <licenses>
    <license>
      <name>GNU General Public License v3.0 or later</name>
      <url>https://www.gnu.org/licenses/gpl-3.0-standalone.html</url>
    </license>
  </licenses>

  <developers>
    <developer>
      <name>Blaise de Carné</name>
      <email>contact@conjecto.com</email>
      <organization>Conjecto</organization>
      <organizationUrl>https://www.conjecto.com</organizationUrl>
    </developer>
  </developers>


  <properties>
    <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
    <maven.compiler.source>1.8</maven.compiler.source>
    <maven.compiler.target>1.8</maven.compiler.target>
    <start-class>fr.datatourisme.Application</start-class>
    <spring.version>4.3.7.RELEASE</spring.version>
    <spring.boot.version>1.5.3.RELEASE</spring.boot.version>
    <jena.version>3.13.1</jena.version>
    <undertow.version>1.3.24.Final</undertow.version>
    <sesame.version>2.7.12</sesame.version>
  </properties>

  <repositories>
    <repository>
      <id>maven</id>
      <url>https://repo.maven.apache.org/maven2</url>
    </repository>
    <repository>
      <id>jitpack.io</id>
      <url>https://jitpack.io</url>
    </repository>
  </repositories>

  <dependencies>
    <dependency>
      <groupId>com.conjecto.graphstore</groupId>
      <artifactId>graphstore</artifactId>
      <version>1.0.2</version>
    </dependency>
    <!-- https://mvnrepository.com/artifact/commons-dbcp/commons-dbcp -->
    <dependency>
      <groupId>commons-dbcp</groupId>
      <artifactId>commons-dbcp</artifactId>
      <version>1.4</version>
    </dependency>
    <!-- https://mvnrepository.com/artifact/org.apache.commons/commons-exec -->
    <dependency>
      <groupId>org.apache.commons</groupId>
      <artifactId>commons-exec</artifactId>
      <version>1.3</version>
    </dependency>
    <!-- https://mvnrepository.com/artifact/org.apache.httpcomponents/httpclient -->
    <dependency>
      <groupId>org.apache.httpcomponents</groupId>
      <artifactId>httpclient</artifactId>
      <version>4.5.5</version>
    </dependency>
    <!-- https://mvnrepository.com/artifact/org.apache.httpcomponents/fluent-hc -->
    <dependency>
      <groupId>org.apache.httpcomponents</groupId>
      <artifactId>fluent-hc</artifactId>
      <version>4.5.5</version>
    </dependency>
    <!-- nanocom - console -->
    <dependency>
      <groupId>com.github.nanocom</groupId>
      <artifactId>console</artifactId>
      <version>60d12ce</version>
    </dependency>
    <!-- SpringFramework -->
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-context</artifactId>
      <version>${spring.version}</version>
    </dependency>
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-jdbc</artifactId>
      <version>${spring.version}</version>
    </dependency>
    <dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-webmvc</artifactId>
      <version>${spring.version}</version>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot</artifactId>
      <version>${spring.boot.version}</version>
    </dependency>
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-autoconfigure</artifactId>
      <version>${spring.boot.version}</version>
    </dependency>
    <!-- https://mvnrepository.com/artifact/org.springframework.amqp/spring-rabbit -->
    <dependency>
      <groupId>org.springframework.amqp</groupId>
      <artifactId>spring-rabbit</artifactId>
      <version>2.3.0</version>
    </dependency>
    <!-- com.alexkasko.springjdbc -->
    <dependency>
      <groupId>com.alexkasko.springjdbc</groupId>
      <artifactId>springjdbc-iterable</artifactId>
      <version>1.0.3</version>
    </dependency>
    <!-- https://mvnrepository.com/artifact/org.xerial/sqlite-jdbc -->
    <dependency>
      <groupId>org.xerial</groupId>
      <artifactId>sqlite-jdbc</artifactId>
      <version>3.16.1</version>
    </dependency>
    <!-- unirest -->
    <dependency>
      <groupId>com.mashape.unirest</groupId>
      <artifactId>unirest-java</artifactId>
      <version>1.4.9</version>
    </dependency>
    <!-- undertow -->
    <dependency>
      <groupId>io.undertow</groupId>
      <artifactId>undertow-core</artifactId>
      <version>${undertow.version}</version>
    </dependency>
    <dependency>
      <groupId>io.undertow</groupId>
      <artifactId>undertow-servlet</artifactId>
      <version>${undertow.version}</version>
    </dependency>
    <!-- https://mvnrepository.com/artifact/commons-validator/commons-validator -->
    <dependency>
      <groupId>commons-validator</groupId>
      <artifactId>commons-validator</artifactId>
      <version>1.7</version>
    </dependency>
    <!-- vtd-xml -->
    <dependency>
      <groupId>com.ximpleware</groupId>
      <artifactId>vtd-xml</artifactId>
      <version>2.13</version>
    </dependency>
    <!-- https://mvnrepository.com/artifact/net.sf.saxon/Saxon-HE -->
    <dependency>
      <groupId>net.sf.saxon</groupId>
      <artifactId>Saxon-HE</artifactId>
      <version>9.7.0-20</version>
    </dependency>
    <!-- https://mvnrepository.com/artifact/log4j/log4j -->
    <dependency>
      <groupId>log4j</groupId>
      <artifactId>log4j</artifactId>
      <version>1.2.17</version>
    </dependency>
    <!-- https://mvnrepository.com/artifact/org.slf4j/slf4j-log4j12 -->
    <dependency>
      <groupId>org.slf4j</groupId>
      <artifactId>slf4j-log4j12</artifactId>
      <version>1.7.25</version>
    </dependency>
    <!-- https://github.com/dinstone/com.dinstone.beanstalkc -->
    <dependency>
      <groupId>com.dinstone</groupId>
      <artifactId>beanstalkc</artifactId>
      <version>2.2.0</version>
    </dependency>
    <!-- GSON -->
    <dependency>
      <groupId>com.google.code.gson</groupId>
      <artifactId>gson</artifactId>
      <version>2.8.0</version>
    </dependency>
    <!-- JENA -->
    <dependency>
      <groupId>org.apache.jena</groupId>
      <artifactId>jena-core</artifactId>
      <version>${jena.version}</version>
    </dependency>
    <dependency>
      <groupId>org.apache.jena</groupId>
      <artifactId>jena-querybuilder</artifactId>
      <version>${jena.version}</version>
    </dependency>
    <!-- https://mvnrepository.com/artifact/org.seaborne.rdf-delta/rdf-patch -->
    <dependency>
      <groupId>org.seaborne.rdf-delta</groupId>
      <artifactId>rdf-patch</artifactId>
      <version>0.8.2</version>
    </dependency>
    <!-- JDOM2 -->
    <dependency>
      <groupId>org.jdom</groupId>
      <artifactId>jdom2</artifactId>
      <version>2.0.6</version>
    </dependency>
    <!-- https://mvnrepository.com/artifact/org.openrdf.sesame/sesame-queryresultio-text -->
    <dependency>
      <groupId>org.openrdf.sesame</groupId>
      <artifactId>sesame-queryresultio-text</artifactId>
      <version>${sesame.version}</version>
    </dependency>
    <!-- https://mvnrepository.com/artifact/org.openrdf.sesame/sesame-queryresultio-binary -->
    <dependency>
      <groupId>org.openrdf.sesame</groupId>
      <artifactId>sesame-queryresultio-binary</artifactId>
      <version>${sesame.version}</version>
    </dependency>
    <!-- https://mvnrepository.com/artifact/org.openrdf.sesame/sesame-queryresultio-sparqljson -->
    <dependency>
      <groupId>org.openrdf.sesame</groupId>
      <artifactId>sesame-queryresultio-sparqljson</artifactId>
      <version>${sesame.version}</version>
    </dependency>
    <!-- https://mvnrepository.com/artifact/org.openrdf.sesame/sesame-queryresultio-sparqlxml -->
    <dependency>
      <groupId>org.openrdf.sesame</groupId>
      <artifactId>sesame-queryresultio-sparqlxml</artifactId>
      <version>${sesame.version}</version>
    </dependency>
    <!-- https://mvnrepository.com/artifact/jaxen/jaxen -->
    <dependency>
      <groupId>jaxen</groupId>
      <artifactId>jaxen</artifactId>
      <version>1.1.6</version>
    </dependency>
    <!-- https://mvnrepository.com/artifact/org.jsoup/jsoup -->
    <dependency>
      <groupId>org.jsoup</groupId>
      <artifactId>jsoup</artifactId>
      <version>1.10.3</version>
    </dependency>
    <!-- https://mvnrepository.com/artifact/org.pojava/pojava -->
    <!-- Date parsing library -->
    <dependency>
      <groupId>org.pojava</groupId>
      <artifactId>pojava</artifactId>
      <version>3.0.3</version>
    </dependency>
    <!-- https://mvnrepository.com/artifact/com.googlecode.libphonenumber/libphonenumber -->
    <!-- Phone number parsing library -->
    <dependency>
      <groupId>com.googlecode.libphonenumber</groupId>
      <artifactId>libphonenumber</artifactId>
      <version>8.7.0</version>
    </dependency>
    <!-- URL parsing library --><!-- https://mvnrepository.com/artifact/com.linkedin.urls/url-detector -->
    <dependency>
      <groupId>io.mola.galimatias</groupId>
      <artifactId>galimatias</artifactId>
      <version>0.2.0</version>
    </dependency>
    <!-- https://mvnrepository.com/artifact/org.rdfhdt/hdt-api -->
    <dependency>
      <groupId>org.rdfhdt</groupId>
      <artifactId>hdt-java-core</artifactId>
      <version>2.1.2</version>
    </dependency>
    <!-- junit -->
    <dependency>
      <groupId>junit</groupId>
      <artifactId>junit</artifactId>
      <version>4.12</version>
      <scope>test</scope>
    </dependency>
    <!-- junit-addons -->
    <dependency>
      <groupId>junit-addons</groupId>
      <artifactId>junit-addons</artifactId>
      <version>1.4</version>
      <scope>test</scope>
    </dependency>
    <!-- spring boot addons -->
    <dependency>
      <groupId>org.springframework.boot</groupId>
      <artifactId>spring-boot-starter-test</artifactId>
      <version>${spring.boot.version}</version>
      <scope>test</scope>
    </dependency>
    <!--<dependency>
      <groupId>org.springframework</groupId>
      <artifactId>spring-test</artifactId>
      <version>${spring.version}</version>
      <scope>test</scope>
    </dependency>-->
  </dependencies>

  <dependencyManagement>
    <dependencies>
      <dependency>
        <groupId>com.fasterxml.jackson.core</groupId>
        <artifactId>jackson-core</artifactId>
        <version>2.8.6</version>
      </dependency>
      <dependency>
        <groupId>com.fasterxml.jackson.core</groupId>
        <artifactId>jackson-annotations</artifactId>
        <version>2.8.6</version>
      </dependency>
      <dependency>
        <groupId>com.fasterxml.jackson.core</groupId>
        <artifactId>jackson-databind</artifactId>
        <version>2.8.6</version>
      </dependency>
    </dependencies>
  </dependencyManagement>

  <build>
    <plugins>
      <plugin>
        <!-- Generate bootable JAR -->
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-maven-plugin</artifactId>
        <configuration>
          <mainClass>${start-class}</mainClass>
        </configuration>
        <executions>
          <execution>
            <goals>
              <goal>repackage</goal>
            </goals>
          </execution>
        </executions>
      </plugin>
      <!-- maven surefire -->
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-plugin</artifactId>
        <configuration>
          <skipTests>true</skipTests>
        </configuration>
      </plugin>
      <!-- jacoco -->
      <plugin>
        <groupId>org.jacoco</groupId>
        <artifactId>jacoco-maven-plugin</artifactId>
        <version>0.7.9</version>
        <executions>
          <execution>
            <id>default-prepare-agent</id>
            <goals>
              <goal>prepare-agent</goal>
            </goals>
          </execution>
          <!--<execution>
            <id>default-report</id>
            <goals>
              <goal>report</goal>
            </goals>
          </execution>-->
        </executions>
      </plugin>
    </plugins>
  </build>

  <reporting>
    <plugins>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-project-info-reports-plugin</artifactId>
        <version>2.9</version>
        <reportSets>
          <reportSet>
            <reports>
              <report>index</report>
              <report>dependencies</report>
            </reports>
          </reportSet>
        </reportSets>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-surefire-report-plugin</artifactId>
        <version>2.19.1</version>
        <reportSets>
          <reportSet>
            <reports>
              <report>report-only</report>
            </reports>
          </reportSet>
        </reportSets>
      </plugin>
      <plugin>
        <groupId>org.jacoco</groupId>
        <artifactId>jacoco-maven-plugin</artifactId>
        <version>0.7.9</version>
        <reportSets>
          <reportSet>
            <reports>
              <!-- select non-aggregate reports -->
              <report>report</report>
            </reports>
          </reportSet>
        </reportSets>
      </plugin>
      <plugin>
        <groupId>org.apache.maven.plugins</groupId>
        <artifactId>maven-javadoc-plugin</artifactId>
        <version>2.10.4</version>
        <reportSets>
          <reportSet>
            <reports>
              <report>javadoc</report>
            </reports>
          </reportSet>
        </reportSets>
      </plugin>
    </plugins>
  </reporting>

</project>

CHANGELOG
===================

Ce fichier est basé sur [Keep a Changelog](http://keepachangelog.com/)
et le projet utilise [Semantic Versioning](http://semver.org/).

## En cours

## [1.9.1] - 2021-04-20

### Correction
- Les propriétés owl:sameAs apparaissent maintenant dans les sérialisations bundle

### Modification
- URLTransformer utilise maintenant la librairie Apache Commons Validators pour vérifier la validité de l'URL

## [1.9.0] - 2020-11-20

# Ajout
- Ajout de la gestion du pipeline d'enrichissement

### Correction
- L'alignement des propriétés fonctionnelles a été globalement revue pour assurer l'unicité de la propriété associée à la ressource
- La sérialisation bundle limite maintenant les propriétés fonctionnelles à une valeur

## [1.8.5] - 2020-09-23

### Correction
- Le téléchargement d'un flux se fait maintenant en stream pour gérer sans problème de mémoire une taille importante de flux

## [1.8.4] - 2020-07-06

### Correction
- Bundle : les anciens fichiers d'output sont maintenant supprimés

## [1.8.3] - 2020-06-26

### Correction
- Bundle : la portée des ressources à détailler à été revue
- Commande build-graphstore : ajout d'une option pour constituer le store dans un autre dossier

## [1.8.2] - 2020-06-25

### Correction
- Suppression du mode read-only pour les bases de données SQLLite
- Opération atomique Split : remplacement de StringUtils.split qui supprimait les espaces vides

## [1.8.1] - 2020-06-24

### Correction
- Correction de l'erreur SQLITE_BUSY

## [1.8.0] - 2020-06-24

### Ajout
- Ajout des nouveaux formats bundle Json-LD et XML
- Ajout d'un mode read-only pour les bases de données SQLLite
- Ajout de la commande "build-graphstore" permettant de préparer le graphstore
- Ajout d'un exemple de script CRON à utiliser en production

## [1.7.1] - 2020-01-28

### Modification
- L'obligation de contact ne doit pas être effective pour les aires de camping-car

## [1.7.0] - 2019-12-12

### Ajout
- Ajout de la date de modification dans Datatourisme lastUpdateDatatourisme
- Consumer API : ajout d'une route pour delete le storage
- Producer : Ajout du code postal aux rapports d'erreur

## [1.6.3] - 2019-06-17

### Correction
- Inférence de ville : ajout des exceptions de cohérence insee <> code postal (corse + liste)

## [1.6.2] - 2019-05-14

### Correction
- Correction du système de détermination de mime-type lors du téléchargement d'un XML sans extension

## [1.6.1] - 2019-04-15

### Modification
- La résolution des types de POI utilise maintenant les libellés de classe
- Mise à jour du Graphstore : les exports CSV ont maintenant une colonne lang

## [1.6.0] - 2019-01-14

### Modification
- Modification de l'IRI des ontologies DATAtourisme

### Ajout
- Ajout de scripts shell pour la mise à jours des IRI + reload général du dataset

## [1.5.1] - 2018-08-05

### Modification
- Les erreurs de requête XPath/XQuery sont maintenant non bloquantes et remontées dans les avertissements de validation

### Correction
- Optimisation de la prévisualisation d'expression

## [1.5.0] - 2018-07-11

### Ajout
- Ajout du support de l'alignement multilingue multi-sources

### Modification
- OntologySerializer peut maintenant sérialiser dans une langue particulière
- Refactor du touchexecute

### Correction
- Meilleures gestion des erreurs de parse XML

----------------

## [1.4.4] - 2018-06-12

### Correction
- Hotfix : limitation de la taille des erreurs retournées
- Hotfix : log des exceptions lors de l'alignement threadé
- Hotfix : DownloadTask : limitation du fallback aux url commencant par http/https

----------------

## [1.4.3] - 2018-06-08

### Correction
- Hotfix : les propriétés hasViolation sont maintenant ignorées pour comptabiliser les propriétés générées dans 
ResourceRule

----------------

## [1.4.2] - 2018-06-07

### Modification
- La startDate de la période est obligatoire

### Correction
- Correction de la règle de validation de la commune et le code postal
- Hotfix : utilisation de touchExecute au sein de DownloadTask
- Hotfix : ajout d'un fallback Apache HTTP Client lors du téléchargement (bug lftp/GnuTLS rehandshake)

----------------

## [1.4.1] - 2018-06-04

### Correction
- Hotfix : XmlProcessor traite maintenant toutes les rules d'une propriété
- Preview : ajout d'une info de langue dans la valeur

----------------

## [1.4.0] - 2018-06-04

### Ajout
- Ajout du support de l'alignement multilingue
- Filtrage des langues lors de la serialization des flux diffuseurs

----------------

## [1.3.6] - 2018-05-14

### Modification
- Mise à jour de la librairie conjecto/graphstore

----------------

## [1.3.5] - 2018-04-24

### Modification
- Modification de la méthode d'envoi HTTP de la requête SPARQL Blazegraph (POST + content-type)

----------------

## [1.3.4] - 2018-04-16

### Correction
- Modification du header d'accept pour accepter */*

----------------

## [1.3.3] - 2018-04-16

### Correction
- Ajout d'un header Accept lors du téléchargement du xml/zip

----------------

## [1.3.2] - 2018-04-05

### Correction
- Utilisation de la classe URL dans DownloadTask pour corriger la génération du fichier temporaire

----------------

## [1.3.1] - 2018-03-30

### Correction
- Validation : schema:postalAddress -> schema:address

----------------

## [1.3.0] - 2018-03-30

### Modifications
- La liste des propriétés à aligner est maintenant extraite à partir de la (des) classe(s) POI alignée(s)

### Ajout
- Ajout d'une règle d'inférence business pour retirer les propriétés hasAddressCity pointant vers une ville dont le code insee est incohérent avec le code postal, au sein d'une schema:address
- Ajout de différentes règles de validation pour rendre obligatoire : adresse, code postal, commune, créateur, date de mise à jour
- Ajout de la gestion des propriétés localisées

----------------

## [1.2.2] - 2018-03-22

### Modifications
- La webapp API utilise maintenant Apache HTTP fluent pour faire le post aux webapp
- Augmentation du timeout de socket à 2h

----------------

## [1.2.1] - 2018-03-20

### Modifications
- Mise à jour de la librairie graphstore

### Corrections
- Le requêtes DELETE dans le namespace d'archive se font maintenant en batch de 1000 resources

----------------

## [1.2.0] - 2018-03-16

### Modifications
- Meilleure prise en charge des erreurs en provenance de Blazegraph

### Ajout
- Support du format HDT pour le serializer

----------------

## [1.1.4] - 2018-02-28

### Corrections
- Les POI invalides ne sont plus supprimés de la base de donnée locale
- L'accès à la base de donnée locale se fait au travers d'une connection unique
- Lors d'une erreur d'itération, l'itérateur est fermé
- Les POI extraits de PDF d'un encodage autre qu'UTF-8 sont décodés/encodés en UTF-8 avant stockage
- Les routes du serveur HTTP gères maintenant correctement les points dans la dernière partie de l'URL

----------------

## [1.1.3] - 2018-02-23

### Corrections
- L'archivage des POI a été revu pour gérer le volume

----------------

## [1.1.2] - 2018-02-22

### Modifications
- Mise à jour de la dépendance conjecto/graphstore

----------------

## [1.1.1] - 2018-02-22

### Ajouts
- Les POI supprimés sont ajoutés dans le namespace archive. Les POI insérés sont retirés du namespace archive

### Modifications
- Lorsqu'un POI devient invalide, il est supprimé de la base de donnée locale (et donc du dataset)

### Corrections
- Nettoyage des dépendances inutiles
- Fix : ResourceRule a maintenant une liste de sous-règles initialisée

----------------

## [1.1.0] - 2018-02-12

### Modifications
- Prise en charge du nouveau format récursif d'alignement

----------------

## [1.0.3] - 2018-02-08

### Modifications
- Les tâches PersistRDFTask et ProduceRDF renvoie maintenant le type secondaire dans son rapport

----------------

## [1.0.2] - 2018-01-31

### Corrections
- La tâche ProduceRDF renvoie également le contenu de la propriété :hasBeenCreatedBy/schema:legalName dans son rapport

----------------

## [1.0.1] - 2018-01-30

### Corrections
- Le fichier d'alignement de test inclue maintenant l'alignement de la géolocalisation

### Modifications
- La tâche PersistRDFTask renvoie maintenant le contenu de la propriété :hasBeenCreatedBy/schema:legalName dans son rapport

----------------

## [1.0.0] - 2018-01-12

### Ajouts
- TimeTransformer : LiteralTransformer pour xsd:time
- Jena Builtin RegexNeg : renvoi true si le texte ne match pas avec un regex
- Règles de validation regex des adresses email (schema:email) et des URL (foaf:homepage)
- Html2Text : opération atomique pour transformer le contenu HTML en texte
- Les numéros de téléphone sont automatiquement converti au format INTL
- Les URL sont normalisée selon la spec https://url.spec.whatwg.org/
- Ajout des stats "items" et "identifiers" dans le rapport de la tâche ProcessXml
- Ajout de la priorité dans la prévisualisation des propriétés
- Ajout de l'obligation de renseigner les coordonnées géographiques d'un POI

### Modifications
- Ajout du fichier reviews.ttl dans la configuration de l'assembler
- Suppression de :allowsRating des propriétés inférées
- Les transformeurs de literaux ne nécessitent plus de Model
- Les erreurs de typages sont maintenant catchées par le LiteralTransformer (évite une exception du reasoner)
- La plateforme accepte maintenant tous les mimetype dérivés de application/xml
- Optimisation de l'algorithme de génération du schema XPath
- Gestion des expressions XQuery dans l'alignement
- Refactor des controller ExprPreviewController et ExprRawExtractController
- Modification de la structuration du format alignment.json

### Corrections
- Le rapport de fin de traitement s'assure maintenant de l'unicité des ressouces
- Le rapport d'erreur précise le vocabulaire concerné
- L'operation atomique Index gère correctement un index en dehors des limites de l'ensemble
- API : gestion de l'évaluation des fonctions string dans ExecuteRawXpathController

----------------

## [0.1.1] - 2017-07-04

### Ajouts

- Application : application en ligne de commande pour le lancement des services (worker, server)
- API : API REST à usage interne (http://127.0.0.1:8042)
- Assembler : service d'assemblage de modèles et reasoner Jena
- Commandes :
    - start-server : démarre le serveur d'API REST
    - start-worker : démarre un worker de tâche DATAtourisme
- Dataset : service d'interfacage SPARQL avec le dataset principal
- GSON : divers sérializer/déserializer GSON
- Ontology : encapsulation Jena Ont API, avec forte gestion du cache
- Processor :
    - Alignement : gestion de l'alignement produit par la plateforme DATAtourisme
    - Atomic : implémentation des traitements atomiques
    - Preview : service de prévisualisation de l'alignement
    - Reasoner : services d'inférence et de validation des POIs
        - InferenceReasoner : inférence principale (instances) + inférence extra (règles spécifiques DATAtourisme)
        - ValidationReasoner : inférence de validation
    - Report : association ressource RDF / rapport de validation
    - Thesaurus : gestion de l'alignement de thésaurus et résolution des valeurs
- Vocabulary : Divers helper des vocabulaires RDF courants
- Webapp API : Interface de dialogue avec l'API Rest de la webapp producer
- Worker : Package du worker de traitements
    - DownloadTask : tâche de téléchargement des sources XML
    - PreprocessXMLTask : tâche de préprocessing des sources XML
    - ProcessXMLTask : extraction et indexation des POI à partir des sources XML
    - ProduceRDFTask : production des triplets à partir de l'alignement et des POI indexés
    - PersistRDFTask : persistence dans le dataset
    - ReportingTask : reporting de traitement à l'API REST de la webapp producer
- XML : Service d'indexation XML et génération de cartographie XPath